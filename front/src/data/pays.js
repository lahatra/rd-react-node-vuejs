const pays= 
[
  {
    "id": 1,
    "pays": "Afghanistan"
  },
  {
    "id": 2,
    "pays": "Afrique du Sud"
  },
  {
    "id": 3,
    "pays": "Albanie"
  },
  {
    "id": 4,
    "pays": "Alg‚rie"
  },
  {
    "id": 5,
    "pays": "Allemagne"
  },
  {
    "id": 6,
    "pays": "Andorre"
  },
  {
    "id": 7,
    "pays": "Angola"
  },
  {
    "id": 8,
    "pays": "Anguilla"
  },
  {
    "id": 9,
    "pays": "Antigua-et-Barbuda"
  },
  {
    "id": 10,
    "pays": "Arabie saoudite"
  },
  {
    "id": 11,
    "pays": "Argentine"
  },
  {
    "id": 12,
    "pays": "Armenie"
  },
  {
    "id": 13,
    "pays": "Aruba"
  },
  {
    "id": 14,
    "pays": "Australie"
  },
  {
    "id": 15,
    "pays": "Autriche"
  },
  {
    "id": 16,
    "pays": "Azerba‹djan"
  },
  {
    "id": 17,
    "pays": "Bahamas"
  },
  {
    "id": 18,
    "pays": "Bahre‹n"
  },
  {
    "id": 19,
    "pays": "Bangladesh"
  },
  {
    "id": 20,
    "pays": "Barbade"
  },
  {
    "id": 21,
    "pays": "Belgique"
  },
  {
    "id": 22,
    "pays": "Belize"
  },
  {
    "id": 23,
    "pays": "Benin"
  },
  {
    "id": 24,
    "pays": "Bermudes"
  },
  {
    "id": 25,
    "pays": "Bhoutan"
  },
  {
    "id": 26,
    "pays": "Bi‚lorussie"
  },
  {
    "id": 27,
    "pays": "Bolivie (tat plurinational de)"
  },
  {
    "id": 28,
    "pays": "Bonaire"
  },
  {
    "id": 29,
    "pays": "Bosnie-Herz‚govine"
  },
  {
    "id": 30,
    "pays": "Botswana"
  },
  {
    "id": 31,
    "pays": "Br‚sil"
  },
  {
    "id": 32,
    "pays": "Brun‚i Darussalam"
  },
  {
    "id": 33,
    "pays": "Bulgarie"
  },
  {
    "id": 34,
    "pays": "Burkina Faso"
  },
  {
    "id": 35,
    "pays": "Burundi"
  },
  {
    "id": 36,
    "pays": "Cambodge"
  },
  {
    "id": 37,
    "pays": "Cameroun"
  },
  {
    "id": 38,
    "pays": "Canada"
  },
  {
    "id": 39,
    "pays": "Cap-Vert"
  },
  {
    "id": 40,
    "pays": "Chili"
  },
  {
    "id": 41,
    "pays": "Chine"
  },
  {
    "id": 42,
    "pays": "Chypre"
  },
  {
    "id": 43,
    "pays": "Colombie"
  },
  {
    "id": 44,
    "pays": "Comores"
  },
  {
    "id": 45,
    "pays": "Congo"
  },
  {
    "id": 46,
    "pays": "Congo (R‚publique d‚mocratique du)"
  },
  {
    "id": 47,
    "pays": "Cor‚e du Nord (r‚p. pop. d‚m. de)"
  },
  {
    "id": 48,
    "pays": "Cor‚e du Sud (r‚p. de)"
  },
  {
    "id": 49,
    "pays": "Costa Rica"
  },
  {
    "id": 50,
    "pays": "C“te d?Ivoire"
  },
  {
    "id": 51,
    "pays": "Croatie"
  },
  {
    "id": 52,
    "pays": "Cuba"
  },
  {
    "id": 53,
    "pays": "Cura‡ao"
  },
  {
    "id": 54,
    "pays": "Danemark"
  },
  {
    "id": 55,
    "pays": "Djibouti"
  },
  {
    "id": 56,
    "pays": "Dominique"
  },
  {
    "id": 57,
    "pays": "gypte"
  },
  {
    "id": 58,
    "pays": "El Salvador"
  },
  {
    "id": 59,
    "pays": "mirats arabes unis"
  },
  {
    "id": 60,
    "pays": "quateur"
  },
  {
    "id": 61,
    "pays": "rythr‚e"
  },
  {
    "id": 62,
    "pays": "Espagne"
  },
  {
    "id": 63,
    "pays": "Estonie"
  },
  {
    "id": 64,
    "pays": "Eswatini"
  },
  {
    "id": 65,
    "pays": "tats-Unis d?Am‚rique"
  },
  {
    "id": 66,
    "pays": "thiopie"
  },
  {
    "id": 67,
    "pays": "Fidji"
  },
  {
    "id": 68,
    "pays": "Finlande"
  },
  {
    "id": 69,
    "pays": "France"
  },
  {
    "id": 70,
    "pays": "Gabon"
  },
  {
    "id": 71,
    "pays": "Gambie"
  },
  {
    "id": 72,
    "pays": "G‚orgie"
  },
  {
    "id": 73,
    "pays": "G‚orgie du Sud-et-les ×les Sandwich du Sud"
  },
  {
    "id": 74,
    "pays": "Ghana"
  },
  {
    "id": 75,
    "pays": "Gibraltar"
  },
  {
    "id": 76,
    "pays": "GrŠce"
  },
  {
    "id": 77,
    "pays": "Grenade"
  },
  {
    "id": 78,
    "pays": "Groenland"
  },
  {
    "id": 79,
    "pays": "Guadeloupe"
  },
  {
    "id": 80,
    "pays": "Guam"
  },
  {
    "id": 81,
    "pays": "Guatemala"
  },
  {
    "id": 82,
    "pays": "Guernesey"
  },
  {
    "id": 83,
    "pays": "Guin‚e"
  },
  {
    "id": 84,
    "pays": "Guin‚e ‚quatoriale"
  },
  {
    "id": 85,
    "pays": "Guin‚e-Bissau"
  },
  {
    "id": 86,
    "pays": "Guyana"
  },
  {
    "id": 87,
    "pays": "Guyane fran‡aise"
  },
  {
    "id": 88,
    "pays": "Ha‹ti"
  },
  {
    "id": 89,
    "pays": "Honduras"
  },
  {
    "id": 90,
    "pays": "Hongrie"
  },
  {
    "id": 91,
    "pays": "×le Bouvet"
  },
  {
    "id": 92,
    "pays": "×le Christmas"
  },
  {
    "id": 93,
    "pays": "×le de Man"
  },
  {
    "id": 94,
    "pays": "×le Norfolk"
  },
  {
    "id": 95,
    "pays": "×les Ca‹manes"
  },
  {
    "id": 96,
    "pays": "×les Cook"
  },
  {
    "id": 97,
    "pays": "×les d?land"
  },
  {
    "id": 98,
    "pays": "×les des Cocos (Keeling)"
  },
  {
    "id": 99,
    "pays": "×les Falkland (Malvinas)"
  },
  {
    "id": 100,
    "pays": "×les F‚ro‚"
  },
  {
    "id": 101,
    "pays": "×les Mariannes du Nord"
  },
  {
    "id": 102,
    "pays": "×les Marshall"
  },
  {
    "id": 103,
    "pays": "×les Salomon"
  },
  {
    "id": 104,
    "pays": "×les Svalbard-et-Jan Mayen"
  },
  {
    "id": 105,
    "pays": "×les Turques-et-Ca‹ques"
  },
  {
    "id": 106,
    "pays": "×les Vierges am‚ricaines"
  },
  {
    "id": 107,
    "pays": "×les Vierges britanniques"
  },
  {
    "id": 108,
    "pays": "×les Wallis-et-Futuna"
  },
  {
    "id": 109,
    "pays": "Inde"
  },
  {
    "id": 110,
    "pays": "Indon‚sie"
  },
  {
    "id": 111,
    "pays": "Irak"
  },
  {
    "id": 112,
    "pays": "Iran (R‚publique islamique d?)"
  },
  {
    "id": 113,
    "pays": "Irlande"
  },
  {
    "id": 114,
    "pays": "Islande"
  },
  {
    "id": 115,
    "pays": "Isra‰l"
  },
  {
    "id": 116,
    "pays": "Italie"
  },
  {
    "id": 117,
    "pays": "Jama‹que"
  },
  {
    "id": 118,
    "pays": "Japon"
  },
  {
    "id": 119,
    "pays": "Jersey"
  },
  {
    "id": 120,
    "pays": "Jordanie"
  },
  {
    "id": 121,
    "pays": "Kazakhstan"
  },
  {
    "id": 122,
    "pays": "Kenya"
  },
  {
    "id": 123,
    "pays": "Kirghizistan"
  },
  {
    "id": 124,
    "pays": "Kiribati"
  },
  {
    "id": 125,
    "pays": "Kowe‹t"
  },
  {
    "id": 126,
    "pays": "Laos (R‚publique d‚mocratique populaire)"
  },
  {
    "id": 127,
    "pays": "Lesotho"
  },
  {
    "id": 128,
    "pays": "Lettonie"
  },
  {
    "id": 129,
    "pays": "Liban"
  },
  {
    "id": 130,
    "pays": "Lib‚ria"
  },
  {
    "id": 131,
    "pays": "Libye"
  },
  {
    "id": 132,
    "pays": "Liechtenstein"
  },
  {
    "id": 133,
    "pays": "Lituanie"
  },
  {
    "id": 134,
    "pays": "Luxembourg"
  },
  {
    "id": 135,
    "pays": "Mac‚doine"
  },
  {
    "id": 136,
    "pays": "Madagascar"
  },
  {
    "id": 137,
    "pays": "Malaisie"
  },
  {
    "id": 138,
    "pays": "Malawi"
  },
  {
    "id": 139,
    "pays": "Maldives"
  },
  {
    "id": 140,
    "pays": "Mali"
  },
  {
    "id": 141,
    "pays": "Malte"
  },
  {
    "id": 142,
    "pays": "Maroc"
  },
  {
    "id": 143,
    "pays": "Martinique"
  },
  {
    "id": 144,
    "pays": "Maurice"
  },
  {
    "id": 145,
    "pays": "Mauritanie"
  },
  {
    "id": 146,
    "pays": "Mayotte"
  },
  {
    "id": 147,
    "pays": "Mexique"
  },
  {
    "id": 148,
    "pays": "Micron‚sie (tats f‚d‚r‚s de)"
  },
  {
    "id": 149,
    "pays": "Moldavie"
  },
  {
    "id": 150,
    "pays": "Monaco"
  },
  {
    "id": 151,
    "pays": "Mongolie"
  },
  {
    "id": 152,
    "pays": "Mont‚n‚gro"
  },
  {
    "id": 153,
    "pays": "Montserrat"
  },
  {
    "id": 154,
    "pays": "Mozambique"
  },
  {
    "id": 155,
    "pays": "Myanmar"
  },
  {
    "id": 156,
    "pays": "Namibie"
  },
  {
    "id": 157,
    "pays": "Nauru"
  },
  {
    "id": 158,
    "pays": "N‚pal"
  },
  {
    "id": 159,
    "pays": "Nicaragua"
  },
  {
    "id": 160,
    "pays": "Niger"
  },
  {
    "id": 161,
    "pays": "Nig‚ria"
  },
  {
    "id": 162,
    "pays": "Niou‚"
  },
  {
    "id": 163,
    "pays": "NorvŠge"
  },
  {
    "id": 164,
    "pays": "Nouvelle-Cal‚donie"
  },
  {
    "id": 165,
    "pays": "Nouvelle-Z‚lande"
  },
  {
    "id": 166,
    "pays": "Oman"
  },
  {
    "id": 167,
    "pays": "Ouganda"
  },
  {
    "id": 168,
    "pays": "Ouzb‚kistan"
  },
  {
    "id": 169,
    "pays": "Pakistan"
  },
  {
    "id": 170,
    "pays": "Palaos"
  },
  {
    "id": 171,
    "pays": "Palestine"
  },
  {
    "id": 172,
    "pays": "Panama"
  },
  {
    "id": 173,
    "pays": "Papouasie-Nouvelle-Guin‚e"
  },
  {
    "id": 174,
    "pays": "Paraguay"
  },
  {
    "id": 175,
    "pays": "Pays-Bas"
  },
  {
    "id": 176,
    "pays": "P‚rou"
  },
  {
    "id": 177,
    "pays": "Philippines"
  },
  {
    "id": 178,
    "pays": "Pitcairn"
  },
  {
    "id": 179,
    "pays": "Pologne"
  },
  {
    "id": 180,
    "pays": "Polyn‚sie fran‡aise"
  },
  {
    "id": 181,
    "pays": "Porto Rico"
  },
  {
    "id": 182,
    "pays": "Portugal"
  },
  {
    "id": 183,
    "pays": "Qatar"
  },
  {
    "id": 184,
    "pays": "R‚publique centrafricaine"
  },
  {
    "id": 185,
    "pays": "R‚publique dominicaine"
  },
  {
    "id": 186,
    "pays": "R‚publique tchŠque"
  },
  {
    "id": 187,
    "pays": "R‚union"
  },
  {
    "id": 188,
    "pays": "Roumanie"
  },
  {
    "id": 189,
    "pays": "Royaume-Uni"
  },
  {
    "id": 190,
    "pays": "Russie (F‚d‚ration de)"
  },
  {
    "id": 191,
    "pays": "Rwanda"
  },
  {
    "id": 192,
    "pays": "Saba"
  },
  {
    "id": 193,
    "pays": "Sahara occidental"
  },
  {
    "id": 194,
    "pays": "Saint-Barth‚lemy"
  },
  {
    "id": 195,
    "pays": "Sainte-H‚lŠne"
  },
  {
    "id": 196,
    "pays": "Sainte-Lucie"
  },
  {
    "id": 197,
    "pays": "Saint-Eustache"
  },
  {
    "id": 198,
    "pays": "Saint-Kitts-et-Nevis"
  },
  {
    "id": 199,
    "pays": "Saint-Marin"
  },
  {
    "id": 200,
    "pays": "Saint-Martin (partie fran‡aise)"
  },
  {
    "id": 201,
    "pays": "Saint-Martin (partie n‚erlandaise)"
  },
  {
    "id": 202,
    "pays": "Saint-Pierre-et-Miquelon"
  },
  {
    "id": 203,
    "pays": "Saint-Vincent-et-les Grenadines"
  },
  {
    "id": 204,
    "pays": "Samoa"
  },
  {
    "id": 205,
    "pays": "Samoa am‚ricaines"
  },
  {
    "id": 206,
    "pays": "Sao Tom‚-et-Principe"
  },
  {
    "id": 207,
    "pays": "S‚n‚gal"
  },
  {
    "id": 208,
    "pays": "Serbie"
  },
  {
    "id": 209,
    "pays": "Sercq"
  },
  {
    "id": 210,
    "pays": "Seychelles"
  },
  {
    "id": 211,
    "pays": "Sierra Leone"
  },
  {
    "id": 212,
    "pays": "Singapour"
  },
  {
    "id": 213,
    "pays": "Slovaquie"
  },
  {
    "id": 214,
    "pays": "Slov‚nie"
  },
  {
    "id": 215,
    "pays": "Somalie"
  },
  {
    "id": 216,
    "pays": "Soudan"
  },
  {
    "id": 217,
    "pays": "Soudan du Sud"
  },
  {
    "id": 218,
    "pays": "Sri Lanka"
  },
  {
    "id": 219,
    "pays": "SuŠde"
  },
  {
    "id": 220,
    "pays": "Suisse"
  },
  {
    "id": 221,
    "pays": "Suriname"
  },
  {
    "id": 222,
    "pays": "Syrie"
  },
  {
    "id": 223,
    "pays": "Tadjikistan"
  },
  {
    "id": 224,
    "pays": "Ta‹wan"
  },
  {
    "id": 225,
    "pays": "Tanzanie (R‚publique-Unie de)"
  },
  {
    "id": 226,
    "pays": "Tchad"
  },
  {
    "id": 227,
    "pays": "Terres australes fran‡aises"
  },
  {
    "id": 228,
    "pays": "Territoire britannique de l'oc‚an Indien"
  },
  {
    "id": 229,
    "pays": "Tha‹lande"
  },
  {
    "id": 230,
    "pays": "Timor-Leste"
  },
  {
    "id": 231,
    "pays": "Togo"
  },
  {
    "id": 232,
    "pays": "Tok‚laou"
  },
  {
    "id": 233,
    "pays": "Tonga"
  },
  {
    "id": 234,
    "pays": "Trinit‚-et-Tobago"
  },
  {
    "id": 235,
    "pays": "Tunisie"
  },
  {
    "id": 236,
    "pays": "Turkm‚nistan"
  },
  {
    "id": 237,
    "pays": "Turquie"
  },
  {
    "id": 238,
    "pays": "Tuvalu"
  },
  {
    "id": 239,
    "pays": "Ukraine"
  },
  {
    "id": 240,
    "pays": "Uruguay"
  },
  {
    "id": 241,
    "pays": "Vanuatu"
  },
  {
    "id": 242,
    "pays": "Vatican"
  },
  {
    "id": 243,
    "pays": "Venezuela (R‚publique bolivarienne du)"
  },
  {
    "id": 244,
    "pays": "Viet Nam"
  },
  {
    "id": 245,
    "pays": "Y‚men"
  },
  {
    "id": 246,
    "pays": "Zambie"
  },
  {
    "id": 247,
    "pays": "Zimbabwe"
  }
];

  export default pays;