import React, { Component } from 'react';
import Header4 from '../components/header4';

class Info extends Component {

	render() {
	  return (
		  <div>
			  <Header4/>
  <div className="sante-page">				
		<div id="titre">
			<div className ="element">
			<label className="title">Moustiques </label>
			</div>
		</div>
	
		<div className="group">
		<label className="description">
		En résumé, je me protège des moustiques !
		<br></br>
- Avant mon départ : 
Je prévois de vêtements amples et couvrants, qui me protègeront aussi du soleil
J’imprègne mes vêtements à l’aide d’un produit insecticide adapté ;
<br></br>
- Sur place, je me protège le jour et/ou la nuit en fonction du risque local avec des répulsifs cutanés, mes vêtements imprégnés et si possible une moustiquaire (de préférence imprégnée).

		</label>
		</div>
		<div id="conteneur">
		<div className="element">
		<img className="prise"src="../img/moustiquaire.png" alt="prise"/>
		</div>
		</div>
		<br></br>
		<div className="group">
<label class="description">	- Je peux utiliser aussi des méthodes complémentaires efficaces comme la climatisation, les insecticides (en spray ou diffuseur), les raquettes électriques, les serpentins fumigènes ; sans oublier qu’elles ne suffisent pas à elles seules.
</label>	
</div>
		<div className="tab blue">
				<input id="tab-four" type="radio" name="tabs2"/>
					<label for="tab-four">En savoir plus  </label>
				<div className="tab-content">
					<p>
					Il peut être nécessaire de me protéger contre les piqûres d’insectes pour de multiples raisons. Les protections les plus efficaces sont l’application de répulsifs cutanés sur les parties non couvertes du corps, l’utilisation de vêtements amples et couvrants, voire imprégnés d’insecticides, et dormir la nuit sous une moustiquaire imprégnée (correctement installée et en s’assurant de l’intégrité du maillage). Ils sont disponibles en pharmacie sans prescription ou dans les magasins spécialisés de voyage.

Dans les habitations, la climatisation diminue les risques de piqûres. Elle peut être utilisée en mesure d’appoint, tout comme les insecticides (en spray ou diffuseur) ainsi que les raquettes électriques ; mais sont moins efficaces que les méthodes précédentes.
À l’extérieur, les serpentins fumigènes peuvent également être utilisés.
Il est également recommandé de ne pas laisser stagner de l’eau dans des récipients, car cela favorise la multiplication des moustiques.

					</p>
					<br></br>
					<p>
					Les moustiquaires grillagées aux portes et fenêtres sont presque aussi efficaces que les moustiquaires imprégnées et constitue une bonne protection.
Pour se protéger des piqûres de moustique en journée ou en soirée, l’usage de répulsifs cutanés et de vêtements imprégnés est fortement recommandé. 

En revanche, les produits suivants ne sont pas (suffisamment) efficaces :
Les bracelets anti-insectes pour se protéger des moustiques et des tiques ;
Les huiles essentielles : lorsqu’elles sont efficaces, leur durée d’efficacité est limitée (généralement une vingtaine de minutes).
Autres méthodes non ou insuffisamment efficaces : les appareils sonores à ultrasons, la vitamine B1, l’homéopathie, les rubans, papiers et autocollants gluants sans insecticide.

					</p>
				</div>
		</div>
		<div className="tab blue">
				<input id="tab-five" type="radio" name="tabs2"/>
					<label for="tab-five">Zones à risque </label>
				<div className="tab-content">
					<p>Lorem ipsum dolor ismet </p>
					
				</div>
		</div>

		<div className="tab blue">
				<input id="tab-six" type="radio" name="tabs2"/>
					<label for="tab-six">Comment choisir un répulsif pour la peau ? </label>
				<div className="tab-content">
					<p>Les espèces de moustiques sont nombreuses, aucun produit n’est parfait et efficace sur tous les moustiques. Il faut donc privilégier des produits globalement efficaces sur les moustiques les plus fréquents, mais surtout efficaces contre ceux qui peuvent transmettre des maladies. Celui-ci doit donc contenir l’un des composants actifs suivants : DEET ou IR3535. D’autres sont en cours d’évaluation au niveau européen. </p>
					<div id="conteneur">
					<div id="element">
					</div>
					<img src="../img/répulsif_moustique.png" className="image_footer" alt="Infos"/>
					</div>
				</div>
		</div>
		<div className="tab blue">
				<input id="tab-seven" type="radio" name="tabs2"/>
					<label for="tab-seven">Comment utiliser un répulsif cutané ? </label>
				<div className="tab-content">
					<p>Les répulsifs cutanés sont composés d’une substance active qui éloigne les insectes sans les tuer et sont à appliquer sur toutes les parties du corps non couvertes. La durée de la protection varie de 4 à 8 heures selon le produit et les conditions (transpiration, température et humidité ambiantes…). Le nombre maximal d’application par jour est variable et précisé dans la notice du produit.
Les répulsifs sont éliminés par l’eau, il faut donc penser à renouveler l’application après une douche ou une baignade. En cas d’utilisation de crème solaire, la crème solaire doit toujours être appliquée avant le répulsif. L’application de répulsif doit donc avoir lieu après un délai d’au moins 20 minutes pour limiter la diminution d’efficacité de la crème solaire. Il est aussi préférable d’utiliser une crème solaire d’indice de protection élevée (50+) dans ce cas.
 </p>
					
				</div>
		</div>
		<div className="tab blue">
				<input id="tab-eight" type="radio" name="tabs2"/>
					<label for="tab-eight">Pourquoi mettre des produits insecticides sur mes vêtements ? </label>
				<div className="tab-content">
					<p>Ces produits évitent les piqûres au travers des vêtements. Une seule imprégnation suffit et résiste à plusieurs lavages. Il ne faut donc imprégner que les vêtements visibles (pas les sous-vêtements). Les répulsifs pour la peau ne sont nécessaires que sur les parties non couvertes.

 </p>
					
				</div>
		</div>


		
		
		<div className="group">
					<div className="footer">
				<a href="/info">
					<img src="../img/info.png" className="image_footer" alt="Infos"/>
				</a>
				<a href="/moustique">
					<img src="../img/moustique.png" className="image_footer" alt="Moustique"/>
				</a>
				<a href="/soleil">
					<img src="/img/soleil.png" className="image_footer" alt="soleil2"/>
				</a>
				<a href="/baignade">
					<img src="../img/baignade.png" className="image_footer" alt="baignade"/>
				</a>
				<a href="/animaux">
					<img src="../img/animaux.png" className="image_footer" alt="renard"/>
				</a>
				<a href="/quiz">
				<img src="../img/quizz.png" className="image_footer" alt="Transport"/>
				</a>
			</div>	
		</div>
		</div>
		</div>

	  );
	}
  }
  
  export default Info;