import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Header4 from '../components/header4';

class Authentification extends Component {
  login = () => {
    console.log("login");
  }

  render() {
    return (
      <div>
           <Header4/>
        <div className="sante-page">				
        <div id="titre">
        <div className="element">
            <label className="title">Ma Santé</label>
        </div>
        </div>
        <div className="group">
            <label className="description">Age : 30ans</label>
        </div>
        <div className="group">
            <label className="description">Poids : 76kg</label>
        </div>
        <div className="group">
            <label className="description">Antécedant : Dépression</label>
        </div>
        <div className="group">
            <label className="description">Allergie : Oeuf , arachide </label>
        </div>
        <div className="group">
            <label className="description">Traitement habituel : SEROPLEX 10MG 1 fois par jours / tous les jours </label>
        </div>
        <div className="form">
            <form className="login-form">
              <button onClick={this.login}>Modifier</button>
            </form>
          </div>
    <div className="group">
        <div className="footer">
           
            <Link to="/vaccin">
                 <img src="../img/vaccin.png" className="image_footer" alt="vaccin" />
            </Link>
            <Link to="/medicament">
                <img src="../img/medicament.png" className="image_footer" alt="medicament" />
            </Link>
            <Link to="/trousse">
                <img src="../img/trousse.png" className="image_footer" alt="trousse" />
            </Link>
            <Link to="/medecin">
                <img src="../img/medecin.png" className="image_footer" alt="medecin" />
            </Link>
            <Link to="/hand">
                <img src="../img/hand.png" className="image_footer" alt="hand" />
            </Link>
            </div>	
        </div>
    </div>
    </div>
    );
  }
}

export default Authentification;
