import axios from 'axios';

import {
  ADD_MALADIE,
  GET_ERRORS,
  CLEAR_ERRORS,
  GET_ONE_MALADIE,
  GET_MALADIE,
  MALADIE_LOADING,
  DELETE_MALADIE,
  UPDATE_MALADIE,
} from './types';
import config from '../utils/config';

const listArray = [
  'sejour',
  'vaccin',
  'vaccinSugg',
];

// Set loading state
export const setMaladiesLoading = () => ({
  type: MALADIE_LOADING,
});

// Clear errors
export const clearErrors = () => ({
  type: CLEAR_ERRORS,
});

// Get Maladies
export const getMaladie = () => (dispatch, getState) => {
  dispatch(setMaladiesLoading());
  axios
    .get(`${config.baseUrl}/api/maladie`)
    .then((res) => {
      const payload = res.data.map((i) => {
        const result = i;
        listArray.map((j) => {
          if (j === 'vaccinSugg') {
            if (i[j] && i[j].length > 0) {
              result[j] = getState().vaccin && getState().vaccin.vaccin ? getState().vaccin.vaccin.filter(p => i[j].map(k => k._id).includes(p._id)) : [];
            }
            return result;
          }
          if (i[j] && i[j].length > 0) {
            result[j] = getState()[j] && getState()[j][j] ? getState()[j][j].filter(p => i[j].map(k => k._id).includes(p._id)) : [];
          }
          return result;
        });
        return result;
      });
      dispatch({
        type: GET_MALADIE,
        payload,
      });
    })
    .catch(err =>
      dispatch({
        type: GET_MALADIE,
        payload: null,
      }),
    );
};

// Add Post
export const updateMaladie = maladieData => (dispatch) => {
  dispatch(clearErrors());
  axios
    .put(`${config.baseUrl}/api/maladie/${maladieData.id}`, maladieData)
    .then(() => dispatch(getMaladie()))
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response ? err.response.data : err,
      }),
    );
};

// Add Post
export const addMaladie = maladieData => (dispatch) => {
  dispatch(clearErrors());
  axios
    .post(`${config.baseUrl}/api/maladie`, maladieData)
    .then(() => dispatch(getMaladie()))
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response,
      }),
    );
};

// Get Post
export const postMaladie = id => (dispatch) => {
  dispatch(setMaladiesLoading());
  axios
    .get(`${config.baseUrl}/api/maladie/${id}`)
    .then(res =>
      dispatch({
        type: GET_ONE_MALADIE,
        payload: res.data,
      }),
    )
    .catch(err =>
      dispatch({
        type: GET_ONE_MALADIE,
        payload: null,
      }),
    );
};

// Delete Maladies
export const deleteMaladie = id => (dispatch) => {
  axios
    .delete(`${config.baseUrl}/api/maladie/${id}`)
    .then(res =>
      dispatch({
        type: DELETE_MALADIE,
        payload: id,
      }),
    )
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response ? err.response.data : err,
      }),
    );
};
