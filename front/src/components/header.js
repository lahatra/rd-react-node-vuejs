import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

import mapStateToProps from '../redux/mapStateToProps';
import mapDispatchToProps from '../redux/mapDispatchToProps';

class Header extends Component {
  onLogoutClick(e) {
    e.preventDefault();
    this.props.logoutUser();
    this.props.history && this.props.history.push('/authentification');
  }

    toogleMenu = () => {
      const x = document.getElementById('myTopnav');
      if (x.className === 'topnav') {
        x.className += ' responsive';
      } else {
        x.className = 'topnav';
      }
    }

    render() {
      const { isAuthenticated, user } = this.props.auth;

      const authLinks = (
        <ul className="navbar-nav ml-auto">
          <li className="nav-item">
            <a
              href=""
              onClick={this.onLogoutClick.bind(this)}
              className="nav-link"
            >
              <img
                className="rounded-circle"
                src={user.avatar}
                alt={user.name}
                style={{ width: '25px', marginRight: '5px' }}
                title="You must have a Gravatar connected to your email to display an image"
              />{' '}
                Logout
            </a>
          </li>
        </ul>
      );

      const guestLinks = (
        <ul className="navbar-nav ml-auto">
          <li className="nav-item">
            <Link className="nav-link" to="/register">
                  Sign Up
            </Link>
          </li>
          <li className="nav-item">
            <Link className="nav-link" to="/authentification">
                  Login
            </Link>
          </li>
        </ul>
      );

      return (
        <div className="topnav" id="myTopnav">
          {/* <a href="http://test.taleta.net/login" className="active">Connexion</a> */}
          <Link className="navbar-brand" to="/accueil">
            Accueil
          </Link>
          {/* <a href="http://test.taleta.net/register">Inscription</a>
          <a href="http://test.taleta.net">A propos</a> */}
          <div className="collapse navbar-collapse" id="mobile-nav">
            {isAuthenticated ? authLinks : guestLinks}
          </div>
          <a href="" className="icon" onClick={this.toogleMenu} >
            <i className="fa fa-bars" />
          </a>
        </div>
      );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Header);
