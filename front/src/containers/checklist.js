import React, { Component } from 'react';
import { connect } from 'react-redux';
import Header4 from '../components/header4';

import mapStateToProps from '../redux/mapStateToProps';
import mapDispatchToProps from '../redux/mapDispatchToProps';

class Accueil extends Component {
  constructor(props) {
    super(props);
    this.state = {
      checklistList: [],
    };
  }

  async componentWillMount() {
    await this.props.getChecklistUser();
    await this.props.getQuestions();
    // await this.props.getData();
    const { checklistUser } = this.props.checklist;
    console.log('checklistUser', checklistUser);
    this.setState({ checklistList: checklistUser || [] });
  }

  login = () => {
    console.log('login'); //
  }

  toggleChange = (item, k) => {
    let list = [];
    alert(item.commentaire || item.description);
    if (this.isChecked(item, k)) {
      list = this.state[`${k}List`].filter(key => key._id !== item._id);
    } else {
      list = [...this.state[`${k}List`], item];
    }
    this.props.addLikeChecklist(list);
    this.setState({ [`${k}List`]: list });
  }

  isChecked = (item, k) => this.state[`${k}List`].filter(key => key._id === item._id).length > 0

  render() {
    const { checklists, checklistUser } = this.props.checklist;

    console.log(checklistUser, 'checklists', this.state.checklistList);

    return (
      <div>
        <Header4/>
        <div className="checklist-page">
          <div id="titre">
            <div className="element">
              <label className="title">Ma Checklist </label>
            </div>
          </div>
          <div className="form">
            <form className="login-form"action="#">
              <table>
                <tbody>
                  {
                    checklists && checklists.map(item => (
                      <tr key={item._id}>
                        <td><label>
                          <input
                            type="checkbox"
                            className="ios-switch"
                            checked={this.isChecked(item, 'checklist')}
                            onChange={() => this.toggleChange(item, 'checklist')}
                          /><div><div /></div></label></td>
                        <td><label type="description">{item.description || item.intitule}</label></td>
                      </tr>
                    ))
                  }
                  {/* <tr>
                    <td><label><input type="checkbox" className="ios-switch" /><div><div /></div></label></td>
                    <td><label type="description">Rendez-vous chez le médecin</label></td>
                  </tr>
                  <tr>
                    <td><label><input type="checkbox" className="ios-switch" /><div><div /></div></label></td>
                    <td><label type="description">Passeport</label></td>
                  </tr>
                  <tr>
                    <td><label><input type="checkbox" className="ios-switch" /><div><div /></div></label></td>
                    <td><label type="description">Visa</label></td>
                  </tr>
                  <tr>
                    <td><label><input type="checkbox" className="ios-switch" /><div><div /></div></label></td>
                    <td><label type="description">Billet d'avion</label></td>
                  </tr>
                  <tr>
                    <td><label><input type="checkbox" className="ios-switch" /><div><div /></div></label></td>
                    <td><label type="description">Hebergement</label></td>
                  </tr>
                  <tr>
                    <td><label><input type="checkbox" className="ios-switch" /><div><div /></div></label></td>
                    <td><label type="description">Transport</label></td>
                  </tr>
                  <tr>
                    <td>
                      <label className="description"> <strong>4 Juin Date limite pour les vaccin </strong></label>
                    </td>
                  </tr>
                  <tr>
                    <td><label><input type="checkbox" className="ios-switch" /><div><div /></div></label></td>
                    <td><label type="description">Liste des courses</label></td>
                  </tr>
                  <tr>
                    <td><label><input type="checkbox" className="ios-switch" /><div><div /></div></label></td>
                    <td><label type="description">Moyen de Paiement</label></td>
                  </tr>
                  <tr>
                    <td><label><input type="checkbox" className="ios-switch" /><div><div /></div></label></td>
                    <td><label type="description">Souscrire à une assurance santé</label></td>
                  </tr>
                  <tr>
                    <td><label><input type="checkbox" className="ios-switch" /><div><div /></div></label></td>
                    <td><label type="description">Enregistrer les documents importants</label></td>
                  </tr>
                  <tr>
                    <td><label><input type="checkbox" className="ios-switch" /><div><div /></div></label></td>
                    <td><label type="description">Adapteur international</label></td>
                  </tr>
                  <tr>
                    <td><label><input type="checkbox" className="ios-switch" /><div><div /></div></label></td>
                    <td><label type="description">Applications Utiles</label></td>
                  </tr> */}
                  <tr><td>	<label type="description">	<strong>14 Juin Départ</strong> </label></td></tr>
                </tbody>
              </table>
            </form>

          </div>
        </div>
      </div>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Accueil);
