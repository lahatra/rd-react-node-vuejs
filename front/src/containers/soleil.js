import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Header4 from '../components/header4';

class Boiremanger extends Component {
	accueil = () => {
		this.props.history.push('/accueil');
	}
// style="padding-top: 120px;"
  render() {
    return (

<div>
	<Header4/>
<div className="sante-page">				
			   <div id="titre">
					<div className="element">
						<label className="title">Soleil</label>
					</div>
				</div>
	     <div className="group">
				<label className="description"> En résumé : Je prévois et j’utilise : 
											<br>
											</br>
								- Des vêtements amples et couvrants, qui protègent aussi très bien des moustiques,
								<br>
											</br>
								- Une crème solaire adaptée,
								<br>
											</br>
								- Une casquette ou un chapeau, des lunettes de soleil,
								<br>
											</br>
								- Et de l’eau !
								<br>
											</br>
			</label>
			</div>
<div id="conteneur">
<div className="element">
<img src="../img/protection_soleil.png" className="image" alt="protection soleil" />
</div>
</div>			

<div className="tab blue">
				<input id="tab-four" type="radio" name="tabs2" />
					<label htmlFor="tab-four">Quelles sont les risques ?</label>
				<div className="tab-content">
				<p>Coup de soleil : c'est une brûlure (du second degré s'il y a des cloques/perte de peau), ni plus ni moins ! 
<br>
</br>
<br>
</br>
Coup de chaleur/insolation : différent du coup de soleil, ceci arrive lorsqu'une personne n'arrive plus à compenser l'augmentation de la température de son corps, généralement à cause de la chaleur ambiante, plus ou moins associée à une déshydratation, l’activité physique, le manque de protection solaire, etc... C'est l'une des causes les plus fréquentes de maux de tête/coup de fatigue en voyage après une longue journée, tout comme la déshydratation !
<br></br><br>
</br>

Le soleil brûle aussi les yeux, et là c’est souvent irréversible !
<br>
</br><br>
</br>
Le rayonnement solaire favorise aussi l’apparition de cancer de la peau, et accessoirement accélère le vieillissement de la peau.
<br>
</br><br>
</br>
Les risques sont donc multiples et de gravité variable. Ils se cumulent en particulier durant les voyages : plus de soleil, plus d’activité physique (baignades, longues journées de visites, etc…), obligation d’anticiper les besoins en eau quand l’eau n’est pas potable, etc…</p>
	<p><img src="../img/risque_soleil.png" className="image_toggle" alt="soleil"/></p>
 
				</div>
			</div>
		<div className="tab blue">
				<input id="tab-five" type="radio" name="tabs2"/>
					<label htmlFor="tab-five">Les risques d'infections</label>
			<div className="tab-content">
				<p>La meilleure protection est constituée par des vêtements amples et couvrants, au moins au niveau des zones les plus sensibles, souvent les épaules et le dos ; et d’éviter les expositions lors des heures de la journée où le rayonnement est le plus intense (entre 12 et 16h). 

En plus, je peux porter une casquette ou chapeau, et surtout des lunettes de soleil pour protéger mes yeux. J’achète mes lunettes de soleil dans un magasin fiable, qui ne risque pas d’avoir trafiqué le marquage CE. Ce dernier signifie que les lunettes respectent les obligations de fabrication européennes qui définissent le niveau de protection de 0 (pas de protection UV – confort uniquement) à 4 (protection maximale). La protection contre les UV n’est pas liée à la couleur du verre ! Je peux choisir le niveau de protection des lunettes. Le niveau 3 est un bon compromis. Il permet une bonne </p>
<br>
</br>
<p>
protection, tout comme le niveau 4, mais ce dernier n’est pas adapté pour la conduite automobile et les usagers de la route. 
</p>
<br></br>
<p>
Si je ne conduis pas, une paire de lunettes niveau 4 est idéale à la plage ou à la montagne.

J’utilise une crème solaire d’indice adapté sur toutes les zones non couvertes et en applique régulièrement, surtout après les baignades ou même si je transpire beaucoup.

Plus il fait chaud, plus je m’hydrate ! (Rappel : l’alcool déshydrate 😊 )


</p>
			</div>
		</div>
		<div className="tab blue">
				<input id="tab-six" type="radio" name="tabs2"/>
					<label htmlFor="tab-six">Comment choisir sa crème solaire ? </label>
				<div className="tab-content">
					<p>Le choix de la crème solaire dépend essentiellement du niveau d’ensoleillement et de la sensibilité de la </p>
				<p>peau au soleil (ou phototype). En effet, plus on se rapproche des tropiques ou des sommets de montagne, plus l’intensité du soleil est forte ; quelque soit la température. Et nous sommes tous inégaux concernant la réaction de notre peau devant le soleil.
Pour faire simple, plus on a la peau claire, plus on est sensible au soleil, plus l’indice de protection doit être élevé. 
Plusieurs choses peuvent aussi diminuer l’efficacité de la crème solaire : évidemment la baignade, la transpiration, mais aussi l’utilisation de produits anti-moustique (qui doit être appliqué au moins 20 min après la crème solaire). De plus, les conditions d’utilisation réelle de la crème sont généralement très différentes de celles prévues dans le mode d’emploi. Si vous regardez par curiosité le mode d’emploi de votre crème, vous vous rendrez compte que pour obtenir l’indice de protection souhaité, les quantités de crèmes à appliquer sont nettement plus importantes que ce que vous imaginez. Il faudrait, pour respecter la dose conseillée par le fabricant, étaler un volume de crème équivalent à celui d'une balle de ping-pong pour protéger un adulte… Par rapport à la réalité, on peut donc déjà diviser l’indice de protection par deux.

Dernière chose, n’oubliez pas qu’une crème solaire à une date de péremption, elle n’est donc plus efficace après cette date. 

Finalement, quel indice choisir ? À savoir, l’écran total n’existe pas et est une appellation désormais interdite en France car aucune crème ne bloque 100% des rayons UV, même appliquée selon le mode d’emploi. En résumé et vu le nombre de facteurs qui diminuent l’indice réel d’une crème, dans des conditions d’ensoleillement importantes (et quel que soit la température), je choisis une crème solaire indice de protection 50 SPF (et minimum 30 SPF si j’ai la peau très foncée, car une peau foncée protège bien des coups de soleils mais peu du risque de cancer). 
</p>
				</div>
		</div>
		
		<div className="group">
	<div className="footer">
				<Link to="/info">
					<img src="../img/info.png" className="image_footer" alt="Infos" />
				</Link>
				<Link to="/moustique">
					<img src="../img/moustique.png" className="image_footer" alt="Moustique"/>				</Link>
				<Link to="/soleil">
					<img src="../img/soleil.png" className="image_footer" alt="soleil2"/>
				</Link>
				<Link to="/baignade">
					<img src="../img/baignade.png" className="image_footer" alt="baignade"/>
				</Link>
				<Link to="/animaux">
					<img src="../img/animaux.png" className="image_footer" alt="renard"/>
				</Link>
				<Link to="/quiz">
				<img src="../img/quizz.png" className="image_footer" alt="Transport"/>
				</Link>
	</div>	</div>
	</div>
	</div>
		

    );
  }
}

export default Boiremanger;
