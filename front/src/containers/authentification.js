import React, { Component } from 'react';
import { connect } from 'react-redux';

import mapStateToProps from '../redux/mapStateToProps';
import mapDispatchToProps from '../redux/mapDispatchToProps';
import Header2 from '../components/header2';


class Authentification extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
      error: false,
      pays: [],
    };
  }

  componentWillMount() {
    // this.getPays()
  }

  cb = () => {
    this.props.getVoyage();
    this.props.getChecklistUser();
    this.props.getSecoursUser();
    this.props.getPays();
    this.props.history.push('/premiervoyage');
  }

  login = () => {
    const userData = {
      email: this.state.email,
      password: this.state.password,
    };

    this.props.loginUser(userData, this.cb);
  }

  render() {
    return (
      <div>
        <Header2 />
        <div className="login-page">
          <div id="titre">
            <div className="element">
              <h1>Urgence: </h1>
            </div>
          </div>
          <div id="conteneur">
            <div className="element">
              <img src="../img/policeman.png" className="image-auth"alt="urgence-police-travelkit" />
            </div>
            <div className="element">
              <img src="../img/cardio.png" className="image-auth" alt="urgence-police-travelkit" />
            </div>
          </div>
          <div className="form">
            <form className="login-form">
              <input
                value={this.state.email}
                onChange={e => this.setState({ email: e.target.value, error: false })}
                type="text"
                placeholder="Identifiant"
              />
              <input
                value={this.state.password}
                onChange={e => this.setState({ password: e.target.value, error: false })}
                type="password"
                placeholder="Mot de passe"
              />
            </form>
            <button onClick={this.login}>Connexion</button>
          </div>
        </div>
      </div>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Authentification);
