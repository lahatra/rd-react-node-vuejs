import React, { Component } from 'react';
import { connect } from 'react-redux';

import mapStateToProps from '../redux/mapStateToProps';
import mapDispatchToProps from '../redux/mapDispatchToProps';

import Header3 from '../components/header3';

class Accueil extends Component {
  constructor(props) {
    super(props);
  }
    madestination = () => {
      this.props.history.push('/madestination');
    }
    profilsante = () => {
      this.props.history.push('/profilsante');
    }
      checklist = () => {
        this.props.history.push('/checklist');
      }
      render() {
        return (
          <div>
            <Header3 />
            <div className="accueil-page">
              <div id="titre">
                <div className="element">
                  <h1>Travelkit</h1>
                </div>
              </div>
              <div id="conteneur-nouveau-voyage">
                <div className="element">
                  <button onClick={this.madestination} id="nouveau-voyage" type="submit">Nouveau Voyage</button>
                </div>
              </div>
              <div className="group" onClick={this.checklist}>
                <div className="sante">
                  <img src="../img/checklist.png" className="nvimage1" alt="sante" />
                  <span className="text">Ma Checklist</span>
                </div>
              </div>
              <div className="group" onClick={this.profilsante}>
                <div className="sante">
                  <img src="../img/stethoscope_blue.png" className="image" alt="Vaccins" />
                  <span className="text">Mon profil Santé</span>
                </div>
              </div>
              <div className="group" onClick={() => {}}>
                <div className="sante">
                  <img src="../img/placeholder.png" className="image" alt="Vaccins" />
                  <span className="text">Mon Quotidien</span>
                </div>
              </div>
              <div className="group" onClick={() => {}}>
                <div className="sante">
                  <img src="../img/passport_blue.png" className="image" alt="Vaccins" />
                  <span className="text">Mes Documents  </span>
                </div>
              </div>
              <div className="group" onClick={() => {}}>
                <div className="sante">
                  <img src="../img/arrivals_blue.png" className="image" alt="Vaccins" />
                  <span className="text">Au retour</span>
                </div>
              </div>
            </div>
          </div>
        );
      }
}

export default connect(mapStateToProps, mapDispatchToProps)(Accueil);
