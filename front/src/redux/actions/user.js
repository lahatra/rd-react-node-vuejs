import { get, post } from '../api';

export const LOGIN = 'LOGIN';
export const REGESTER = 'REGESTER';
export const ADD_VOYAGES = 'ADD_VOYAGES';
export const GET_VOYAGES = 'GET_VOYAGES';
export const ADD_MASANTE = 'ADD_MASANTE';
export const GET_MASANTE = 'GET_MASANTE';
export const ISCONNECTED = 'ISCONNECTED';


export const login1 = payload => dispatch => dispatch({ type: LOGIN, payload });

// Add Post

export const login = (payload, cb) =>
  async (dispatch) => {
    const result = await post('login', payload);
    if (result && result.data && result.data.user) {
      dispatch({ type: LOGIN, payload: result.data.user });
      cb();
    }
  };

export const register = (payload, cb = () => {}) =>
  async (dispatch) => {
    const result = await post('register', payload);
    if (result && result.data && result.data.user) {
      dispatch({ type: LOGIN, payload: result.data.user });
      cb();
    }
  };

export const addVoyages = payload =>
  async (dispatch) => {
    const result = await post('add-voyages', payload);
    dispatch({ type: ADD_VOYAGES, payload });
  };

export const addMasante = payload =>
  async (dispatch) => {
    const result = await post('masante', payload);
    dispatch({ type: ADD_MASANTE, payload });
  };

export const getVoyages = payload =>
  async (dispatch) => {
    const result = await get(`voyages?id=${payload.user_id}`, payload);
    console.log('getVoyages');
    dispatch({ type: GET_VOYAGES, payload });
  };

export const getMasante = payload =>
  async (dispatch) => {
    const result = await get(`get-masante?id=${payload.user_id}`, payload);
    console.log('get-masante');
    dispatch({ type: GET_MASANTE, payload });
  };
