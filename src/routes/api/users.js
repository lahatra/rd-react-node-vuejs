
// Load Input Validation
const validateRegisterInput = require('../../validation/register');
const validateLoginInput = require('../../validation/login');

// Load User model
const User = require('../../models/User');
const Location = require('../../models/Location');

const express = require('express');

const router = express.Router();
const gravatar = require('gravatar');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

const passport = require('passport');

// @route   GET api/users/test
// @desc    Tests users route
// @access  Public
router.get('/test', (req, res) => res.json({ msg: 'Users Works' }));

// @route   POST api/users/register
// @desc    Register user
// @access  Public
router.post('/register', (req, res) => {
  const { errors, isValid } = validateRegisterInput(req.body);

  // Check Validation
  if (!isValid) {
    return res.status(400).json(errors);
  }

  User.findOne({ email: req.body.email }).then((user) => {
    if (user) {
      errors.email = 'Email already exists';
      return res.status(400).json(errors);
    }
    const avatar = gravatar.url(req.body.email, {
      s: '200', // Size
      r: 'pg', // Rating
      d: 'mm', // Default
    });

    const newUser = new User({
      name: req.body.name,
      email: req.body.email,
      avatar,
      password: req.body.password,
    });

    return bcrypt.genSalt(10, (err, salt) => {
      bcrypt.hash(newUser.password, salt, (err, hash) => {
        if (err) throw err;
        newUser.password = hash;
        newUser
          .save()
          .then(user => res.json(user))
          .catch(e => console.log(e));
      });
    });
  });
});

// @route   GET api/users/login
// @desc    Login User / Returning JWT Token
// @access  Public
router.post('/login', (req, res) => {
  const { errors, isValid } = validateLoginInput(req.body);

  // Check Validation
  if (!isValid) {
    return res.status(400).json(errors);
  }

  const email = req.body.email;
  const password = req.body.password;
  // Find user by email
  User.findOne({ email }).then((user) => {
    // Check for user
    if (!user) {
      errors.email = 'User not found';
      return res.status(404).json(errors);
    }
    // Check Password
    return bcrypt.compare(password, user.password).then((isMatch) => {
      if (isMatch) {
        // User Matched
        const payload = { id: user.id, name: user.name, avatar: user.avatar, vaccin: user.vaccin }; // Create JWT Payload
        console.log('payload', payload);
        // Sign Token
        jwt.sign(
          payload,
          'jwt',
          { expiresIn: 360000 },
          (err, token) => {
            if (err) {
              res.json(payload);
            }
            console.log('err err', err);
            console.log('token token', token);
            res.json({
              success: true,
              token: `Bearer ${token}`,
            });
          },
        );
      } else {
        errors.password = 'Password incorrect';
        return res.status(400).json(errors);
      }
    });
  }).catch(err => res.status(400).json(err));
});

// @route   POST api/posts/comment/:id
// @desc    Add comment to post
// @access  Private
router.put(
  '/:id',
  // // passport.authenticate('jwt', { session: false }),
  (req, res) => {
    User.findById(req.params.id)
      .then((post) => {
        // Add to comments array

        post.vaccin = req.body.vaccin || [];

        // Save
        post.save()
          .then(post => res.json(post))
          .catch(() => res.status(400).json({ errors: 'No found' }));
      })
      .catch(() => res.status(404).json({ postnotfound: 'No post found' }));
  },
);

// @route   POST api/posts/comment/:id
// @desc    Add comment to post
// @access  Private
router.post(
  'upload/:id',
  // // passport.authenticate('jwt', { session: false }),
  (req, res) => {
    User.findById(req.params.id)
      .then((post) => {
        // Add to comments array
        console.log('req.files req.files', req.files);

        if (Object.keys(req.files).length === 0) {
          res.status(400).send('No files were uploaded.');
          return;
        }

        console.log('req.files >>>', req.files); // eslint-disable-line

        const sampleFile = req.files.sampleFile;

        const uploadPath = `${__dirname}/uploads/${sampleFile.name}`;

        sampleFile.mv(uploadPath, (err) => {
          if (err) {
            return res.status(500).send(err);
          }

          res.send(`File uploaded to ${uploadPath}`);
        });
      })
      .catch(() => res.status(404).json({ postnotfound: 'No post found' }));
  },
);

// @route   GET api/users/current
// @desc    Return current user
// @access  Private
router.get(
  '/current',
  // passport.authenticate('jwt', { session: false }),
  (req, res) => {
    res.json({
      id: req.user.id,
      name: req.user.name,
      email: req.user.email,
    });
  },
);

router.get('/', (req, res) => {
  User.find()
    .then(posts => res.json(posts))
    .catch(err => res.status(404).json({ nopostsfound: 'No posts found' }));
});

router.post(
  '/location',
  (req, res) => {
    const newCentre = new Location({
      latitude: req.body.latitude,
      longitude: req.body.longitude,
      heure: req.body.heure,
    });

    newCentre.save().then(post => res.json(post));
  },
);

// @route   GET api/users/current
// @desc    Return current user
// @access  Private
router.get(
  '/data:id',
  // passport.authenticate('jwt', { session: false }),
  (req, res) => {
    User.findById(req.params.id)
      .then((post) => {
        // Add to comments array
        const data = {
          secours: post.secours,
          checklist: post.checklist,
        };
        res.json(data);
      })
      .catch(() => res.status(404).json({ postnotfound: 'No post found' }));
  },
);

// module.exports = router;
module.exports = router;
