import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Modal from 'react-responsive-modal';
import MedecinForm from './MedecinForm';
import MedecinFeed from './MedecinFeed';
import Spinner from '../common/Spinner';
import { getMedecin } from '../../actions/MedecinActions';

class Medecin extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      data: {}
    };
  }

  onOpenModal = () => {
    this.setState({ open: true, data: {} });
  };

  onUpdate = data => {
    this.setState({ open: true, data });
  };
 
  onCloseModal = () => {
    this.setState({ open: false });
  };

  componentDidMount() {
    this.props.getMedecin();
  }

  render() {
    const { medecin, loading } = this.props.medecin;
    const { open, data } = this.state;
    let medecinContent;
    console.log('v ;!sd v!s!cv !xc!vcx', medecin);
    if (medecin === null || loading) {
      medecinContent = <Spinner />;
    } else {
      medecinContent = <MedecinFeed medecin={medecin} onUpdate={this.onUpdate} />;
    }

    return (
      <div className="feed">
        <div className="container">
          <div className="row">
            <div className="col-md-12">
            <button onClick={this.onOpenModal} type="submit" className="btn btn-dark">
                  Ajout
                </button>
              {medecinContent}
            </div>
          </div>
        </div>
        <Modal open={open} onClose={this.onCloseModal} center>
          <MedecinForm onClose={this.onCloseModal} data={data} />
        </Modal>
      </div>
    );
  }
}

Medecin.propTypes = {
  getMedecin: PropTypes.func.isRequired,
  medecin: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  medecin: state.medecin
});

export default connect(mapStateToProps, { getMedecin })(Medecin);
