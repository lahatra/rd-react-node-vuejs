import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';

import mapStateToProps from '../redux/mapStateToProps';
import mapDispatchToProps from '../redux/mapDispatchToProps';

class Header4 extends Component {
  goBack = () => (this.props.history ? this.props.history.goBack() : {})

  render() {
    return (
      <div className="entete" id="entete">
        <div className="logo" onClick={() => this.props.history ? this.props.history.push('/accueil') : {}}><img src="../img/logo.png" className="image"alt="logo-travelkit" /></div>
        <div className="retour" onClick={this.goBack}>
          {/* <a href="http://test.taleta.net/login" className="active"> */}
          <img src="../img/back-arrow.png" className="image"alt="logo-travelkit" />
          {/* </a> */}
        </div>

      </div>

    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Header4));
// export default withRouter(Header4);
