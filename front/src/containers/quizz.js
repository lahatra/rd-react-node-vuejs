import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Header4 from '../components/header4';

class Quizz extends Component {

	
	render() {
	  return (
		  <div>
			  <Header4/>
		  	<div className="sante-page">	
			   <div id="titre">
				<div className="element">
					<label className="title">Quizz</label>
				</div>
			</div>
	     <div className="group">
			<label className="description">Le moustique qui transmet la Dengue
pique :</label>
			<div className="form-quizz">
			<button onClick={this.login}><label className="description-quizz">Le jour</label> </button>
			<button onClick={this.login}><label className="description-quizz">La nuit </label></button>
			<button onClick={this.login}><label className="description-quizz">Les deux, mais surtout le jour</label></button>
			<button onClick={this.login}><label className="description-quizz">Les deux, mais surtout la nuit</label></button>
			<button onClick={this.login}><label className="description-quizz">La Dengue n'est pas transmise par un moustique</label></button>
			</div>
			<div id="conteneur2">
			<div className="button_confirm">
                        <button onClick={this.go} className="yes"> OUI </button>
                        <button onClick={this.go} className="no"> NON </button>
                    </div> 
			</div>

 
	<div className="footer">
				<Link to="/info">
					<img src="../img/info.png" className="image_footer" alt="Infos" />
				</Link>
				<Link to="/moustique">
					<img src="../img/moustique.png" className="image_footer" alt="Moustique"/>				</Link>
				<Link to="/soleil">
					<img src="../img/soleil.png" className="image_footer" alt="soleil2"/>
				</Link>
				<Link to="/baignade">
					<img src="../img/baignade.png" className="image_footer" alt="baignade"/>
				</Link>
				<Link to="/animaux">
					<img src="../img/animaux.png" className="image_footer" alt="renard"/>
				</Link>
				<Link to="/quiz">
				<img src="../img/quizz.png" className="image_footer" alt="Transport"/>
				</Link>
	</div>	
	
	</div>
			</div>
			</div>
	  );
	}
  }
  
  export default Quizz;