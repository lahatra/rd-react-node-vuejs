import axios from 'axios';

import {
  ADD_SECOURS,
  GET_ERRORS,
  CLEAR_ERRORS,
  GET_SECOURS,
  SECOURS_LOADING,
  DELETE_SECOURS,
  UPDATE_SECOURS,
  ADD_USER_SECOURS,
} from './types';
import config from '../utils/config';

export const addUserSecours = payload => ({
  type: ADD_USER_SECOURS,
  payload,
});

// Get all profiles
export const getSecoursUser = () => (dispatch, getState) => {
  const id = getState().auth.user ? getState().auth.user.id : null;
  if (!id) return;
  axios
    .get(`${config.baseUrl}/api/secours/user${id}`)
    .then((res) => {
      if (res && res.data) {
        dispatch(addUserSecours(res.data.secours));
      }
    })
    .catch((err) => {});
};

// Add Post
export const updateSecours = SECOURSData => (dispatch) => {
  dispatch(clearErrors());
  axios
    .put(`${config.baseUrl}/api/secours/${SECOURSData.id}`, SECOURSData)
    .then(res =>
      dispatch({
        type: UPDATE_SECOURS,
        payload: res.data,
      }),
    )
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data,
      }),
    );
};

// Add Like
export const addLikeSecours = Secours => (dispatch, getState) => {
  const user = getState().auth.user ? getState().auth.user.id : null;
  if (!user) return;
  axios
    .put(`${config.baseUrl}/api/secours/like-unlink/${user}`, Secours)
    .then(() => {
      dispatch(getSecoursUser());
    })
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response ? err.response.data : err,
      }),
    );
};

// Add Post
export const addSecours = SECOURSData => (dispatch) => {
  dispatch(clearErrors());
  axios
    .post(`${config.baseUrl}/api/secours`, SECOURSData)
    .then(res =>
      dispatch({
        type: ADD_SECOURS,
        payload: res.data,
      }),
    )
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data,
      }),
    );
};

// Get SECOURS
export const getSecours = () => (dispatch) => {
  dispatch(setSecoursLoading());
  axios
    .get(`${config.baseUrl}/api/secours`)
    .then(res =>
      dispatch({
        type: GET_SECOURS,
        payload: res.data,
      }),
    )
    .catch(err =>
      dispatch({
        type: GET_SECOURS,
        payload: null,
      }),
    );
};

// Get Post
export const postSecours = id => (dispatch) => {
  dispatch(setSecoursLoading());
  axios
    .get(`${config.baseUrl}/api/secours/${id}`)
    .then(res =>
      dispatch({
        type: GET_SECOURS,
        payload: res.data,
      }),
    )
    .catch(err =>
      dispatch({
        type: GET_SECOURS,
        payload: null,
      }),
    );
};

// Delete SECOURS
export const deleteSecours = id => (dispatch, getState) => {
  const user = getState().auth.user ? getState().auth.user.id : null;
  if (!user) return;
  axios
    .delete(`${config.baseUrl}/api/secours/${user}`, id)
    .then(res =>
      dispatch({
        type: DELETE_SECOURS,
        payload: id,
      }),
    )
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data,
      }),
    );
};


// Set loading state
export const setSecoursLoading = () => ({
  type: SECOURS_LOADING,
});

// Clear errors
export const clearErrors = () => ({
  type: CLEAR_ERRORS,
});
