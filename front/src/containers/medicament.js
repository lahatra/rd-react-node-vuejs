import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Header4 from '../components/header4';

class Medicament extends Component {
  login = () => {
    console.log("login");
  }
// style="padding-top: 120px;"
  render() {
    return (

<div>
	<Header4/>
<div className="sante-page">				
			   <div id="titre">
					<div className="element">
						<label className="title">Medicament</label>
					</div>
				</div>
	     <div className="group">
				<label className="description"> Un traitement médicamenteux préventif contre le paludisme est nécessaire durant votre voyage.
			</label>
			</div>		

<div className="tab blue">
				<input id="tab-four" type="radio" name="tabs2" />
					<label htmlFor="tab-four">Comment voyager en avion 
avec des médicaments ?</label>
				<div className="tab-content">
				<p>
				Les contrôles douaniers sont rares, mais ils peuvent être exigeants. Si j’ai un traitement médicamenteux personnel à prendre pendant le voyage, les médicaments doivent être dans leur emballage d’origine et accompagné d’une ordonnance. Il est préférable de vérifier avec mon médecin traitant que l’ordonne mentionne bien la Dénomination Commune Internationale de chaque médicament (et pas uniquement le nom de marque d’un laboratoire en particulier).
Il est fortement conseillé de conserver une partie au moins du traitement dans son bagage à main. Ainsi, en cas de perte du bagage en soute, je ne risque pas de manquer d’un médicament important. 
Pour cela, il faut que l’ordonnance mentionne que les médicaments doivent impérativement être conservé sur soi en permanence. Il est aussi conseillé de préciser cette mention en anglais pour ne pas avoir de problème au voyage retour : « must be carried at all times ».

				</p>	
				</div>
			</div>
		<div className="tab blue">
				<input id="tab-five" type="radio" name="tabs2"/>
					<label htmlFor="tab-five">Chimioprophylaxie antipaludique</label>
			<div className="tab-content">
				<p>La meilleure protection est constituée par des vêtements amples et couvrants, au moins au niveau des zones les plus sensibles, souvent les épaules et le dos ; et d’éviter les expositions lors des heures de la journée où le rayonnement est le plus intense (entre 12 et 16h). 

En plus, je peux porter une casquette ou chapeau, et surtout des lunettes de soleil pour protéger mes yeux. J’achète mes lunettes de soleil dans un magasin fiable, qui ne risque pas d’avoir trafiqué le marquage CE. Ce dernier signifie que les lunettes respectent les obligations de fabrication européennes qui définissent le niveau de protection de 0 (pas de protection UV – confort uniquement) à 4 (protection maximale). La protection contre les UV n’est pas liée à la couleur du verre ! Je peux choisir le niveau de protection des lunettes. Le niveau 3 est un bon compromis. Il permet une bonne </p>
<br>
</br>
<p>
protection, tout comme le niveau 4, mais ce dernier n’est pas adapté pour la conduite automobile et les usagers de la route. 
</p>
<br></br>
<p>
Si je ne conduis pas, une paire de lunettes niveau 4 est idéale à la plage ou à la montagne.

J’utilise une crème solaire d’indice adapté sur toutes les zones non couvertes et en applique régulièrement, surtout après les baignades ou même si je transpire beaucoup.

Plus il fait chaud, plus je m’hydrate ! (Rappel : l’alcool déshydrate 😊 )


</p>
			</div>
		</div>
		
		
		<div className="group">
	<div className="footer">
				<Link to="/info">
					<img src="../img/info.png" className="image_footer" alt="Infos" />
				</Link>
				<Link to="/moustique">
					<img src="../img/moustique.png" className="image_footer" alt="Moustique"/>				</Link>
				<Link to="/soleil">
					<img src="../img/soleil.png" className="image_footer" alt="soleil2"/>
				</Link>
				<Link to="/baignade">
					<img src="../img/baignade.png" className="image_footer" alt="baignade"/>
				</Link>
				<Link to="/animaux">
					<img src="../img/animaux.png" className="image_footer" alt="renard"/>
				</Link>
				<Link to="/quiz">
				<img src="../img/quizz.png" className="image_footer" alt="Transport"/>
				</Link>
	</div>	</div>
	</div>
	</div>
		

    );
  }
}

export default Medicament;
