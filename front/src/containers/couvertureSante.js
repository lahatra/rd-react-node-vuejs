import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Header4 from '../components/header4';

class couvertureSante extends Component {

  render() {
    return (    
			<div>     
				<Header4/>   	
	<div className="sante-page">				
		<div id="titre">
			<div className ="element">
			<label className="title">Couverture Santé</label>
			</div>
		</div>
		<div className="group">
		<label className="description">Je pars dans une destination hors Europe et EEE où il n’existe aucune convention avec la France. </label>
		<label className="description">Ma caisse française d'assurance maladie peut prendre en charge les soins inopinés (c'est-à-dire imprévus et imprévisibles ; hospitaliers ou ambulatoires) que j’ai reçus à l'occasion d'un séjour temporaire, lors de mes vacances ou d'un séjour professionnel ou linguistiques par exemple. Il s'agit d'une possibilité et non d'une obligation.
Si j’ai effectué l'avance des frais de soins, je peux faire une demande de remboursement en présentant à ma caisse française d'assurance maladie les factures acquittées, ainsi que la déclaration de frais (imprimé S3125).
Ma caisse française vérifiera en particulier que les conditions prévues par la réglementation française en matière de remboursement sont satisfaites. Elle peut procéder, s'il y a lieu, au remboursement forfaitaire des soins dispensés, sur la base des tarifs français de la sécurité sociale.
Mon organisme d'assurance maladie complémentaire (mutuelle, assurance, institution de prévoyance) peut éventuellement compléter ce remboursement.
		 </label>
		  <label className="description">
		<strong>
		Il s'agit d'une possibilité et non d'une obligation.
		</strong>
		 </label>
		 <label className="description">
		 <Link to="">Assurance et assistance aux voyageurs   </Link>
		 </label>
		 
		<label className="description">

Il est prudent de souscrire une assurance voyage spécifique ou de m’assurer que je dispose déjà d'une telle assurance, par exemple avec votre assurance automobile ou liée à ma carte bancaire ; dans ce dernier cas il faut vérifier dans quelles conditions cette assurance fonctionne.

		 </label>
		 <label className="description">
L’assistance aux voyageurs est différente de l’assurance maladie (remboursement des soins). Le rapatriement sanitaire est l’une des possibilités offertes par un contrat d’assistance.<br/>
- Pour les séjours <strong>inférieurs à trois mois</strong>, un contrat d’assistance inclus dans mon contrat d’assurances habitations, véhicules, ou lié aux cartes bancaires est souvent valable pour les dépenses peu importantes. Le risque est de dépassé le plafond (généralement autour de 11000 euros) en cas de prise en charge lourde dans certains pays. À titre de comparaison, 24h d’hospitalisation en France coûte entre 2000 et 3000 euros et les prix sont régulés. À l’étranger, selon le pays, le prix de l’hospitalisation peut être plus important, ou moins important mais non régulé donc fixer librement selon le patient.<br/>
- Pour les séjours <strong>supérieurs à trois mois </strong>effectifs, il est recommandé de souscrire un contrat d’assistance spécifique.
		 </label>
		<div className="group">

			<div className="footer">
				<Link to="/info">
					<img src="../img/info.png" className="image_footer" alt="Infos"/>
				</Link>
				<Link to="/moustique">
					<img src="../img/moustique.png" className="image_footer" alt="Moustique"/>
				</Link>
				<Link to="/soleil">
					<img src="../img/soleil.png" className="image_footer" alt="soleil2"/>
				</Link>
				<Link to="/baignade">
					<img src="../img/baignade.png" className="image_footer" alt="baignade"/>
				</Link>
				<Link to="/animaux">
					<img src="../img/animaux.png" className="image_footer" alt="renard"/>
				</Link>
				<Link to="/quiz">
				<img src="../img/quizz.png" className="image_footer" alt="Transport"/>
				</Link>
			</div>	
		</div>
	</div>
    </div>
		</div>
    );
  }
}
export default couvertureSante;
