import React, { Component } from 'react';
import { connect } from 'react-redux';

import mapStateToProps from '../redux/mapStateToProps';
import mapDispatchToProps from '../redux/mapDispatchToProps';

import Header3 from '../components/header3';

class Accueil extends Component {
  render() {
    return (
      <div>
				<Header3 />
        <div className="checklist-page">				
		<div id="titre">
			<div className ="element">
			<label className="title">Mon Profil Voyageur </label>
			</div>
		</div>
		<div id="conteneur">
		<div className ="element">
		<img src="../img/profile.png" className="image-profil" alt="baignade"/>
			</div>
			<div className ="element">
		<label>Anna Martine </label>
			</div>
		</div>
		<div id="conteneur">
		<div className ="element">
		<img src="../img/Afrique.png" className="image-profil" alt="baignade"/>
			</div>
			<div className ="element">
			<img src="../img/badge-premiereconnexion.png" className="image-profil" alt="baignade"/>
			</div>
			<div className ="element">
			<img src="../img/apprenti.png" className="image-profil" alt="baignade"/>
			</div>
			<div className ="element">
			<img src="../img/astronaute.png" className="image-profil" alt="baignade"/>
			</div>
		</div>
		<div className="form">
		<form className="checklistprofil"action="#">
		<table>
		<tbody>
		<tr>
			<td><label id="titre3">Mes habitudes de voyages : </label></td>
		</tr>	
			<tr>
			<td><label className="description2">Je mange souvent au restaurant  </label></td>
				<td><label><input type="checkbox" className="ios-switch" /><div><div></div></div></label></td>
				
			</tr>
		<tr>
		<td><label className="description2">Je dors souvent dans des hôtels</label></td>
		<td><label><input type="checkbox" className="ios-switch" /><div><div></div></div></label></td>
		
		</tr>
		<tr>
			<td><label id="titre3">Pour ce voyage je  prévois  : </label></td>
		</tr>
		<tr>
		<td><label className="description2">D'aller à plus de 3000m d'altitude </label></td>
		<td><label><input type="checkbox" className="ios-switch" /><div><div></div></div></label></td>
		
		</tr>
		<tr>
		<td><label className="description2">D'aller dans une de ces zones : 
Ayacucho, Jumin, Loreto, Madre
de Dios, Piura, San Martin ou 
Tumbes</label></td>
		<td><label><input type="checkbox" className="ios-switch" /><div><div></div></div></label></td>
	
		</tr>
		<tr>
		<td><label className="description2">De me baigner</label></td>
		<td><label><input type="checkbox" className="ios-switch" /><div><div></div></div></label></td>
		
		</tr>
		<tr>
		<td><label className="description2">D'aller dans une zone ensoleillée
ou sous les tropiques</label></td>
		<td><label><input type="checkbox" className="ios-switch" /><div><div></div></div></label></td>
		
		</tr>
		
		</tbody>
		</table>
		</form>

</div>
	</div>
      </div>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Accueil);
