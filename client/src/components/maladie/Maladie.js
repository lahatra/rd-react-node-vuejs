import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Modal from 'react-responsive-modal';
import { connect } from 'react-redux';
import MaladieForm from './MaladieForm';
import MaladieFeed from './MaladieFeed';
import Spinner from '../common/Spinner';
import { getMaladie } from '../../actions/MaladieActions';

class Maladie extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      data: {}
    };
  }

  componentDidMount() {
    this.props.getMaladie();
  }

  onOpenModal = () => {
    console.log('dvsdvsv')
    this.setState({ open: true, data: {} });
  };

  onUpdate = data => {
    this.setState({ open: true, data });
  };
 
  onCloseModal = () => {
    this.setState({ open: false });
  };

  render() {
    const { maladie, loading } = this.props.maladie;
    const { open, data } = this.state;
    const { vaccin } = this.props.vaccin;
    const { sejour } = this.props.sejour;
    let maladieContent;

    if (maladie === null || loading) {
      maladieContent = <Spinner />;
    } else {
      maladieContent = <MaladieFeed maladie={maladie} onUpdate={this.onUpdate} />;
    }

    return (
      <div className="feed">
        <div className="container">
          <div className="row">
            <div className="col-md-12">
            <button onClick={this.onOpenModal} type="submit" className="btn btn-dark">
              Ajout
            </button>
              {/* <MaladieForm vaccin={vaccin} maladie={maladie} sejour={sejour} /> */}
              {maladieContent}
            </div>
          </div>
        </div>
        <Modal open={open} onClose={this.onCloseModal} center>
          <MaladieForm
            onClose={this.onCloseModal}
            data={data}
            maladie={maladie}
            sejour={sejour}
            vaccin={vaccin}
          />
        </Modal>
      </div>
    );
  }
}

Maladie.propTypes = {
  getMaladie: PropTypes.func.isRequired,
  maladie: PropTypes.object.isRequired,
  vaccin: PropTypes.object.isRequired,
};

const mapStateToProps = state => ({
  maladie: state.maladie,
  vaccin: state.vaccin,
  sejour: state.sejour
});

export default connect(mapStateToProps, { getMaladie })(Maladie);
