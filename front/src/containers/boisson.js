import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Header4 from '../components/header4';

import { connect } from 'react-redux';

import mapStateToProps from '../redux/mapStateToProps';
import mapDispatchToProps from '../redux/mapDispatchToProps';

class Info extends Component {
  constructor(props) {
    super(props);
    this.state = {
      questions: [],
      paysCurrent: {},
      maladieCurrent: [],
      vaccin: {
        v1: [],
        v2: [],
      },
      i: 0,
    };
  }
  componentDidMount() {
    const { one_voyage } = this.props.voyage;
    const { pays } = this.props.pays;
    const { maladie } = this.props.maladie;
    const { vaccin } = this.props.vaccin;
    if (!pays) return;
    const paysID = pays.filter(i => one_voyage && one_voyage.pays === i._id);
    paysID && this.setState({ paysCurrent: paysID[0] });
    const maladieIDS = paysID[0].maladie && paysID[0].maladie.map(i => i._id);
    const maladieCurrent = maladie.filter(i => maladieIDS.includes(i._id));
    maladieCurrent && this.setState({ maladieCurrent });
    // if (paysID && paysID.length > 0) {
    //   const maladieIDS = paysID[0].maladie && paysID[0].maladie.map(i => i._id);
    //   const res = {
    //     v1: [],
    //     v2: [],
    //   };

    //   const vaccinDetail = maladie && maladie.reduce((acc, curr) => {
    //     if (maladieIDS && maladieIDS.includes(curr._id)) {
    //       const mIDS = curr.vaccin.map(i => i._id);
    //       const s = vaccin && vaccin.filter(i => mIDS.includes(i._id));
    //       s && acc.v1.push(...s);
    //       const mIDSugg = curr.vaccinSugg.map(i => i._id);
    //       const s2 = vaccin && vaccin.filter(i => mIDSugg.includes(i._id));
    //       s2 && acc.v2.push(...s2);
    //     }
    //     return acc;
    //   }, res);
    //   const vs = vaccinDetail ? { v1: getUnique(vaccinDetail.v1, '_id'), v2: getUnique(vaccinDetail.v2, '_id') } : [];
    //   console.log('vaccin', vs);
    //   this.setState({ vaccin: vs });
    // }
    // this.props.getSejour(maladie[0]._id);
  }

  render() {
	  const { paysCurrent } = this.state;
	  return (
  <div>
        <Header4 />
        <div className="sante-page">
      <div id="titre">
            <div className="element">
          <label className="title">Boisson </label>
        </div>
          </div>

      <div className="group">
            <p>A votre destination, l’eau du robinet disponible est {paysCurrent.eau && paysCurrent.eau.potable ? 'potable ' : 'non potable' }</p>
            {paysCurrent.eau && paysCurrent.eau.potable
              ? <p>Il n’y a donc pas de précautions particulières supplémentaires à prendre par rapport à ce qui s’appliquerait en France métropolitaine</p>
			  : <p>{paysCurrent.eau ? paysCurrent.eau.commentaire : ''}</p>}
            <label className="description">
          <h3>Quels sont les risques liés à l’eau de consommation ?</h3>
Le risque est principalement infectieux, mais l’eau peut aussi contenir des substances chimiques nocives. Cela peut être dû à tout problème à la source ou lors de l’acheminement de l’eau. On ne juge donc pas la qualité d’une eau à la beauté du robinet, même s’il est très joli. La prévention est valable pour tous !


La solution la plus simple et la plus efficace est donc de boire de l’eau en bouteille encapsulée, qui se trouve facilement partout.
          <br />
L’eau peut se retrouver sous différentes formes :
Les glaçons, qui sont déconseillés. De plus en plus souvent, on trouve dans les lieux touristiques des boissons servies avec des glaçons industriels (reconnaissables à leur forme cylindrique/en forme de doigt) faits généralement avec de l'eau purifiée ou désinfectée.
Les boissons chaudes (thé, café sans lait ou crème) et les soupes chaudes sont sans risque.
Les jus de fruits et boissons gazeuses achetés en bouteille encapsulées sont sans risque.
          <br />

Le lait doit être pasteurisé, quel que soit la forme de consommation finale : boissons chaudes contenant du lait, crèmes glacées, etc… Encore une fois, les produits industriels sont soumis à des règles plus strictes donc moins risqués.

Je peux par contre aussi me désaltérer avec des fruits frais, à condition qu’ils soient pelés. Les jus, smoothies, etc… sont possibles avec les mêmes précautions : pelés, sans eau du robinet et sans ajout de jus artisanal.

Sur place, il est toujours possible de trouver des endroits qui préparent aussi bien les classiques que les spécialités locales en suivant ces principes.

        </label>
          </div>
      <div id="conteneur">
            <div className="element">
          <img className="prise"src="../img/cheers.png" alt="prise" />
        </div>
          </div>

      <div className="group">
            <div className="footer">
          <Link to="/info">
                <img src="../img/info.png" className="image_footer" alt="Infos" />
              </Link>
          <Link to="/moustique">
                <img src="../img/moustique.png" className="image_footer" alt="Moustique" />
              </Link>
          <Link to="/soleil">
                <img src="/img/soleil.png" className="image_footer" alt="soleil2" />
              </Link>
          <Link to="/baignade">
                <img src="../img/baignade.png" className="image_footer" alt="baignade" />
              </Link>
          <Link to="/animaux">
                <img src="../img/animaux.png" className="image_footer" alt="renard" />
              </Link>
          <Link to="/quiz">
                <img src="../img/quizz.png" className="image_footer" alt="Transport" />
              </Link>
        </div>
          </div>
    </div>
      </div>

	  );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Info);
