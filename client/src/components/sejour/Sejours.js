import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Modal from 'react-responsive-modal';
import SejourForm from './SejourForm';
import SejourFeed from './SejourFeed';
import Spinner from '../common/Spinner';
import { getSejours } from '../../actions/SejourActions';

class Sejours extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      data: {}
    };
  }

  onOpenModal = () => {
    this.setState({ open: true, data: {} });
  };

  onUpdate = data => {
    this.setState({ open: true, data });
  };
 
  onCloseModal = () => {
    this.setState({ open: false });
  };

  componentDidMount() {
    this.props.getSejours();
  }

  render() {
    const { sejour, loading } = this.props.sejour;
    const { open, data } = this.state;
    let sejourContent;

    if (sejour === null || loading) {
      sejourContent = <Spinner />;
    } else {
      sejourContent = <SejourFeed sejour={sejour} onUpdate={this.onUpdate} />;
    }

    return (
      <div className="feed">
        <div className="container">
          <div className="row">
            <div className="col-md-12">
            <button onClick={this.onOpenModal} type="submit" className="btn btn-dark">
                  Ajout
                </button>
              {sejourContent}
            </div>
          </div>
        </div>
        <Modal open={open} onClose={this.onCloseModal} center>
          <SejourForm onClose={this.onCloseModal} data={data} />
        </Modal>
      </div>
    );
  }
}

Sejours.propTypes = {
  getSejours: PropTypes.func.isRequired,
  sejour: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  sejour: state.sejour
});

export default connect(mapStateToProps, { getSejours })(Sejours);
