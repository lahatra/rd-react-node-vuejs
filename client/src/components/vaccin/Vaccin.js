import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Modal from 'react-responsive-modal';

import VaccinForm from './VaccinForm';
import VaccinFeed from './VaccinFeed';
import Spinner from '../common/Spinner';
import { getVaccin } from '../../actions/VaccinActions';

class Vaccin extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      data: {}
    };
  }

  onOpenModal = () => {
    this.setState({ open: true, data: {} });
  };

  onUpdate = data => {
    this.setState({ open: true, data });
  };
 
  onCloseModal = () => {
    this.setState({ open: false });
  };
  componentDidMount() {
    this.props.getVaccin();
  }

  render() {
    const { vaccin, loading } = this.props.vaccin;
    const { open, data } = this.state;
    let vaccinContent;

    if (vaccin === null || loading) {
      vaccinContent = <Spinner />;
    } else {
      vaccinContent = <VaccinFeed vaccin={vaccin} onUpdate={this.onUpdate} />;
    }

    return (
      <div className="feed">
        <div className="container">
          <div className="row">
            <div className="col-md-12">
            <button onClick={this.onOpenModal} type="submit" className="btn btn-dark">
                  Ajout
                </button>
              {vaccinContent}
            </div>
          </div>
        </div>
        <Modal open={open} onClose={this.onCloseModal} center>
          <VaccinForm onClose={this.onCloseModal} data={data} />
        </Modal>
      </div>
    );
  }
}

Vaccin.propTypes = {
  getVaccin: PropTypes.func.isRequired,
  vaccin: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  vaccin: state.vaccin
});

export default connect(mapStateToProps, { getVaccin })(Vaccin);
