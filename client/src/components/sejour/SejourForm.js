import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import TextFieldGroup from '../common/TextFieldGroup';
import TextAreaFieldGroup from '../common/TextareaFieldGroup';
import { addSejour, updateSejour } from '../../actions/SejourActions';

class SejourForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      text: this.props.data.description || '',
      isChecked: this.props.data.isQuestion || false,
      errors: {}
    };

    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  componentWillReceiveProps(newProps) {
    if (newProps.errors) {
      this.setState({ errors: newProps.errors });
    }
  }

  onSubmit(e) {
    e.preventDefault();

    const { user } = this.props.auth;

    const newSejour = {
      description: this.state.text,
      isQuestion: this.state.isChecked
    };

    if(this.props.data._id) {
      newSejour.id = this.props.data._id
      this.props.updateSejour(newSejour);
    } else {
      this.props.addSejour(newSejour);
    }
    this.props.onClose();
  }

  onChange(e) {
    this.setState({ [e.target.name]: e.target.value });
  }

  render() {
    const { errors, isChecked } = this.state;

    return (
      <div className="Sejour-form mb-3">
        <div className="card card-info">
          <div className="card-header bg-info text-white">Say Something...</div>
          <div className="card-body">
            <form onSubmit={this.onSubmit}>
              <div className="form-group">
                <TextAreaFieldGroup
                  placeholder="Create a Sejour"
                  name="text"
                  value={this.state.text}
                  onChange={this.onChange}
                  error={errors.text}
                />
              </div>
              <div>
                IsQuestion
                <input type="checkbox"
                  checked={isChecked}
                  onChange={() => this.setState({ isChecked: !isChecked})}
                />
              </div>
              <button type="submit" className="btn btn-dark">
                Submit
              </button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

SejourForm.propTypes = {
  updateSejour: PropTypes.func.isRequired,
  addSejour: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired,
  // errors: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  auth: state.auth,
  errors: state.errors
});

export default connect(mapStateToProps, { addSejour, updateSejour })(SejourForm);
