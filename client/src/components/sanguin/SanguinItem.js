import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import classnames from 'classnames';
import { Link } from 'react-router-dom';
import Modal from 'react-responsive-modal';

import { deleteSanguin, updateSanguin } from '../../actions/SanguinActions';

class SanguinItem extends Component {
  constructor(props) {
    super(props);
    this.state = {
      text: '',
      errors: {},
      open: false,
      id: null,
    };

    this.onChange = this.onChange.bind(this);
  }

  componentWillReceiveProps(newProps) {
    if (newProps.errors) {
      this.setState({ errors: newProps.errors });
    }
  }

  onChange(e) {
    this.setState({ [e.target.name]: e.target.value });
  }
 
  onOpenModal = (data) => {
    this.setState({ open: true, id: data._id, text: data.description });
  };
 
  onCloseModal = () => {
    this.setState({ open: false, id: null });
  };

  onDeleteClick(id) {
    this.props.deleteSanguin(id);
  }

  onUpdateClick(id) {
    const description = {
      description: this.state.text,
      id,
    };
    this.onCloseModal();
    this.props.updateSanguin(description);
  }

  render() {
    const { sanguin } = this.props;
    const { open, errors } = this.state;

    return (
      <div className="card card-body mb-3">
        <div className="row">
          <div className="col-md-10">
            <p className="lead">{sanguin.name}</p>
              <span>
                <button
                  onClick={this.onDeleteClick.bind(this, sanguin._id)}
                  type="button"
                  className="btn btn-danger mr-1"
                >
                  <i className="fas fa-times" />
                </button>
                <button
                  onClick={() => this.props.onUpdate(sanguin)}
                  type="button"
                  className="btn btn-info mr-1"
                >
                  update
                </button>
              </span>
          </div>
        </div>
      </div>
    );
  }
}

SanguinItem.defaultProps = {
  showActions: true
};

SanguinItem.propTypes = {
  deleteSanguin: PropTypes.func.isRequired,
  updateSanguin: PropTypes.func.isRequired,
  sanguin: PropTypes.object.isRequired,
  auth: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  auth: state.auth
});

export default connect(mapStateToProps, { deleteSanguin, updateSanguin })(
  SanguinItem
);
