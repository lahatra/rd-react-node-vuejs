import React, { Component } from 'react';
import Header4 from '../components/header4';

class Detailsejour extends Component {
  login = () => {
    console.log("login");
  }

  render() {
    return (
  <div>
		<Header4/>
      <div className="sante-page" >				
		<div id="titre">
			<div className ="element">
		  	<label className="title">Détails Séjours </label>	
			</div>
		</div>
	
		<div className="form">
		  <label className="description">Pour ce voyage, je prévois le plus souvent de :</label>
		
	<form className="login-form"action="#">
		
		<input type="radio" id="bouton_1" name="bouton" />
		<label htmlFor="bouton_1">Dormir dans des hôtels ou appartement
				 de moyen standing et manger dans 
				des restaurants 
				(rarement dans la rue ou les cafés - bistrots)</label>
	
		<div className="element1">
      <input type="radio" id="bouton_2" name="bouton" />
      <label htmlFor="bouton_2">Dormir dans des hôtels ou appartement 
      de moyen standing et manger dans la rue 
      ou des cafés-bistrots</label>
		 </div>
		<div className="element1">
			<input type="radio" name="bouton" />
				<label htmlFor="bouton_3">Dormir en auberge, hôtel de bas standing
				ou chez l'habitant et manger dans la 
				rue ou des cafés - bistrots</label>
		</div>
	
	</form>
	</div>
</div>
</div>
    );
  }
}

export default Detailsejour;
