import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { loginUser } from '../../actions/authActions';
import TextFieldGroup from '../common/TextFieldGroup';

import { getAllergie } from '../../actions/AllergieActions';
import { getCentres } from '../../actions/CentreActions';
import { getMaladie } from '../../actions/MaladieActions';
import { getMedecin } from '../../actions/MedecinActions';
import { getPays } from '../../actions/PaysActions';
import { getQuestions } from '../../actions/QuestionActions';
import { getSanguin } from '../../actions/SanguinActions';
import { getSante } from '../../actions/SanteActions';
import { getSecours } from '../../actions/SecoursActions';
import { getSejours } from '../../actions/SejourActions';
import { getUrgence } from '../../actions/UrgenceActions';
import { getVaccin } from '../../actions/VaccinActions';
import { getVoyage } from '../../actions/VoyageActions';


class Login extends Component {
  constructor() {
    super();
    this.state = {
      email: '',
      password: '',
      errors: {}
    };
  }

  async componentDidMount() {
    await setTimeout(this.props.getAllergie(), 500);
    await setTimeout(this.props.getCentres(), 500); 
    await setTimeout(this.props.getMedecin(), 500);
    await setTimeout(this.props.getSecours(), 500);
    await setTimeout(this.props.getSanguin(), 500);
    await setTimeout(this.props.getQuestions(), 500);
    await setTimeout(this.props.getSante(), 500);
    await setTimeout(this.props.getSejours(), 500);
    await setTimeout(this.props.getVaccin(), 500);
    await setTimeout(this.props.getMaladie(), 500);
    await setTimeout(this.props.getUrgence(), 500);
    await setTimeout(this.props.getPays(), 500);
    await setTimeout(this.props.getVoyage(), 500);
    if (this.props.auth.isAuthenticated) {
      this.props.history.push('/voyage');
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.auth.isAuthenticated) {
      this.props.history.push('/voyage');
    }

    if (nextProps.errors) {
      this.setState({ errors: nextProps.errors });
    }
  }

  onSubmit = e => {
    e.preventDefault();

    const userData = {
      email: this.state.email,
      password: this.state.password
    };

    this.props.loginUser(userData);
  }

  onChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  }

  render() {
    const { errors } = this.state;

    return (
      <div className="login">
        <div className="container">
          <div className="row">
            <div className="col-md-8 m-auto">
              <h1 className="display-4 text-center">Log In</h1>
              <p className="lead text-center">
                Sign in to your DevConnector account
              </p>
              <form onSubmit={this.onSubmit}>
                <TextFieldGroup
                  placeholder="Email Address"
                  name="email"
                  type="email"
                  value={this.state.email}
                  onChange={this.onChange}
                  error={errors.email}
                />

                <TextFieldGroup
                  placeholder="Password"
                  name="password"
                  type="password"
                  value={this.state.password}
                  onChange={this.onChange}
                  error={errors.password}
                />
                <input type="submit" className="btn btn-info btn-block mt-4" />
              </form>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

Login.propTypes = {
  loginUser: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired,
  errors: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  auth: state.auth,
  errors: state.errors
});

export default connect(
  mapStateToProps,
  {
    loginUser, getAllergie,
    getCentres, getMaladie,
    getMedecin, getPays,
    getQuestions, getSanguin,
    getSante, getSecours,
    getSejours, getUrgence,
    getVaccin, getVoyage,
  })(Login);
