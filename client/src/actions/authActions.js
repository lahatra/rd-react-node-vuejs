import axios from 'axios';
import setAuthToken from '../utils/setAuthToken';
import jwt_decode from 'jwt-decode';

import { GET_ERRORS, SET_CURRENT_USER, GO_ADMIN, GET_USERS } from './types';

import config from '../utils/config';

export const goAdmin = payload => (dispatch) => {
  dispatch({
    type: GO_ADMIN,
    payload,
  });
};

// Get Maladies
export const getUsers = () => (dispatch) => {
  axios
    .get(`${config.baseUrl}/api/users`)
    .then((res) => {
      const payload = res.data;
      dispatch({
        type: GET_USERS,
        payload,
      });
    })
    .catch(err =>
      dispatch({
        type: GET_USERS,
        payload: [],
      }),
    );
};

// Register User
export const registerUser = (userData, history) => (dispatch) => {
  axios
    .post(`${config.baseUrl}/api/users/register`, userData)
    .then(res => history.push('/login'))
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response ? err.response.data : err,
      }),
    );
};

// Login - Get User Token
export const loginUser = userData => (dispatch) => {
  axios
    .post(`${config.baseUrl}/api/users/login`, userData)
    .then((res) => {
      // Save to localStorage
      const { token } = res.data;
      // Set token to ls
      localStorage.setItem('jwtToken', token);
      // Set token to Auth header
      setAuthToken(token);
      // Decode token to get user data
      const decoded = jwt_decode(token);
      // Set current user
      console.log('decoded', decoded);
      dispatch(setCurrentUser({ ...decoded, token }));
    })
    .catch((err) => {
      const payload = err.response ? err.response.data : err;
      dispatch({
        type: GET_ERRORS,
        payload,
      })
      ;
    },
    );
};

// Set logged in user
export const setCurrentUser = decoded => ({
  type: SET_CURRENT_USER,
  payload: decoded,
});

// Log user out
export const logoutUser = () => (dispatch) => {
  // Remove token from localStorage
  localStorage.removeItem('jwtToken');
  // Remove auth header for future requests
  setAuthToken(false);
  // Set current user to {} which will set isAuthenticated to false
  dispatch(setCurrentUser({}));
};
