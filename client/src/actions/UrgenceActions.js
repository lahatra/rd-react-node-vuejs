import axios from 'axios';

import {
  ADD_URGENCE,
  GET_ERRORS,
  CLEAR_ERRORS,
  GET_URGENCE,
  URGENCE_LOADING,
  DELETE_URGENCE,
  UPDATE_URGENCE,
} from './types';
import { updatePays } from './PaysActions';
import config from '../utils/config';
import fct from '../utils/function';

// Set loading state
export const setUrgenceLoading = () => ({
  type: URGENCE_LOADING,
});

// Clear errors
export const clearErrors = () => ({
  type: CLEAR_ERRORS,
});


// Add Post
export const updateUrgence = urgenceData => (dispatch) => {
  dispatch(clearErrors());
  axios
    .put(`${config.baseUrl}/api/urgence/${urgenceData.id}`, urgenceData)
    .then((res) => {
      dispatch({
        type: UPDATE_URGENCE,
        payload: res.data,
      });
      if (urgenceData.paysSelect.length > 0) {
        const p = urgenceData.paysSelect[0];
        p.urgence = fct.getUnique([...p.urgence, res.data], '_id');
        p.id = p._id;
        dispatch(updatePays(p));
      }
    })
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response ? err.response.data : err,
      }),
    );
};

// Add Post
export const addUrgence = urgenceData => (dispatch) => {
  dispatch(clearErrors());
  axios
    .post(`${config.baseUrl}/api/urgence`, urgenceData)
    .then((res) => {
      dispatch({
        type: ADD_URGENCE,
        payload: res.data,
      });
      if (urgenceData.paysSelect.length > 0) {
        const p = urgenceData.paysSelect[0];
        p.urgence = [...p.urgence, res.data];
        p.id = p._id;
        dispatch(updatePays(p));
      }
    })
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response ? err.response.data : err,
      }),
    );
};

// Get urgence
export const getUrgence = () => (dispatch) => {
  dispatch(setUrgenceLoading());
  axios
    .get(`${config.baseUrl}/api/urgence`)
    .then(res =>
      dispatch({
        type: GET_URGENCE,
        payload: res.data,
      }),
    )
    .catch(() =>
      dispatch({
        type: GET_URGENCE,
        payload: null,
      }),
    );
};

// Get Post
export const postUrgence = id => (dispatch) => {
  dispatch(setUrgenceLoading());
  axios
    .get(`${config.baseUrl}/api/urgence/${id}`)
    .then(res =>
      dispatch({
        type: GET_URGENCE,
        payload: res.data,
      }),
    )
    .catch(() =>
      dispatch({
        type: GET_URGENCE,
        payload: null,
      }),
    );
};

// Delete urgence
export const deleteUrgence = id => (dispatch) => {
  axios
    .delete(`${config.baseUrl}/api/urgence/${id}`)
    .then(() =>
      dispatch({
        type: DELETE_URGENCE,
        payload: id,
      }),
    )
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response ? err.response.data : err,
      }),
    );
};
