import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Modal from 'react-responsive-modal';

import SanguinForm from './SanguinForm';
import SanguinFeed from './SanguinFeed';
import Spinner from '../common/Spinner';
import { getSanguin } from '../../actions/SanguinActions';

class Sanguins extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      data: {}
    };
  }

  onOpenModal = () => {
    this.setState({ open: true, data: {} });
  };

  onUpdate = data => {
    this.setState({ open: true, data });
  };
 
  onCloseModal = () => {
    this.setState({ open: false });
  };

  componentDidMount() {
    this.props.getSanguin();
  }

  render() {
    const { sanguin, loading } = this.props.sanguin;
    const { open, data } = this.state;
    let SanguinContent;

    if (sanguin === null || loading) {
      SanguinContent = <Spinner />;
    } else {
      SanguinContent = <SanguinFeed sanguin={sanguin} onUpdate={this.onUpdate} />;
    }

    return (
      <div className="feed">
        <div className="container">
          <div className="row">
            <div className="col-md-12">
            <button onClick={this.onOpenModal} type="submit" className="btn btn-dark">
                  Ajout
                </button>
              {SanguinContent}
            </div>
          </div>
        </div>
        <Modal open={open} onClose={this.onCloseModal} center>
          <SanguinForm onClose={this.onCloseModal} data={data} />
        </Modal>
      </div>
    );
  }
}

Sanguins.propTypes = {
  getSanguin: PropTypes.func.isRequired,
  sanguin: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  sanguin: state.sanguin
});

export default connect(mapStateToProps, { getSanguin })(Sanguins);
