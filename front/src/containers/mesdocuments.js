import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Header3 from '../components/header3';

class Moustiquetique extends Component {
  render() {
    return (
      <div>
      <Header3 />
      <div className="sante-page">
          <div id="titre">
  <div className="element">
              <label className="title">Mes Documents</label>
            </div>
          </div>
          <br />
          <br /><br />
          <div id="titre">
            <div className="element">
  <label className="description">Stockez ici vos documents importants :</label>
            </div>
          </div>

          <div id="conteneur">
            <div ClassName="element">
  <Link to="/info">
  <img src="../img/passeport.png" className="image-mesdocuments" alt="passeport" />
              </Link>
              <br />
              <label className="description1">Passeport </label>
            </div>
            <div ClassName="element">
              <Link to="/alimentation">
  <img src="../img/visa.png" className="image-mesdocuments" alt="aliment" />
              </Link>
              <br />
              <label className="description1">Visa</label>
            </div>

          </div>

          <div id="conteneur">
            <div ClassName="element">
              <Link to="/moustique">
                <img src="../img/notes.png" className="image-mesdocuments" alt="moustique" />
              </Link>
              <br />
              <label className="description1">Ordonnances </label>
            </div>
            <div ClassName="element">
  <Link to="/soleil">
                <img src="../img/camera.png" className="image-mesdocuments" alt="soleil" />
              </Link>
            </div>

          </div>



        </div>
      </div>


    );
  }
}

export default Moustiquetique;
