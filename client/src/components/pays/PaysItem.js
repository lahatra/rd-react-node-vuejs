import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { deletePays, updatePays } from '../../actions/PaysActions';
import attribut from '../../attributs';

class PaysItem extends Component {
  constructor(props) {
    super(props);
    this.state = {
      text: '',
      errors: {},
      open: false,
      id: null,
    };

    this.onChange = this.onChange.bind(this);
  }

  componentWillReceiveProps(newProps) {
    if (newProps.errors) {
      this.setState({ errors: newProps.errors });
    }
  }

  onChange(e) {
    this.setState({ [e.target.name]: e.target.value });
  }
 
  onOpenModal = (data) => {
    this.setState({ open: true, id: data._id, text: data.name });
  };
 
  onCloseModal = () => {
    this.setState({ open: false, id: null });
  };

  onDeleteClick(id) {
    this.props.deletePays(id);
  }

  onUpdateClick(id) {
    const description = {
      name: this.state.text,
      id,
    };
    this.onCloseModal();
    this.props.updatePays(description);
  }

  render() {
    const { pays } = this.props;

    return (
      <div className="card card-body mb-3">
        <div className="row">
          <div className="col-md-10">
          {attribut.pays.map((key, i) => {
            if (key === 'eau') {
              return<div key={i}>{key} Potable: {pays[key].potable ? 'yes' : 'no'} { pays[key].commentaire ? pays[key].commentaire : ''}</div>
            }
            if (key === 'maladie') {
              return<div key={i}>{key}: {pays[key].map(k => <p key={k._id}>. {k.value || k._id}</p>)}</div>
            }
            if (key === 'centre') {
              return<div key={i}>{key}: {pays[key].map(k => <p key={k._id}>. {k.name || k._id}</p>)}</div>
            }
            if (key === 'urgence') {
              return<div key={i}>{key}: {pays[key].map(k => <p key={k._id}>. {k.numero || k._id}</p>)}</div>
            }
            if (key === 'medecin') {
              return<div key={i}>{key}: {pays[key].map(k => <p key={k._id}>. {k.value || k._id}</p>)}</div>
            }
            return <div key={i}>{key}: {pays[key]}</div>
          })}
              <span>
                <button
                  onClick={this.onDeleteClick.bind(this, pays._id)}
                  type="button"
                  className="btn btn-danger mr-1"
                >
                  <i className="fas fa-times" />
                </button>
                <button
                  onClick={() => this.props.onUpdate(pays)}
                  type="button"
                  className="btn btn-info mr-1"
                >
                  update
                </button>
              </span>
          </div>
        </div>
      </div>
    );
  }
}

PaysItem.defaultProps = {
  showActions: true
};

PaysItem.propTypes = {
  deletePays: PropTypes.func.isRequired,
  updatePays: PropTypes.func.isRequired,
  pays: PropTypes.object.isRequired,
  auth: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  auth: state.auth
});

export default connect(mapStateToProps, { deletePays, updatePays })(
  PaysItem
);
