const meningoque = [
    {
      "id": 1,
      "pays": "Afghanistan",
      "risque": "NON"
    },
    {
      "id": 2,
      "pays": "Afrique du Sud",
      "risque": "NON"
    },
    {
      "id": 3,
      "pays": "Albanie",
      "risque": "NON"
    },
    {
      "id": 4,
      "pays": "Alg‚rie",
      "risque": "NON"
    },
    {
      "id": 5,
      "pays": "Allemagne",
      "risque": "NON"
    },
    {
      "id": 6,
      "pays": "Andorre",
      "risque": "NON"
    },
    {
      "id": 7,
      "pays": "Angola",
      "risque": "NON"
    },
    {
      "id": 8,
      "pays": "Anguilla",
      "risque": "NON"
    },
    {
      "id": 9,
      "pays": "Antigua-et-Barbuda",
      "risque": "NON"
    },
    {
      "id": 10,
      "pays": "Arabie saoudite",
      "risque": "OUI"
    },
    {
      "id": 11,
      "pays": "Argentine",
      "risque": "NON"
    },
    {
      "id": 12,
      "pays": "Arm‚nie",
      "risque": "NON"
    },
    {
      "id": 13,
      "pays": "Aruba",
      "risque": "NON"
    },
    {
      "id": 14,
      "pays": "Australie",
      "risque": "NON"
    },
    {
      "id": 15,
      "pays": "Autriche",
      "risque": "NON"
    },
    {
      "id": 16,
      "pays": "Azerba‹djan",
      "risque": "NON"
    },
    {
      "id": 17,
      "pays": "Bahamas",
      "risque": "NON"
    },
    {
      "id": 18,
      "pays": "Bahre‹n",
      "risque": "NON"
    },
    {
      "id": 19,
      "pays": "Bangladesh",
      "risque": "NON"
    },
    {
      "id": 20,
      "pays": "Barbade",
      "risque": "NON"
    },
    {
      "id": 21,
      "pays": "Belgique",
      "risque": "NON"
    },
    {
      "id": 22,
      "pays": "Belize",
      "risque": "NON"
    },
    {
      "id": 23,
      "pays": "Benin",
      "risque": "OUI"
    },
    {
      "id": 24,
      "pays": "Bermudes",
      "risque": "NON"
    },
    {
      "id": 25,
      "pays": "Bhoutan",
      "risque": "NON"
    },
    {
      "id": 26,
      "pays": "Bi‚lorussie",
      "risque": "NON"
    },
    {
      "id": 27,
      "pays": "Bolivie (tat plurinational de)",
      "risque": "NON"
    },
    {
      "id": 28,
      "pays": "Bonaire",
      "risque": "NON"
    },
    {
      "id": 29,
      "pays": "Bosnie-Herz‚govine",
      "risque": "NON"
    },
    {
      "id": 30,
      "pays": "Botswana",
      "risque": "NON"
    },
    {
      "id": 31,
      "pays": "Br‚sil",
      "risque": "NON"
    },
    {
      "id": 32,
      "pays": "Brun‚i Darussalam",
      "risque": "NON"
    },
    {
      "id": 33,
      "pays": "Bulgarie",
      "risque": "NON"
    },
    {
      "id": 34,
      "pays": "Burkina Faso",
      "risque": "OUI"
    },
    {
      "id": 35,
      "pays": "Burundi",
      "risque": "OUI"
    },
    {
      "id": 36,
      "pays": "Cambodge",
      "risque": "NON"
    },
    {
      "id": 37,
      "pays": "Cameroun",
      "risque": "OUI"
    },
    {
      "id": 38,
      "pays": "Canada",
      "risque": "NON"
    },
    {
      "id": 39,
      "pays": "Cap-Vert",
      "risque": "NON"
    },
    {
      "id": 40,
      "pays": "Chili",
      "risque": "NON"
    },
    {
      "id": 41,
      "pays": "Chine",
      "risque": "NON"
    },
    {
      "id": 42,
      "pays": "Chypre",
      "risque": "NON"
    },
    {
      "id": 43,
      "pays": "Colombie",
      "risque": "NON"
    },
    {
      "id": 44,
      "pays": "Comores",
      "risque": "NON"
    },
    {
      "id": 45,
      "pays": "Congo",
      "risque": "NON"
    },
    {
      "id": 46,
      "pays": "Congo (R‚publique d‚mocratique du)",
      "risque": "OUI"
    },
    {
      "id": 47,
      "pays": "Cor‚e du Nord (r‚p. pop. d‚m. de)",
      "risque": "NON"
    },
    {
      "id": 48,
      "pays": "Cor‚e du Sud (r‚p. de)",
      "risque": "NON"
    },
    {
      "id": 49,
      "pays": "Costa Rica",
      "risque": "NON"
    },
    {
      "id": 50,
      "pays": "C“te d?Ivoire",
      "risque": "OUI"
    },
    {
      "id": 51,
      "pays": "Croatie",
      "risque": "NON"
    },
    {
      "id": 52,
      "pays": "Cuba",
      "risque": "NON"
    },
    {
      "id": 53,
      "pays": "Cura‡ao",
      "risque": "NON"
    },
    {
      "id": 54,
      "pays": "Danemark",
      "risque": "NON"
    },
    {
      "id": 55,
      "pays": "Djibouti",
      "risque": "NON"
    },
    {
      "id": 56,
      "pays": "Dominique",
      "risque": "NON"
    },
    {
      "id": 57,
      "pays": "gypte",
      "risque": "NON"
    },
    {
      "id": 58,
      "pays": "El Salvador",
      "risque": "NON"
    },
    {
      "id": 59,
      "pays": "mirats arabes unis",
      "risque": "NON"
    },
    {
      "id": 60,
      "pays": "quateur",
      "risque": "NON"
    },
    {
      "id": 61,
      "pays": "rythr‚e",
      "risque": "OUI"
    },
    {
      "id": 62,
      "pays": "Espagne",
      "risque": "NON"
    },
    {
      "id": 63,
      "pays": "Estonie",
      "risque": "NON"
    },
    {
      "id": 64,
      "pays": "Eswatini",
      "risque": "NON"
    },
    {
      "id": 65,
      "pays": "tats-Unis d?Am‚rique",
      "risque": "NON"
    },
    {
      "id": 66,
      "pays": "thiopie",
      "risque": "OUI"
    },
    {
      "id": 67,
      "pays": "Fidji",
      "risque": "NON"
    },
    {
      "id": 68,
      "pays": "Finlande",
      "risque": "NON"
    },
    {
      "id": 69,
      "pays": "France",
      "risque": "NON"
    },
    {
      "id": 70,
      "pays": "Gabon",
      "risque": "NON"
    },
    {
      "id": 71,
      "pays": "Gambie",
      "risque": "OUI"
    },
    {
      "id": 72,
      "pays": "G‚orgie",
      "risque": "NON"
    },
    {
      "id": 73,
      "pays": "G‚orgie du Sud-et-les ×les Sandwich du Sud",
      "risque": "NON"
    },
    {
      "id": 74,
      "pays": "Ghana",
      "risque": "OUI"
    },
    {
      "id": 75,
      "pays": "Gibraltar",
      "risque": "NON"
    },
    {
      "id": 76,
      "pays": "GrŠce",
      "risque": "NON"
    },
    {
      "id": 77,
      "pays": "Grenade",
      "risque": "NON"
    },
    {
      "id": 78,
      "pays": "Groenland",
      "risque": "NON"
    },
    {
      "id": 79,
      "pays": "Guadeloupe",
      "risque": "NON"
    },
    {
      "id": 80,
      "pays": "Guam",
      "risque": "NON"
    },
    {
      "id": 81,
      "pays": "Guatemala",
      "risque": "NON"
    },
    {
      "id": 82,
      "pays": "Guernesey",
      "risque": "NON"
    },
    {
      "id": 83,
      "pays": "Guin‚e",
      "risque": "OUI"
    },
    {
      "id": 84,
      "pays": "Guin‚e ‚quatoriale",
      "risque": "NON"
    },
    {
      "id": 85,
      "pays": "Guin‚e-Bissau",
      "risque": "OUI"
    },
    {
      "id": 86,
      "pays": "Guyana",
      "risque": "NON"
    },
    {
      "id": 87,
      "pays": "Guyane fran‡aise",
      "risque": "NON"
    },
    {
      "id": 88,
      "pays": "Ha‹ti",
      "risque": "NON"
    },
    {
      "id": 89,
      "pays": "Honduras",
      "risque": "NON"
    },
    {
      "id": 90,
      "pays": "Hongrie",
      "risque": "NON"
    },
    {
      "id": 91,
      "pays": "×le Christmas",
      "risque": "NON"
    },
    {
      "id": 92,
      "pays": "×le de Man",
      "risque": "NON"
    },
    {
      "id": 93,
      "pays": "×le Norfolk",
      "risque": "NON"
    },
    {
      "id": 94,
      "pays": "×les Ca‹manes",
      "risque": "NON"
    },
    {
      "id": 95,
      "pays": "×les Cook",
      "risque": "NON"
    },
    {
      "id": 96,
      "pays": "×les d?land",
      "risque": "NON"
    },
    {
      "id": 97,
      "pays": "×les des Cocos (Keeling)",
      "risque": "NON"
    },
    {
      "id": 98,
      "pays": "×les Falkland (Malvinas)",
      "risque": "NON"
    },
    {
      "id": 99,
      "pays": "×les F‚ro‚",
      "risque": "NON"
    },
    {
      "id": 100,
      "pays": "×les Mariannes du Nord",
      "risque": "NON"
    },
    {
      "id": 101,
      "pays": "×les Marshall",
      "risque": "NON"
    },
    {
      "id": 102,
      "pays": "×les Salomon",
      "risque": "NON"
    },
    {
      "id": 103,
      "pays": "×les Svalbard-et-Jan Mayen",
      "risque": "NON"
    },
    {
      "id": 104,
      "pays": "×les Turques-et-Ca‹ques",
      "risque": "NON"
    },
    {
      "id": 105,
      "pays": "×les Vierges am‚ricaines",
      "risque": "NON"
    },
    {
      "id": 106,
      "pays": "×les Vierges britanniques",
      "risque": "NON"
    },
    {
      "id": 107,
      "pays": "×les Wallis-et-Futuna",
      "risque": "NON"
    },
    {
      "id": 108,
      "pays": "Inde",
      "risque": "NON"
    },
    {
      "id": 109,
      "pays": "Indon‚sie",
      "risque": "NON"
    },
    {
      "id": 110,
      "pays": "Irak",
      "risque": "NON"
    },
    {
      "id": 111,
      "pays": "Iran (R‚publique islamique d?)",
      "risque": "NON"
    },
    {
      "id": 112,
      "pays": "Irlande",
      "risque": "NON"
    },
    {
      "id": 113,
      "pays": "Islande",
      "risque": "NON"
    },
    {
      "id": 114,
      "pays": "Isra‰l",
      "risque": "NON"
    },
    {
      "id": 115,
      "pays": "Italie",
      "risque": "NON"
    },
    {
      "id": 116,
      "pays": "Jama‹que",
      "risque": "NON"
    },
    {
      "id": 117,
      "pays": "Japon",
      "risque": "NON"
    },
    {
      "id": 118,
      "pays": "Jersey",
      "risque": "NON"
    },
    {
      "id": 119,
      "pays": "Jordanie",
      "risque": "NON"
    },
    {
      "id": 120,
      "pays": "Kazakhstan",
      "risque": "NON"
    },
    {
      "id": 121,
      "pays": "Kenya",
      "risque": "OUI"
    },
    {
      "id": 122,
      "pays": "Kirghizistan",
      "risque": "NON"
    },
    {
      "id": 123,
      "pays": "Kiribati",
      "risque": "NON"
    },
    {
      "id": 124,
      "pays": "Kowe‹t",
      "risque": "NON"
    },
    {
      "id": 125,
      "pays": "Laos (R‚publique d‚mocratique populaire)",
      "risque": "NON"
    },
    {
      "id": 126,
      "pays": "Lesotho",
      "risque": "NON"
    },
    {
      "id": 127,
      "pays": "Lettonie",
      "risque": "NON"
    },
    {
      "id": 128,
      "pays": "Liban",
      "risque": "NON"
    },
    {
      "id": 129,
      "pays": "Lib‚ria",
      "risque": "NON"
    },
    {
      "id": 130,
      "pays": "Libye",
      "risque": "NON"
    },
    {
      "id": 131,
      "pays": "Liechtenstein",
      "risque": "NON"
    },
    {
      "id": 132,
      "pays": "Lituanie",
      "risque": "NON"
    },
    {
      "id": 133,
      "pays": "Luxembourg",
      "risque": "NON"
    },
    {
      "id": 134,
      "pays": "Mac‚doine",
      "risque": "NON"
    },
    {
      "id": 135,
      "pays": "Madagascar",
      "risque": "NON"
    },
    {
      "id": 136,
      "pays": "Malaisie",
      "risque": "NON"
    },
    {
      "id": 137,
      "pays": "Malawi",
      "risque": "NON"
    },
    {
      "id": 138,
      "pays": "Maldives",
      "risque": "NON"
    },
    {
      "id": 139,
      "pays": "Mali",
      "risque": "OUI"
    },
    {
      "id": 140,
      "pays": "Malte",
      "risque": "NON"
    },
    {
      "id": 141,
      "pays": "Maroc",
      "risque": "NON"
    },
    {
      "id": 142,
      "pays": "Martinique",
      "risque": "NON"
    },
    {
      "id": 143,
      "pays": "Maurice",
      "risque": "NON"
    },
    {
      "id": 144,
      "pays": "Mauritanie",
      "risque": "OUI"
    },
    {
      "id": 145,
      "pays": "Mayotte",
      "risque": "NON"
    },
    {
      "id": 146,
      "pays": "Mexique",
      "risque": "NON"
    },
    {
      "id": 147,
      "pays": "Micron‚sie (tats f‚d‚r‚s de)",
      "risque": "NON"
    },
    {
      "id": 148,
      "pays": "Moldavie",
      "risque": "NON"
    },
    {
      "id": 149,
      "pays": "Monaco",
      "risque": "NON"
    },
    {
      "id": 150,
      "pays": "Mongolie",
      "risque": "NON"
    },
    {
      "id": 151,
      "pays": "Mont‚n‚gro",
      "risque": "NON"
    },
    {
      "id": 152,
      "pays": "Montserrat",
      "risque": "NON"
    },
    {
      "id": 153,
      "pays": "Mozambique",
      "risque": "NON"
    },
    {
      "id": 154,
      "pays": "Myanmar",
      "risque": "NON"
    },
    {
      "id": 155,
      "pays": "Namibie",
      "risque": "NON"
    },
    {
      "id": 156,
      "pays": "Nauru",
      "risque": "NON"
    },
    {
      "id": 157,
      "pays": "N‚pal",
      "risque": "NON"
    },
    {
      "id": 158,
      "pays": "Nicaragua",
      "risque": "NON"
    },
    {
      "id": 159,
      "pays": "Niger",
      "risque": "OUI"
    },
    {
      "id": 160,
      "pays": "Nig‚ria",
      "risque": "OUI"
    },
    {
      "id": 161,
      "pays": "Niou‚",
      "risque": "NON"
    },
    {
      "id": 162,
      "pays": "NorvŠge",
      "risque": "NON"
    },
    {
      "id": 163,
      "pays": "Nouvelle-Cal‚donie",
      "risque": "NON"
    },
    {
      "id": 164,
      "pays": "Nouvelle-Z‚lande",
      "risque": "NON"
    },
    {
      "id": 165,
      "pays": "Oman",
      "risque": "NON"
    },
    {
      "id": 166,
      "pays": "Ouganda",
      "risque": "OUI"
    },
    {
      "id": 167,
      "pays": "Ouzb‚kistan",
      "risque": "NON"
    },
    {
      "id": 168,
      "pays": "Pakistan",
      "risque": "NON"
    },
    {
      "id": 169,
      "pays": "Palaos",
      "risque": "NON"
    },
    {
      "id": 170,
      "pays": "Palestine",
      "risque": "NON"
    },
    {
      "id": 171,
      "pays": "Panama",
      "risque": "NON"
    },
    {
      "id": 172,
      "pays": "Papouasie-Nouvelle-Guin‚e",
      "risque": "NON"
    },
    {
      "id": 173,
      "pays": "Paraguay",
      "risque": "NON"
    },
    {
      "id": 174,
      "pays": "Pays-Bas",
      "risque": "NON"
    },
    {
      "id": 175,
      "pays": "P‚rou",
      "risque": "NON"
    },
    {
      "id": 176,
      "pays": "Philippines",
      "risque": "NON"
    },
    {
      "id": 177,
      "pays": "Pitcairn",
      "risque": "NON"
    },
    {
      "id": 178,
      "pays": "Pologne",
      "risque": "NON"
    },
    {
      "id": 179,
      "pays": "Polyn‚sie fran‡aise",
      "risque": "NON"
    },
    {
      "id": 180,
      "pays": "Porto Rico",
      "risque": "NON"
    },
    {
      "id": 181,
      "pays": "Portugal",
      "risque": "NON"
    },
    {
      "id": 182,
      "pays": "Qatar",
      "risque": "NON"
    },
    {
      "id": 183,
      "pays": "R‚publique centrafricaine",
      "risque": "OUI"
    },
    {
      "id": 184,
      "pays": "R‚publique dominicaine",
      "risque": "NON"
    },
    {
      "id": 185,
      "pays": "R‚publique tchŠque",
      "risque": "NON"
    },
    {
      "id": 186,
      "pays": "R‚union",
      "risque": "NON"
    },
    {
      "id": 187,
      "pays": "Roumanie",
      "risque": "NON"
    },
    {
      "id": 188,
      "pays": "Royaume-Uni",
      "risque": "NON"
    },
    {
      "id": 189,
      "pays": "Russie (F‚d‚ration de)",
      "risque": "NON"
    },
    {
      "id": 190,
      "pays": "Rwanda",
      "risque": "OUI"
    },
    {
      "id": 191,
      "pays": "Saba",
      "risque": "NON"
    },
    {
      "id": 192,
      "pays": "Sahara occidental",
      "risque": "NON"
    },
    {
      "id": 193,
      "pays": "Saint-Barth‚lemy",
      "risque": "NON"
    },
    {
      "id": 194,
      "pays": "Sainte-H‚lŠne",
      "risque": "NON"
    },
    {
      "id": 195,
      "pays": "Sainte-Lucie",
      "risque": "NON"
    },
    {
      "id": 196,
      "pays": "Saint-Eustache",
      "risque": "NON"
    },
    {
      "id": 197,
      "pays": "Saint-Kitts-et-Nevis",
      "risque": "NON"
    },
    {
      "id": 198,
      "pays": "Saint-Marin",
      "risque": "NON"
    },
    {
      "id": 199,
      "pays": "Saint-Martin (partie fran‡aise)",
      "risque": "NON"
    },
    {
      "id": 200,
      "pays": "Saint-Martin (partie n‚erlandaise)",
      "risque": "NON"
    },
    {
      "id": 201,
      "pays": "Saint-Pierre-et-Miquelon",
      "risque": "NON"
    },
    {
      "id": 202,
      "pays": "Saint-Vincent-et-les Grenadines",
      "risque": "NON"
    },
    {
      "id": 203,
      "pays": "Samoa",
      "risque": "NON"
    },
    {
      "id": 204,
      "pays": "Samoa am‚ricaines",
      "risque": "NON"
    },
    {
      "id": 205,
      "pays": "Sao Tom‚-et-Principe",
      "risque": "NON"
    },
    {
      "id": 206,
      "pays": "S‚n‚gal",
      "risque": "OUI"
    },
    {
      "id": 207,
      "pays": "Serbie",
      "risque": "NON"
    },
    {
      "id": 208,
      "pays": "Sercq",
      "risque": "NON"
    },
    {
      "id": 209,
      "pays": "Seychelles",
      "risque": "NON"
    },
    {
      "id": 210,
      "pays": "Sierra Leone",
      "risque": "NON"
    },
    {
      "id": 211,
      "pays": "Singapour",
      "risque": "NON"
    },
    {
      "id": 212,
      "pays": "Slovaquie",
      "risque": "NON"
    },
    {
      "id": 213,
      "pays": "Slov‚nie",
      "risque": "NON"
    },
    {
      "id": 214,
      "pays": "Somalie",
      "risque": "NON"
    },
    {
      "id": 215,
      "pays": "Soudan",
      "risque": "OUI"
    },
    {
      "id": 216,
      "pays": "Soudan du Sud",
      "risque": "OUI"
    },
    {
      "id": 217,
      "pays": "Sri Lanka",
      "risque": "NON"
    },
    {
      "id": 218,
      "pays": "SuŠde",
      "risque": "NON"
    },
    {
      "id": 219,
      "pays": "Suisse",
      "risque": "NON"
    },
    {
      "id": 220,
      "pays": "Suriname",
      "risque": "NON"
    },
    {
      "id": 221,
      "pays": "Syrie",
      "risque": "NON"
    },
    {
      "id": 222,
      "pays": "Tadjikistan",
      "risque": "NON"
    },
    {
      "id": 223,
      "pays": "Ta‹wan",
      "risque": "NON"
    },
    {
      "id": 224,
      "pays": "Tanzanie (R‚publique-Unie de)",
      "risque": "OUI"
    },
    {
      "id": 225,
      "pays": "Tchad",
      "risque": "OUI"
    },
    {
      "id": 226,
      "pays": "Terres australes fran‡aises",
      "risque": "NON"
    },
    {
      "id": 227,
      "pays": "Territoire britannique de l'oc‚an Indien",
      "risque": "NON"
    },
    {
      "id": 228,
      "pays": "Tha‹lande",
      "risque": "NON"
    },
    {
      "id": 229,
      "pays": "Timor-Leste",
      "risque": "NON"
    },
    {
      "id": 230,
      "pays": "Togo",
      "risque": "NON"
    },
    {
      "id": 231,
      "pays": "Tok‚laou",
      "risque": "NON"
    },
    {
      "id": 232,
      "pays": "Tonga",
      "risque": "NON"
    },
    {
      "id": 233,
      "pays": "Trinit‚-et-Tobago",
      "risque": "NON"
    },
    {
      "id": 234,
      "pays": "Tunisie",
      "risque": "NON"
    },
    {
      "id": 235,
      "pays": "Turkm‚nistan",
      "risque": "NON"
    },
    {
      "id": 236,
      "pays": "Turquie",
      "risque": "NON"
    },
    {
      "id": 237,
      "pays": "Tuvalu",
      "risque": "NON"
    },
    {
      "id": 238,
      "pays": "Ukraine",
      "risque": "NON"
    },
    {
      "id": 239,
      "pays": "Uruguay",
      "risque": "NON"
    },
    {
      "id": 240,
      "pays": "Vanuatu",
      "risque": "NON"
    },
    {
      "id": 241,
      "pays": "Vatican",
      "risque": "NON"
    },
    {
      "id": 242,
      "pays": "Venezuela (R‚publique bolivarienne du)",
      "risque": "NON"
    },
    {
      "id": 243,
      "pays": "Viet Nam",
      "risque": "NON"
    },
    {
      "id": 244,
      "pays": "Y‚men",
      "risque": "NON"
    },
    {
      "id": 245,
      "pays": "Zambie",
      "risque": "NON"
    },
    {
      "id": 246,
      "pays": "Zimbabwe",
      "risque": "NON"
    }
  ]
  export default meningoque;