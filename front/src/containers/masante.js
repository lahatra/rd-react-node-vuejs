import React, { Component } from 'react';
import { connect } from 'react-redux';
import Select from 'react-select';
import Header4 from '../components/header4';

import mapStateToProps from '../redux/mapStateToProps';
import mapDispatchToProps from '../redux/mapDispatchToProps';
import TextFieldGroup from '../common/TextFieldGroup';

class masante extends Component {
  constructor(props) {
    super(props);
    this.state = {
      sanguin: '',
      poids: 0,
      problemeSantePasse: '',
      problemeSanteEncours: '',
      naissance: 0,
      errors: {},
      sanguinList: [],
      allergieList: [],
      one_sante: {},
    };
  }

  async componentDidMount() {
    await this.props.getSante();
    await this.props.getSanguin();
    await this.props.getAllergie();
  }

  componentWillMount() {
    const { sante, one_sante } = this.props.sante;
    const { user } = this.props.auth;
    const { allergie } = this.props.allergie;
    console.log(one_sante, 'one_sante =====>', allergie);
    if (user && sante) {
      const s = sante.filter(i => i.user === user.id);
      if (!(s.length > 0)) return;
      this.setState({ one_sante: s[0] });
      const ids = s[0].allergie.map(i => i._id);
      console.log('ssssss', ids);
      this.setState({
        sanguin: s.length > 0 ? s[0].sanguin : '',
        poids: s.length > 0 ? s[0].poids : 0,
        sanguinList: [],
        allergieList: allergie ? allergie.filter(k => ids.includes(k._id)) : [],
        problemeSantePasse: s.length > 0 ? s[0].problemeSantePasse : '',
        problemeSanteEncours: s.length > 0 ? s[0].problemeSanteEncours : '',
        naissance: s.length > 0 ? parseInt(s[0].naissance, 10) : 0,
      });
    }
  }

  onChange = (e) => {
    this.setState({ [e.target.name]: e.target.value });
  }

  isChecked = (item, k) => this.state[`${k}List`].filter(key => key._id === item._id).length > 0

  toggleChange = (item, k) => {
    let list = [];
    if (this.isChecked(item, k)) {
      list = this.state[`${k}List`].filter(key => key._id !== item._id);
    } else {
      list = [...this.state[`${k}List`], item];
    }
    this.setState({ [`${k}List`]: list });
  }

	go = () => {
	  const {
	    sanguin,
	    poids,
	    allergieList,
	    problemeSantePasse,
	    problemeSanteEncours,
	    naissance,
	  } = this.state;
	  const newSante = {
	    sanguin,
	    poids,
	    allergie: allergieList,
	    problemeSantePasse,
	    problemeSanteEncours,
	    naissance,
	  };
	  const { one_sante } = this.props.sante;
	  console.log('one_sante =====>', one_sante);
	  if (one_sante._id) {
	    newSante.id = one_sante._id;
	    this.props.updateSante(newSante);
	  } else {
	    this.props.addSante(newSante);
	  }
	}

	render() {
	  const { one_sante, allergieList, errors } = this.state;
	  const { allergie } = this.props.allergie;
	  const { sanguin } = this.props.sanguin;

	  console.log('this.state.naissance', allergieList);
	  return (
  		<div>
        <Header4/>
    <div className="sante-page">
  <div id="titre">
        <div className="element">
	            <h1>Ma Sante </h1>
	          </div>
	        </div>
  <div className="form">
  <form >

	            <TextFieldGroup
      name="naissance"
      placeholder="age"
	              type="number"
	              value={this.state.naissance}
      onChange={e => this.setState({ naissance: e.target.value })}
      error={errors.naissance}
    />
	            <TextFieldGroup
	              name="poids"
      placeholder="poids"
      type="text"
      value={this.state.poids}
      onChange={e => this.setState({ poids: e.target.value })}
      error={errors.poids}
    />
  <Select
  name="allergie"
  value={allergieList}
	              onChange={allergieList => this.setState({ allergieList })}
	              options={allergie}
  isMulti
	            />
  <select
	              name="sanguin"
	              size="1"
	              value={this.state.sanguin}
	              onChange={e => this.setState({ sanguin: e.target.value })}
	            >
  {
	                sanguin.map((item, i) => <option key={i} value={item._id}>{item.value}</option>)
	              }
	            </select>
  {/* {
	              sanguin.map((item, i) => (
  <div key={i}>
  {item.name}
  <input
	                    type="checkbox"
      checked={this.isChecked(item, 'sanguin')}
      onChange={() => this.toggleChange(item, 'sanguin')}
    />problemeSantePasse,
      problemeSanteEncours,
	                </div>
	              ))
	            } */}
  <textarea
  value={this.state.problemeSanteEncours}
  onChange={e => this.setState({ problemeSanteEncours: e.target.value })}
	              placeholder="Problèmes de santé en cours"
	            />
  <textarea
  value={this.state.problemeSantePasse}
  onChange={e => this.setState({ problemeSantePasse: e.target.value })}
  placeholder="Problèmes de santé passés"
	            />
	          </form>
  <button onClick={this.go}>Soummettre</button>
	        </div>
	      </div>
  </div>
	  );
	}
}

// export default masante;
export default connect(mapStateToProps, mapDispatchToProps)(masante);
