/* eslint-disable max-len */
import axios from 'axios';

import {
  ADD_PAYS,
  GET_ERRORS,
  CLEAR_ERRORS,
  GET_ONE_PAYS,
  GET_PAYS,
  PAYS_LOADING,
  DELETE_PAYS,
  UPDATE_PAYS,
} from './types';
// import fct from '../utils/function';
import config from '../utils/config';

const listArray = ['maladie', 'centre', 'medecin', 'urgence'];
// Set loading state
export const setPayssLoading = () => ({
  type: PAYS_LOADING,
});

// Clear errors
export const clearErrors = () => ({
  type: CLEAR_ERRORS,
});

// Get Payss
export const getPays = () => (dispatch, getState) => {
  dispatch(setPayssLoading());
  axios
    .get(`${config.baseUrl}/api/pays`)
    .then((res) => {
      const payload = res.data.map((i) => {
        const result = i;
        listArray.map((j) => {
          if (i[j] && i[j].length > 0) {
            result[j] = getState()[j] && getState()[j][j] ? getState()[j][j].filter(p => i[j].map(k => k._id).includes(p._id)) : [];
          }
          return result;
        });
        return result;
      });
      dispatch({
        type: GET_PAYS,
        payload,
      });
    })
    .catch(() =>
      dispatch({
        type: GET_PAYS,
        payload: null,
      }),
    );
};

// Add Post
export const addPays = paysData => (dispatch) => {
  dispatch(clearErrors());
  axios
    .post(`${config.baseUrl}/api/pays`, paysData)
    .then(() => dispatch(getPays()))
    .catch((err) => {
      console.log('err err', err);
      dispatch({
        type: GET_ERRORS,
        payload: {},
      });
    },
    );
};

// Get Post
export const postPays = id => (dispatch) => {
  dispatch(setPayssLoading());
  axios
    .get(`${config.baseUrl}/api/pays/${id}`)
    .then(() => dispatch(getPays()))
    .catch(() =>
      dispatch({
        type: GET_ONE_PAYS,
        payload: null,
      }),
    );
};

// Delete Payss
export const deletePays = id => (dispatch) => {
  axios
    .delete(`${config.baseUrl}/api/pays/${id}`)
    .then(() =>
      dispatch({
        type: DELETE_PAYS,
        payload: id,
      }),
    )
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response ? err.response.data : err,
      }),
    );
};

// Up Post
export const updatePays = paysData => (dispatch) => {
  dispatch(clearErrors());
  axios
    .put(`${config.baseUrl}/api/pays/${paysData.id}`, paysData)
    .then(() => dispatch(getPays()))
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response ? err.response.data : err,
      }),
    );
};
