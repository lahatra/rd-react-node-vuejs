import React, { Component } from 'react';
import { connect } from 'react-redux';
import Header4 from '../components/header4';

import mapStateToProps from '../redux/mapStateToProps';
import mapDispatchToProps from '../redux/mapDispatchToProps';

class Accueil extends Component {
  constructor(props) {
    super(props);
    this.state = {
      secoursList: [],
    };
  }

  async componentWillMount() {
    await this.props.getSecoursUser();
    await this.props.getSecours();
    const { secoursUser } = this.props.secours;
    console.log('secoursUser', secoursUser);
    this.setState({ secoursList: secoursUser || [] });
  }

  login = () => {
    console.log('login'); //
  }

  toggleChange = (item, k) => {
    let list = [];
    alert(item.commentaire || item.description);
    if (this.isChecked(item, k)) {
      list = this.state[`${k}List`].filter(key => key._id !== item._id);
    } else {
      list = [...this.state[`${k}List`], item];
    }
    this.props.addLikeSecours(list);
    this.setState({ [`${k}List`]: list });
  }

  isChecked = (item, k) => this.state[`${k}List`].filter(key => key._id === item._id).length > 0

  render() {
    const { secours, secoursUser } = this.props.secours;
    console.log('secours', secours, secoursUser);

    return (
      <div>
        <Header4/>
        <div className="checklist-page">
          <div id="titre">
            <div className="element">
              <label className="title">Trousse de secours </label>
            </div>
          </div>
          <div id="conteneur">
            <div className="element">
              <label className="description">Ce qui n’est pas sélectionné sera ajouté à votre liste de courses dans Ma Checklist</label>
              <br />
              <br />
            </div>
          </div>
          <div id="conteneur">
            <div className="element">
              <label className="titre">Médicaments  : </label>
            </div>
          </div>
          <div className="form">
            <form className="login-form"action="#">
              <table>
                <tbody>
                  {
                    secours.map(item => (
                      <tr key={item._id}>
                        <td><label>
                          <input
                            type="checkbox"
                            className="ios-switch"
                            checked={this.isChecked(item, 'secours')}
                            onChange={() => this.toggleChange(item, 'secours')}
                          /><div><div /></div></label></td>
                        <td><label type="description">{item.description || item.intitule}</label></td>
                      </tr>
                    ))
                  }
                  {/* <tr>
                    <td><label><input type="checkbox" className="ios-switch" /><div><div /></div></label></td>
                    <td><label type="description">Traitement d'urgence en cas
d'allergie grave</label></td>
                  </tr>
                  <tr>
                    <td><label><input type="checkbox" className="ios-switch" /><div><div /></div></label></td>
                    <td><label type="description">Antalgiques – anti-pyrétiques
(contre la douleur et la fièvre) :
paracétamol</label></td>
                  </tr>
                  <tr>
                    <td><label><input type="checkbox" className="ios-switch" /><div><div /></div></label></td>
                    <td><label type="description">  Anti-diarrhéiques et
anti-émétiques (contre les
nausées, vomissements)</label></td>
                  </tr> */}
                </tbody>
              </table>

              <br />
              <br />
              <div id="conteneur">
                <div className="element"><label className="titre" type="description">Toujours utiles </label></div>
              </div>
              <br />
              <table>
                <tbody>
                  <tr>
                    <td><label><input type="checkbox" className="ios-switch" /><div><div /></div></label></td>
                    <td><label type="description">Crème solaire (indice de protection 30 minimum)</label></td>
                  </tr>
                  {/* <tr>
                    <td><label><input type="checkbox" className="ios-switch" /><div><div /></div></label></td>
                    <td><label type="description">Thermomètre</label></td>
                  </tr>
                  <tr>
                    <td><label><input type="checkbox" className="ios-switch" /><div><div /></div></label></td>
                    <td><label type="description">Sérum physiologique unidose</label></td>
                  </tr>
                  <tr>
                    <td><label><input type="checkbox" className="ios-switch" /><div><div /></div></label></td>
                    <td><label type="description">Désinfectant</label></td>
                  </tr>
                  <tr>
                    <td><label><input type="checkbox" className="ios-switch" /><div><div /></div></label></td>
                    <td><label type="description">Compresses stériles</label></td>
                  </tr>
                  <tr>
                    <td><label><input type="checkbox" className="ios-switch" /><div><div /></div></label></td>
                    <td><label type="description">Pansements (différentes tailles)</label></td>
                  </tr>
                  <tr>
                    <td><label><input type="checkbox" className="ios-switch" /><div><div /></div></label></td>
                    <td><label type="description">Ciseaux</label></td>
                  </tr>
                  <tr>
                    <td><label><input type="checkbox" className="ios-switch" /><div><div /></div></label></td>
                    <td><label type="description">Mini sac poubelle</label></td>
                  </tr>
                  <tr>
                    <td><label><input type="checkbox" className="ios-switch" /><div><div /></div></label></td>
                    <td><label type="description">Préservatifs</label></td>
                  </tr>
                  <tr>
                    <td><label><input type="checkbox" className="ios-switch" /><div><div /></div></label></td>
                    <td><label type="description">Pince à épiler</label></td>
                  </tr> */}
                </tbody>
              </table>
            </form>

          </div>
        </div>
      </div>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Accueil);
