import axios from 'axios';

import {
  GET_ERRORS,
  CLEAR_ERRORS,
  GET_ONE_SANTE,
  GET_SANTE,
  SANTE_LOADING,
  DELETE_SANTE,
  UPDATE_SANTE,
} from './types';
import config from '../utils/config';

// Set loading state
export const setSantesLoading = () => ({
  type: SANTE_LOADING,
});

// Clear errors
export const clearErrors = () => ({
  type: CLEAR_ERRORS,
});

export const updateSante = santeData => (dispatch) => {
  dispatch(clearErrors());
  axios
    .put(`${config.baseUrl}/api/sante`, santeData)
    .then(res =>
      dispatch({
        type: UPDATE_SANTE,
        payload: res.data,
      }),
    )
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response ? err.response.data : err,
      }),
    );
};

// Add Post
export const addSante = santeData => (dispatch, getState) => {
  dispatch(clearErrors());
  const user = getState().auth.user ? getState().auth.user.id : null;
  if (!user) return;
  axios
    .post(`${config.baseUrl}/api/sante/${user}`, santeData)
    .then((res) => {
      console.log('res', res.data);
      if (res && res.data) {
        dispatch({
          type: GET_ONE_SANTE,
          payload: res.data,
        });
      }
    })
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response,
      }),
    );
};

// Get Santes
export const getSante = () => (dispatch) => {
  dispatch(setSantesLoading());
  axios
    .get(`${config.baseUrl}/api/sante`)
    .then(res =>
      dispatch({
        type: GET_SANTE,
        payload: res.data,
      }),
    )
    .catch(() =>
      dispatch({
        type: GET_SANTE,
        payload: null,
      }),
    );
};

// Get Santes
export const getSanteUser = () => (dispatch, getState) => {
  dispatch(setSantesLoading());
  const user = getState().auth.user ? getState().auth.user.id : null;
  if (!user) return;
  axios
    .get(`${config.baseUrl}/api/sante/user/${user}`)
    .then((res) => {
      console.log('res', res.data);
      if (res && res.data) {
        dispatch({
          type: GET_ONE_SANTE,
          payload: res.data,
        });
      }
    })
    .catch(() =>
      dispatch({
        type: GET_SANTE,
        payload: null,
      }),
    );
};

// Get Post
export const postSante = id => (dispatch) => {
  dispatch(setSantesLoading());
  axios
    .get(`${config.baseUrl}/api/sante/${id}`)
    .then(res =>
      dispatch({
        type: GET_ONE_SANTE,
        payload: res.data,
      }),
    )
    .catch(() =>
      dispatch({
        type: GET_ONE_SANTE,
        payload: null,
      }),
    );
};

// Delete Santes
export const deleteSante = id => (dispatch) => {
  axios
    .delete(`${config.baseUrl}/api/sante/${id}`)
    .then(() =>
      dispatch({
        type: DELETE_SANTE,
        payload: id,
      }),
    )
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data,
      }),
    );
};
