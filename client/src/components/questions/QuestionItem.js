import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { deleteQuestion, updateQuestion } from '../../actions/QuestionActions';
import attribut from '../../attributs';

class QuestionItem extends Component {
  constructor(props) {
    super(props);
    this.state = {
      text: '',
      errors: {},
      open: false,
      id: null,
    };

    this.onChange = this.onChange.bind(this);
  }

  componentWillReceiveProps(newProps) {
    if (newProps.errors) {
      this.setState({ errors: newProps.errors });
    }
  }

  onChange(e) {
    this.setState({ [e.target.name]: e.target.value });
  }

  onDeleteClick(id) {
    this.props.deleteQuestion(id);
  }

  onUpdateClick(id) {
    const description = {
      description: this.state.text,
      id,
    };
    this.onCloseModal();
    this.props.updateQuestion(description);
  }

  render() {
    const { checklist } = this.props;

    return (
      <div className="card card-body mb-3">
        <div className="row">
          <div className="col-md-10">
          {attribut.checklist.map((key, i) => <div key={i}>{key}: {checklist[key]}</div>
            )}
            {/* <p className="lead">{checklist.description}</p> */}
              <span>
                <button
                  onClick={this.onDeleteClick.bind(this, checklist._id)}
                  type="button"
                  className="btn btn-danger mr-1"
                >
                  <i className="fas fa-times" />
                </button>
                <button
                   onClick={() => this.props.onUpdate(checklist)}
                  type="button"
                  className="btn btn-info mr-1"
                >
                  update
                </button>
              </span>
          </div>
        </div>
      </div>
    );
  }
}

QuestionItem.defaultProps = {
  showActions: true
};

QuestionItem.propTypes = {
  deleteQuestion: PropTypes.func.isRequired,
  updateQuestion: PropTypes.func.isRequired,
  checklist: PropTypes.object.isRequired,
  auth: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  auth: state.auth
});

export default connect(mapStateToProps, { deleteQuestion, updateQuestion })(
  QuestionItem
);
