import axios from 'axios';

import {
  ADD_QUESTION,
  GET_ERRORS,
  CLEAR_ERRORS,
  GET_QUESTIONS,
  GET_QUESTION,
  QUESTION_LOADING,
  DELETE_QUESTION,
  UPDATE_QUESTION,
  ADD_USER_CHECKLIST,
} from './types';
import config from '../utils/config';
// Add Like
export const addLikeChecklist = checklist => (dispatch, getState) => {
  const user = getState().auth.user ? getState().auth.user.id : null;
  if (!user) return;
  axios
    .put(`${config.baseUrl}/api/checklist/like-unlink/${user}`, checklist)
    .then((res) => {
      console.log('ressssss', res);
    })
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data,
      }),
    );
};

// Remove Like
export const removeLike = id => (dispatch, getState) => {
  const user = getState().auth.user ? getState().auth.user.id : null;
  if (!user) return;
  axios
    .post(`${config.baseUrl}/api/checklist/unlike/${user}`, id)
    // .then(res => dispatch(getPosts()))
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data,
      }),
    );
};

export const addUserChecklist = payload => ({
  type: ADD_USER_CHECKLIST,
  payload,
});

// Get all profiles
export const getChecklistUser = () => (dispatch, getState) => {
  const id = getState().auth.user ? getState().auth.user.id : null;
  if (!id) return;
  axios
    .get(`${config.baseUrl}/api/checklist/user${id}`)
    .then((res) => {
      if (res && res.data) {
        console.log('dfsdfsdfsd', res);
        dispatch(addUserChecklist(res.data.checklist));
      }
    })
    .catch((err) => {});
};

// Add Post
export const updateQuestion = questionData => (dispatch) => {
  dispatch(clearErrors());
  axios
    .put(`${config.baseUrl}/api/checklist/${questionData.id}`, questionData)
    .then(res =>
      dispatch({
        type: UPDATE_QUESTION,
        payload: res.data,
      }),
    )
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data,
      }),
    );
};

// Add Post
export const addQuestion = questionData => (dispatch) => {
  dispatch(clearErrors());
  axios
    .post(`${config.baseUrl}/api/checklist`, questionData)
    .then(res =>
      dispatch({
        type: ADD_QUESTION,
        payload: res.data,
      }),
    )
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data,
      }),
    );
};

// Get Questions
export const getQuestions = () => (dispatch) => {
  dispatch(setQuestionsLoading());
  axios
    .get(`${config.baseUrl}/api/checklist`)
    .then(res =>
      dispatch({
        type: GET_QUESTIONS,
        payload: res.data,
      }),
    )
    .catch(err =>
      dispatch({
        type: GET_QUESTIONS,
        payload: null,
      }),
    );
};

// Get Post
export const postQuestion = id => (dispatch) => {
  dispatch(setQuestionsLoading());
  axios
    .get(`${config.baseUrl}/api/checklist/${id}`)
    .then(res =>
      dispatch({
        type: GET_QUESTION,
        payload: res.data,
      }),
    )
    .catch(err =>
      dispatch({
        type: GET_QUESTION,
        payload: null,
      }),
    );
};

// Delete Questions
export const deleteQuestion = id => (dispatch) => {
  axios
    .delete(`${config.baseUrl}/api/checklist/${id}`)
    .then(res =>
      dispatch({
        type: DELETE_QUESTION,
        payload: id,
      }),
    )
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data,
      }),
    );
};


// Set loading state
export const setQuestionsLoading = () => ({
  type: QUESTION_LOADING,
});

// Clear errors
export const clearErrors = () => ({
  type: CLEAR_ERRORS,
});
