import isEmpty from '../validation/is-empty';

import { SET_CURRENT_USER, UPDATE_USERS } from '../actions/types';

const initialState = {
  isAuthenticated: false,
  user: {},
  vcc: [],
};

export default function (state = initialState, action) {
  switch (action.type) {
    case SET_CURRENT_USER:
      return {
        ...state,
        isAuthenticated: !isEmpty(action.payload),
        user: action.payload,
      };
    case UPDATE_USERS:
      return {
        ...state,
        user: {
          ...state.user,
          ...action.payload,
        },
      };
    default:
      return state;
  }
}
