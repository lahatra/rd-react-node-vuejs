import React, { Component } from 'react';
import { connect } from 'react-redux';

import mapStateToProps from '../redux/mapStateToProps';
import mapDispatchToProps from '../redux/mapDispatchToProps';
import Header4 from '../components/header4';

// import pays from '../data/pays';

class Madestination extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dateArrive: '',
      dateDepart: '',
      pays: '',
    };
  }

  componentWillMount() {
    console.log('==================>', this.props.user ? this.props.user.user_id : null);
    this.props.getVoyage();
    this.props.getPays();
    this.props.getSejours();
    this.props.getMaladie();
  }

	addVoyage = () => {
    if(!this.state.pays || this.state.pays.length < 2) return;
	  const { user } = this.props.auth;
	  const newVoyage = {
	    name: this.state.name,
	    dateDepart: this.state.dateDepart,
	    dateArrive: this.state.dateArrive,
      id: user.id,
      user: user.id,
	    pays: this.state.pays,
	  };
	  this.props.addVoyage(newVoyage, this.props.history);
	}

	render() {
	  const { pays } = this.props.pays;
	  return (
  <div>
  <Header4 />
  <div className="sante-page">
      <div id="titre">
	          <div className="element">
            <label className="title">Ma Destination </label>
	          </div>
      </div>

	        <div className="form">
      <form className="login-form">
    <label className="title3">Mon pays : </label>
    <select
	              name="pays"
	              size="1"
	              onChange={(e) => {
	                console.log('e.target', e.target.value);
	                this.setState({ pays: e.target.value });
	              }}
	              value={this.state.pays}
	            >
                <option key={'data._id'} value=''>pays</option>
	              { pays.map(data => <option key={data._id} value={data._id}>{data.name}</option>) }
	            </select>
    <label className="title3">Mes Dates : </label>
    <input
                value={this.state.dateDepart}
	              onChange={e => this.setState({ dateDepart: e.target.value })}
	              type="date"
	              placeholder="dateDepart"
              />
    <input
  value={this.state.dateArrive}
  onChange={e => this.setState({ dateArrive: e.target.value })}
  type="date"
  placeholder="dateArrive"
	            />
  </form>
      <button onClick={this.addVoyage}>Enregistrer</button>
    </div>
    </div>
	    </div>
	  );
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(Madestination);
