import React, { Component } from 'react';
import { StatusBar, NetInfo } from 'react-native';
import { connect } from 'react-redux';

import mapStateToProps from 'redux-local/mapStateToProps';
import mapDispatchToProps from 'redux-local/mapDispatchToProps';
import Onboarding from '../containers/onboarding';
import MainComponent from '../containers/maincomponents';

class Main extends MainComponent {
  constructor(props) {
    super(props);
  }
  componentWillMount() {
  }


  render() {
    return (
      this.props.user && this.props.user.admin ? <Onboarding /> : <AppContainer />
    );
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(Main);
