const mapStateToProps = state => ({
  crud: state.crud,
  user: state.user.user,
  pays: state.pays,
  auth: state.auth,
  errors: state.errors,
  profile: state.profile,
  maladie: state.maladie,
  vaccin: state.vaccin,
  checklist: state.checklist,
  sante: state.sante,
  voyage: state.voyage,
  medecin: state.medecin,
  centre: state.centre,
  sejour: state.sejour,
  urgence: state.urgence,
  sanguin: state.sanguin,
  categorie: state.categorie,
  allergie: state.allergie,
  secours: state.secours,
  documents: state.documents,
});

export default mapStateToProps;
