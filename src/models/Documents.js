const mongoose = require('mongoose');

const Schema = mongoose.Schema;

// Create Schema
const DocumentsSchema = new Schema({
  commentaire: {
    type: String,
  },
  user: {
    type: Schema.Types.ObjectId,
    ref: 'users',
  },
  description: {
    type: String,
  },
  data: [
    {
      type: String,
    },
  ],
  date: {
    type: Date,
    default: Date.now,
  },
});

module.exports = Documents = mongoose.model('documents', DocumentsSchema);
