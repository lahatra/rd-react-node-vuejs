import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Header4 from '../components/header4';

class Info extends Component {
  render() {
	  return (
  <div>
        <Header4 />
        <div className="sante-page">
      <div id="titre">
            <div className="element">
          <label className="title">Alimentation </label>
        </div>
          </div>

      <div className="group">
            <label className="description">

          <h3>En résumé : Pelé, cuit, frit, bouilli ! Sinon tant pis !</h3>
          <br />

Cela laisse tout de même énormément de liberté pour choisir son repas, que ce soit de la cuisine française classique ou un bon petit plat bien local qu'il serait dommage de louper !
          <br />
-Je ne mange rien qui ne soit bouilli, cuit, frit ou pelé !
          <br />
- Et évidemment, je me lave les mains avant de manger.

        </label>
          </div>
      <div id="conteneur">
            <div className="element">
          <img className="image" src="../img/bon_appetit.png" alt="prise" />
        </div>
          </div>
      <div className="tab blue">
            <input id="tab-five" type="radio" name="tabs2" />
            <label htmlFor="tab-five">En savoir plus  </label>
            <div className="tab-content">

          <h3>Pourquoi suivre ces mesures de prévention ?</h3>
          <br />
          <p>
Le risque le plus fréquent est la turista, c’est une simple gastro-entérite, très fréquente, souvent carabinée mais bénigne. Elle guérit spontanément en un à trois jours dans la grande majorité des cas, mais est plus rarement grave chez les personnes fragiles. Elle est plus souvent liée à la consommation d’aliments solides que d’eau. Le risque principal est donc de perdre un à trois jours de voyage, à se reposer plutôt qu’à profiter des vacances.
Mais ce n’est pas tout ! Il existe d’autres maladies, bénignes aussi (ver solitaire ou autres joyeusetés selon les destinations) mais surtout des maladies potentiellement graves voire mortelles. Ce sont ces maladies qui justifient raisonnablement de faire preuve d’un minimum de prévention.

              </p>
        </div>
          </div>
      <div className="tab blue">
            <input id="tab-six" type="radio" name="tabs2" />
            <label htmlFor="tab-six">Mieux vaut prévenir… </label>
            <div className="tab-content">

          <h3>Mieux vaut prévenir…</h3>
          <br />
					La prévention repose sur des mesures d’hygiène simples :
          <br />
Pelé, cuit, frit, bouilli ! Sinon tant pis !
          <br />

          <p>
Quelques précisions :
Pelé, cela vous permettra selon la destination de découvrir des merveilles locales et de vous rafraichir : mangue, fruit du dragon, noix de coco fraîche, etc…
Cuit, ça veut dire cuit à la chaleur en excluant la cuisson basse température et liée à l’acidité : marinade, fumage, séchage, … Cela laisse suffisamment de possibilité : frit, bouilli mais aussi rôti, sauté, poêlé, grillé, etc… tant que c’est cuit à cœur (au minimum « à point » pour une viande).
En dehors des aliments pelés, les aliments devront être consommés chaud (et pas seulement réchauffés).
                <br />
Et avant de manger (ou de préparer mon repas), évidemment je me lave les mains :
eau + savon : permet d'éliminer les saletés et les microbes qui vont avec ! C'est le Must et la base de l'hygiène ! Dans ce cas, l’utilisation d’un gel hydro-alcoolique n’est pas indispensable.
si ce n'est pas possible : un gel hydro-alccolique est une bonne solution, même si c’est moins efficace que le lavage des mains.
                <br />
Quand on s’intéresse à la gastronomie locale, on remarque rapidement que la grande majorité des plats sont compatibles avec ces simples mesures. La tradition fait que les populations adaptent spontanément leur culture culinaire au risque environnant. S’il y a quelques plats qui font exceptions, réservez-les pour une éventuelle soirée d’exception dans un endroit où un standing élevé de préparation des plats est plus probable (même s’il n’y a jamais de garanties).
                <br />
Vous aurez noté qu’on ne parle pas spécifiquement de cuisine de rue. Plus important que le standing, c’est le mode de préparation qui compte lorsqu’on considère les lieux de restauration les plus fréquemment visités durant les voyages, à savoir les cafés, bistrots et établissements de ce genre. La rigueur de préparation n’est pas toujours meilleure que dans la rue. Il vaut donc mieux juger au cas par cas, en fonction du plat lui-même, et dans la rue observer un peu le vendeur préparer avant de commander.
                <br />
En appliquant ces quelques conseils, vous en déduirez l’essentiel des choses à éviter, à réserver pour les soirées spéciales et les très nombreuses spécialités locales à découvrir au quotidien.


              </p>
        </div>
          </div>

      <div className="group">
            <div className="footer">
          <Link to="/info">
                <img src="../img/info.png" className="image_footer" alt="Infos" />
              </Link>
          <Link to="/moustique">
                <img src="../img/moustique.png" className="image_footer" alt="Moustique" />
              </Link>
          <Link to="/soleil">
                <img src="/img/soleil.png" className="image_footer" alt="soleil2" />
              </Link>
          <Link to="/baignade">
                <img src="../img/baignade.png" className="image_footer" alt="baignade" />
              </Link>
          <Link to="/animaux">
                <img src="../img/animaux.png" className="image_footer" alt="renard" />
              </Link>
          <Link to="/quiz">
                <img src="../img/quizz.png" className="image_footer" alt="Transport" />
              </Link>
        </div>
          </div>
    </div>
      </div>
	  );
  }
}

export default Info;
