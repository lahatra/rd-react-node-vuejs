import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Header4 from '../components/header4';
import { connect } from 'react-redux';

import mapStateToProps from '../redux/mapStateToProps';
import mapDispatchToProps from '../redux/mapDispatchToProps';

class Info extends Component {
  constructor(props) {
    super(props);
    this.state = {
      questions: [],
      paysCurrent: {},
      maladieCurrent: [],
      vaccin: {
        v1: [],
        v2: [],
      },
      i: 0,
    };
  }
  componentDidMount() {
    const { one_voyage } = this.props.voyage;
    const { pays } = this.props.pays;
    const { maladie } = this.props.maladie;
    const { vaccin } = this.props.vaccin;
    if (!pays) return;
    const paysID = pays.filter(i => one_voyage.pays === i._id);
    if (!paysID || paysID.length < 1) return;
    paysID && this.setState({ paysCurrent: paysID[0] });
    const maladieIDS = paysID[0].maladie && paysID[0].maladie.map(i => i._id);
    if (!maladieIDS) return;
    const maladieCurrent = maladie.filter(i => maladieIDS.includes(i._id));
    if (!maladieCurrent) return;
    maladieCurrent && this.setState({ maladieCurrent });
    // if (paysID && paysID.length > 0) {
    //   const maladieIDS = paysID[0].maladie && paysID[0].maladie.map(i => i._id);
    //   const res = {
    //     v1: [],
    //     v2: [],
    //   };

    //   const vaccinDetail = maladie && maladie.reduce((acc, curr) => {
    //     if (maladieIDS && maladieIDS.includes(curr._id)) {
    //       const mIDS = curr.vaccin.map(i => i._id);
    //       const s = vaccin && vaccin.filter(i => mIDS.includes(i._id));
    //       s && acc.v1.push(...s);
    //       const mIDSugg = curr.vaccinSugg.map(i => i._id);
    //       const s2 = vaccin && vaccin.filter(i => mIDSugg.includes(i._id));
    //       s2 && acc.v2.push(...s2);
    //     }
    //     return acc;
    //   }, res);
    //   const vs = vaccinDetail ? { v1: getUnique(vaccinDetail.v1, '_id'), v2: getUnique(vaccinDetail.v2, '_id') } : [];
    //   console.log('vaccin', vs);
    //   this.setState({ vaccin: vs });
    // }
    // this.props.getSejour(maladie[0]._id);
  }
  render() {
    const { paysCurrent, maladieCurrent } = this.state;
    console.log('paysCurrent paysCurrent', paysCurrent);
	  return (
  <div>
        <Header4 />
        <div className="sante-page">
      <div id="titre">
            <div className="element">
          <label className="title">Infos Générales </label>
        </div>
          </div>
      <div className="group">
            <label className="description">Capitale : {paysCurrent.capital}</label>
            <label className="description">Indicatif téléphonique : {paysCurrent.indicatifPhone}</label>
            <label className="description">Décalage horaire par rapport à la métropole : {paysCurrent.decalageHoraore}</label>
            <label className="description">Prise électrique : {paysCurrent.prise}</label>
            <img className="prise"src="../img/prise.png" alt="prise" />
          </div>
      <div className="group">
            <h2>
		Les Risques principaux :
        </h2>
            <br />
            <label className="description">
		Les risques suivants sont les principaux risques spécifiques à mon voyage. La liste ne pourra jamais être exhaustive, et ce n’est pas le but ! Le but est de partir bien préparé et de comprendre les risques les plus importants selon la situation et de les limiter avec des mesures de prévention généralement simples qui n’empêchent en rien de profiter du voyage. Les symptômes ne sont pas détaillés, car ils sont généralement non spécifiques au début (souvent comparables à ceux d’une simple grippe ou gastro-entérite) et il ne faudrait pas attendre de voir des symptômes spécifiques pour consulter (ce serait prendre trop de risques que d’attendre).

        </label>
            <label className="description">
		Travel kit n’est pas un système d’alerte en temps réel. Il est donc recommandé de s’inscrire sur l’application Ariane du Ministère de l’Europe et des Affaires étrangères qui propose de recevoir des alertes sécuritaires et sanitaires par SMS ou par courriel, en particulier dans les situations problématiques sur les plans géopolitiques, épidémiques, météorologiques exceptionnelles.
        </label>
            <br />

            <div id="conteneur-info">
          <div className="form">
                {
                  maladieCurrent.map((i, key) => (
                    <div className="tab blue" key={i._id}>
                      <input id={`tab-${key}`} type="radio" name="tabs2" />
                      <label htmlFor="tab-four">{i.name}</label>
                      <div className="tab-content">
                        <p>{i.risque}</p>
                      </div>
                    </div>
                  ))
                }

              </div>
          <div className="footer">
                <Link to="/info">
              <img src="../img/info.png" className="image_footer" alt="Infos" />
            </Link>
                <Link to="/moustique">
              <img src="../img/moustique.png" className="image_footer" alt="Moustique" />
            </Link>
                <Link to="/soleil">
              <img src="/img/soleil.png" className="image_footer" alt="soleil2" />
            </Link>
                <Link to="/baignade">
              <img src="../img/baignade.png" className="image_footer" alt="baignade" />
            </Link>
                <Link to="/animaux">
              <img src="../img/animaux.png" className="image_footer" alt="renard" />
            </Link>
                <Link to="/quiz">
              <img src="../img/quizz.png" className="image_footer" alt="Transport" />
            </Link>
              </div>
        </div>
          </div>
    </div>
      </div>
	  );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Info);
