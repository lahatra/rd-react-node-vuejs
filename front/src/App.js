import React from 'react';
import { BrowserRouter as Router, Route, HashRouter } from 'react-router-dom';

import Authentification from './containers/authentification';
import Baignade from './containers/baignade';
import couvertureSante from './containers/couvertureSante';
import Header from './components/header';
import Header2 from './components/header2';
import Header3 from './components/header3';
import Header4 from './components/header4';
import Accueil from './containers/accueil';
import Boiremanger from './containers/boire-manger';
import ConsultationMedicale from './containers/consultation-medicale';

import Register from './containers/register';
import Detailsejour from './containers/detailsejour';
import Madestination from './containers/madestination';
import Moustiquetique from './containers/moustiquetique';
import PremierVoyage from './containers/premierVoyage';

import detailSejourOne from './containers/detailSejourOne';
import Info from './containers/info';
import masante from './containers/masante';
import nouveauVoyage from './containers/noveauVoyage';
import profilSante from './containers/profilSante';
import CarnetVaccination from './containers/carnetvaccination';
import sejour from './containers/sejour';
import sejourconfirm from './containers/sejourconfirm';

import MasanteAffichage from './containers/masante-affichage';
import Checklist from './containers/checklist';
import CarnetVaccinationAffichage from './containers/carnet-vaccination-affichage';
import Trousse from './containers/trousse';
import Animaux from './containers/animaux';
import Soleil from './containers/soleil';
import Medicament from './containers/medicament';
import Boisson from './containers/boisson';
import Alimentation from './containers/alimentation';
import Moustique from './containers/moustique';
import Monquotidien from './containers/monquotidien';
import Mesdocuments from './containers/mesdocuments';
import Profil from './containers/profil';
import Tique from './containers/tique';
import Monprofil from './containers/monprofil';
import Quizz from './containers/quizz';
import Payement from './containers/payement';

// import './assets/css/style.css';
// import './assets/css/sejour.css';

const App = () => (
  <HashRouter>
    <Router>
      <div>

        <Route exact path="/boire-manger" component={Boiremanger} />
        <Route exact path="/consultation-medicale" component={ConsultationMedicale} />

        <Route exact path="/register" component={Register} />
        <Route exact path="/detailsejour" component={Detailsejour} />
        <Route exact path="/sejour" component={sejour} />
        <Route exact path="/madestination" component={Madestination} />
        <Route exact path="/moustique-tique" component={Moustiquetique} />
        <Route exact path="/premiervoyage" component={PremierVoyage} />
        <Route exact path="/accueil" component={Accueil} />
        <Route exact path="/authentification" component={Authentification} />
        <Route exact path="/baignade" component={Baignade} />
        <Route exact path="/" component={Authentification} />`
        <Route exact path="/couvertureSante" component={couvertureSante} />
        <Route exact path="/carnet-vaccination" component={CarnetVaccination} />
        <Route exact path="/detailSejourOne" component={detailSejourOne} />
        <Route exact path="/Info" component={Info} />
        <Route exact path="/nouveauVoyage" component={nouveauVoyage} />
        <Route exact path="/profilSante" component={profilSante} />
        <Route exact path="/masante" component={masante} />
        <Route exact path="/masante-affichage" component={MasanteAffichage} />
        <Route exact path="/checklist" component={Checklist} />
        <Route exact path="/carnet-vaccination-affichage" component={CarnetVaccinationAffichage} />
        <Route exact path="/trousse" component={Trousse} />
        <Route exact path="/soleil" component={Soleil} />
        <Route exact path="/animaux" component={Animaux} />
        <Route exact path="/medicament" component={Medicament} />
        <Route exact path="/boisson" component={Boisson} />
        <Route exact path="/alimentation" component={Alimentation} />
        <Route exact path="/moustique" component={Moustique} />

        <Route exact path="/mesdocuments" component={Mesdocuments} />
        <Route exact path="/monquotidien" component={Monquotidien} />
        <Route exact path="/profil" component={Profil} />
        <Route exact path="/moustiquetique" component={Moustiquetique} />
        <Route exact path="/tique" component={Tique} />
        <Route exact path="/monprofil" component={Monprofil} />
        <Route exact path="/quizz" component={Quizz} />
        <Route exact path="/payement" component={Payement} />

      </div>


    </Router>
  </HashRouter>
);

export default App;
