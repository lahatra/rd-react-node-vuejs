// const https = require('https');
import config from '../data/config';

const axios = require('axios').default.create({
  /*
  httpsAgent: new https.Agent({
    rejectUnauthorized: false,
    requestCert: false,
    agent: false
  })
  */
});

const headers = {
  // headers: {
  //   'Content-type': 'application/x-www-form-urlencoded',
  // },
  headers: {
    'Content-type': 'application/json',
  },
};

export const get = async url => await axios.get(`${config.baseUrl}/${url}`, headers);

// export const post = async (url, data) => await axios.post(`${config.baseUrl}/${url}`, headers, data);

export const post = async (url, data) => await axios({
  // headers,
  method: 'post',
  url: `${config.baseUrl}/${url}`,
  data,
});

export default get;
