import React, { Component } from 'react';
import { connect } from 'react-redux';

import mapStateToProps from '../redux/mapStateToProps';
import mapDispatchToProps from '../redux/mapDispatchToProps';
import Header3 from '../components/header3';

class profilSante extends Component {
	async componentWillMount() {
		await this.props.getChecklistUser();
    await this.props.getQuestions();
		await this.props.getSanteUser();
		await this.props.getSante();
    await this.props.getSanguin();
    await this.props.getAllergie();
	}

	masante = () => {
	  this.props.history.push('/masante');
	}

	consultation = () => {
	  this.props.history.push('/consultation-medicale');
	}

	couvertureSante = () => {
	  this.props.history.push('/couvertureSante');
	}
	trousse = () => {
	  this.props.history.push('/trousse');
	}

	medicament = () => {
	  this.props.history.push('/medicament');
	}

	carnetaffichage = () => {
	  this.props.history.push('/carnet-vaccination-affichage');
	}
	render() {
	  return (
	    <div>
				<Header3/>
    <div className="sante-page">
  <div id="titre">
	          <div className="element">
      <label className="title3">Mon Profil Santé </label>
    </div>
      </div>
  <div className="group">

	          <div className="sante">
	            <img src="../img/heart.png" className="image" alt="Vaccins" />
      <span className="text" onClick={this.masante}>Ma Santé</span>
    	          </div>

	        </div>
	        <div className="group">

        <div className="sante">
    <img src="../img/vaccin.png" className="image" alt="Vaccins" />
    <span className="text" onClick={this.carnetaffichage}>Carnet de vaccination </span>
  </div>

      </div>
  <div className="group">

  <div className="sante">
	            <img src="../img/medicament.png" className="image" alt="Vaccins" />
	            <span className="text" onClick={this.medicament}>Médicaments </span>
	              </div>

	        </div>
  <div className="group">

        <div className="sante">
	            <img src="../img/trousse.png" className="image" alt="Vaccins" />
	            <span className="text" onClick={this.trousse}>Trousse de Secours </span>
    	          </div>

      </div><div className="group">

	          <div className="sante">
	            <img src="../img/consultation.png" className="image" alt="Vaccins" />
	            <span className="text" onClick={this.consultation}>Consulation Médicale </span>
        	          </div>

</div><div className="group">

        <div className="sante">
	            <img src="../img/vaccin.png" className="image" alt="Vaccins" />
	            <span className="text" onClick={this.couvertureSante}>Couverture Santé </span>
	          </div>

      </div>

	      </div>
  </div>
	  );
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(profilSante);
