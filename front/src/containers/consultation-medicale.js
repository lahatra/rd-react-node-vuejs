import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Header4 from '../components/header4';

class ConsultationMedicale extends Component {
  render() {
    return (
      <div>
        <Header4/>
        <div className="sante-page">
          <div id="titre">
            <div className="element">
              <label className="title">Consultation Médicale</label>
			      </div>
          </div>
          <div className="group">
            <label className="description">Avant mon voyage, je peux avoir besoin de consulter un médecin.</label>
            <label className="description">Vous pouvez consulter votre médecin généraliste. Les vaccins éventuellement nécessaires sont disponibles en pharmacie de ville. En cas de besoin, votre médecin pourra vous orienter vers un centre de vaccinations internationales s’il estime un avis spécialisé nécessaire.
              En pratique, les ruptures de stock et tensions d’approvisionnement sont régulières et certains vaccins sont rarement effectués en médecine de ville. Ils sont donc disponibles uniquement sur commande, renseignez-vous auprès de votre pharmacien.
              Dans les situations de ruptures de stock ou de tension d’approvisionnement, certains vaccins peuvent être réservés aux personnes prioritaires, votre médecin pourra vous préciser si vous en faites partie et faciliter l’obtention d’un rendez-vous en centre de vaccinations internationales. Ces derniers sont prioritaires en cas de tension d’approvisionnement et peuvent donc disposer de plus de stock qu’en pharmacie de ville.
            </label>
            <label className="description">
              Le délai de rendez-vous en centre de vaccinations internationales est très variable et peut être long (jusqu’à un ou deux mois au printemps et en automne dans les périodes de fortes demandes).
              Localisez le centre de vaccinations internationales le plus proche de chez vous : 
            </label>
            <div className="form">
              <form className="login-form">
                <input type="text" placeholder="code postal"/>
              </form>
            </div>
            <div className="footer">
              <Link to="/info">
                <img src="../img/info.png" className="image_footer" alt="Infos" />
              </Link>
              <Link to="/moustique">
                <img src="../img/moustique.png" className="image_footer" alt="Moustique" />
              </Link>
              <Link to="/soleil">
                <img src="../img/soleil.png" className="image_footer" alt="soleil2" />
              </Link>
              <Link to="/baignade">
                <img src="../img/baignade.png" className="image_footer" alt="baignade" />
              </Link>
              <Link to="/animaux">
                <img src="../img/animaux.png" className="image_footer" alt="renard" />
              </Link>
              <Link to="/quiz">
              <img src="../img/quizz.png" className="image_footer" alt="Transport" />
              </Link>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default ConsultationMedicale;
