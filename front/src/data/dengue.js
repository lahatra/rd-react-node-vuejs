const dengue = [
    {
      "id": 1,
      "pays": "Afghanistan",
      "risque": "NON",
      "question": ""
    },
    {
      "id": 2,
      "pays": "Afrique du Sud",
      "risque": "NON",
      "question": ""
    },
    {
      "id": 3,
      "pays": "Albanie",
      "risque": "NON",
      "question": ""
    },
    {
      "id": 4,
      "pays": "Alg‚rie",
      "risque": "NON",
      "question": ""
    },
    {
      "id": 5,
      "pays": "Allemagne",
      "risque": "NON",
      "question": ""
    },
    {
      "id": 6,
      "pays": "Andorre",
      "risque": "NON",
      "question": ""
    },
    {
      "id": 7,
      "pays": "Angola",
      "risque": "OUI",
      "question": ""
    },
    {
      "id": 8,
      "pays": "Anguilla",
      "risque": "OUI",
      "question": ""
    },
    {
      "id": 9,
      "pays": "Antigua-et-Barbuda",
      "risque": "OUI",
      "question": ""
    },
    {
      "id": 10,
      "pays": "Arabie saoudite",
      "risque": "OUI",
      "question": "Sud-Ouest du pays"
    },
    {
      "id": 11,
      "pays": "Argentine",
      "risque": "OUI",
      "question": "Nord-Est du pays de la province de Buenos Aires … la province de Salta"
    },
    {
      "id": 12,
      "pays": "Arm‚nie",
      "risque": "NON",
      "question": ""
    },
    {
      "id": 13,
      "pays": "Aruba",
      "risque": "OUI",
      "question": ""
    },
    {
      "id": 14,
      "pays": "Australie",
      "risque": "OUI",
      "question": "Nord et Nord-Est du Queensland"
    },
    {
      "id": 15,
      "pays": "Autriche",
      "risque": "NON",
      "question": ""
    },
    {
      "id": 16,
      "pays": "Azerba‹djan",
      "risque": "NON",
      "question": ""
    },
    {
      "id": 17,
      "pays": "Bahamas",
      "risque": "OUI",
      "question": ""
    },
    {
      "id": 18,
      "pays": "Bahre‹n",
      "risque": "NON",
      "question": ""
    },
    {
      "id": 19,
      "pays": "Bangladesh",
      "risque": "OUI",
      "question": ""
    },
    {
      "id": 20,
      "pays": "Barbade",
      "risque": "OUI",
      "question": ""
    },
    {
      "id": 21,
      "pays": "Belgique",
      "risque": "NON",
      "question": ""
    },
    {
      "id": 22,
      "pays": "Belize",
      "risque": "OUI",
      "question": ""
    },
    {
      "id": 23,
      "pays": "B‚nin",
      "risque": "NON",
      "question": ""
    },
    {
      "id": 24,
      "pays": "Bermudes",
      "risque": "NON",
      "question": ""
    },
    {
      "id": 25,
      "pays": "Bhoutan",
      "risque": "OUI",
      "question": ""
    },
    {
      "id": 26,
      "pays": "Bi‚lorussie",
      "risque": "NON",
      "question": ""
    },
    {
      "id": 27,
      "pays": "Bolivie (tat plurinational de)",
      "risque": "OUI",
      "question": ""
    },
    {
      "id": 28,
      "pays": "Bonaire",
      "risque": "OUI",
      "question": ""
    },
    {
      "id": 29,
      "pays": "Bosnie-Herz‚govine",
      "risque": "NON",
      "question": ""
    },
    {
      "id": 30,
      "pays": "Botswana",
      "risque": "NON",
      "question": ""
    },
    {
      "id": 31,
      "pays": "Br‚sil",
      "risque": "OUI",
      "question": ""
    },
    {
      "id": 32,
      "pays": "Brun‚i Darussalam",
      "risque": "OUI",
      "question": ""
    },
    {
      "id": 33,
      "pays": "Bulgarie",
      "risque": "NON",
      "question": ""
    },
    {
      "id": 34,
      "pays": "Burkina Faso",
      "risque": "OUI",
      "question": ""
    },
    {
      "id": 35,
      "pays": "Burundi",
      "risque": "NON",
      "question": ""
    },
    {
      "id": 36,
      "pays": "Cambodge",
      "risque": "OUI",
      "question": ""
    },
    {
      "id": 37,
      "pays": "Cameroun",
      "risque": "NON",
      "question": ""
    },
    {
      "id": 38,
      "pays": "Canada",
      "risque": "NON",
      "question": ""
    },
    {
      "id": 39,
      "pays": "Cap-Vert",
      "risque": "OUI",
      "question": ""
    },
    {
      "id": 40,
      "pays": "Chili",
      "risque": "OUI",
      "question": ""
    },
    {
      "id": 41,
      "pays": "Chine",
      "risque": "OUI",
      "question": "Zones frontaliŠres sud … l'Est du Bhoutan jusqu'… Hangzhou"
    },
    {
      "id": 42,
      "pays": "Chypre",
      "risque": "NON",
      "question": ""
    },
    {
      "id": 43,
      "pays": "Colombie",
      "risque": "OUI",
      "question": ""
    },
    {
      "id": 44,
      "pays": "Comores",
      "risque": "OUI",
      "question": ""
    },
    {
      "id": 45,
      "pays": "Congo",
      "risque": "NON",
      "question": ""
    },
    {
      "id": 46,
      "pays": "Congo (R‚publique d‚mocratique du)",
      "risque": "OUI",
      "question": ""
    },
    {
      "id": 47,
      "pays": "Cor‚e du Nord (r‚p. pop. d‚m. de)",
      "risque": "NON",
      "question": ""
    },
    {
      "id": 48,
      "pays": "Cor‚e du Sud (r‚p. de)",
      "risque": "NON",
      "question": ""
    },
    {
      "id": 49,
      "pays": "Costa Rica",
      "risque": "OUI",
      "question": ""
    },
    {
      "id": 50,
      "pays": "C“te d?Ivoire",
      "risque": "OUI",
      "question": ""
    },
    {
      "id": 51,
      "pays": "Croatie",
      "risque": "NON",
      "question": ""
    },
    {
      "id": 52,
      "pays": "Cuba",
      "risque": "OUI",
      "question": ""
    },
    {
      "id": 53,
      "pays": "Cura‡ao",
      "risque": "OUI",
      "question": ""
    },
    {
      "id": 54,
      "pays": "Danemark",
      "risque": "NON",
      "question": ""
    },
    {
      "id": 55,
      "pays": "Djibouti",
      "risque": "NON",
      "question": ""
    },
    {
      "id": 56,
      "pays": "Dominique",
      "risque": "OUI",
      "question": ""
    },
    {
      "id": 57,
      "pays": "gypte",
      "risque": "NON",
      "question": ""
    },
    {
      "id": 58,
      "pays": "El Salvador",
      "risque": "OUI",
      "question": ""
    },
    {
      "id": 59,
      "pays": "mirats arabes unis",
      "risque": "NON",
      "question": ""
    },
    {
      "id": 60,
      "pays": "quateur",
      "risque": "OUI",
      "question": ""
    },
    {
      "id": 61,
      "pays": "rythr‚e",
      "risque": "NON",
      "question": ""
    },
    {
      "id": 62,
      "pays": "Espagne",
      "risque": "NON",
      "question": ""
    },
    {
      "id": 63,
      "pays": "Estonie",
      "risque": "NON",
      "question": ""
    },
    {
      "id": 64,
      "pays": "Eswatini",
      "risque": "NON",
      "question": ""
    },
    {
      "id": 65,
      "pays": "tats-Unis d?Am‚rique",
      "risque": "OUI",
      "question": "Hawa‹, Sud de la Floride et Sud du Texas"
    },
    {
      "id": 66,
      "pays": "thiopie",
      "risque": "OUI",
      "question": ""
    },
    {
      "id": 67,
      "pays": "Fidji",
      "risque": "OUI",
      "question": ""
    },
    {
      "id": 68,
      "pays": "Finlande",
      "risque": "NON",
      "question": ""
    },
    {
      "id": 69,
      "pays": "France",
      "risque": "NON",
      "question": ""
    },
    {
      "id": 70,
      "pays": "Gabon",
      "risque": "OUI",
      "question": ""
    },
    {
      "id": 71,
      "pays": "Gambie",
      "risque": "NON",
      "question": ""
    },
    {
      "id": 72,
      "pays": "G‚orgie",
      "risque": "NON",
      "question": ""
    },
    {
      "id": 73,
      "pays": "G‚orgie du Sud-et-les ×les Sandwich du Sud",
      "risque": "NON",
      "question": ""
    },
    {
      "id": 74,
      "pays": "Ghana",
      "risque": "OUI",
      "question": ""
    },
    {
      "id": 75,
      "pays": "Gibraltar",
      "risque": "NON",
      "question": ""
    },
    {
      "id": 76,
      "pays": "GrŠce",
      "risque": "NON",
      "question": ""
    },
    {
      "id": 77,
      "pays": "Grenade",
      "risque": "OUI",
      "question": ""
    },
    {
      "id": 78,
      "pays": "Groenland",
      "risque": "NON",
      "question": ""
    },
    {
      "id": 79,
      "pays": "Guadeloupe",
      "risque": "OUI",
      "question": ""
    },
    {
      "id": 80,
      "pays": "Guam",
      "risque": "NON",
      "question": ""
    },
    {
      "id": 81,
      "pays": "Guatemala",
      "risque": "OUI",
      "question": ""
    },
    {
      "id": 82,
      "pays": "Guernesey",
      "risque": "NON",
      "question": ""
    },
    {
      "id": 83,
      "pays": "Guin‚e",
      "risque": "OUI",
      "question": ""
    },
    {
      "id": 84,
      "pays": "Guin‚e ‚quatoriale",
      "risque": "NON",
      "question": ""
    },
    {
      "id": 85,
      "pays": "Guin‚e-Bissau",
      "risque": "NON",
      "question": ""
    },
    {
      "id": 86,
      "pays": "Guyana",
      "risque": "OUI",
      "question": ""
    },
    {
      "id": 87,
      "pays": "Guyane fran‡aise",
      "risque": "OUI",
      "question": ""
    },
    {
      "id": 88,
      "pays": "Ha‹ti",
      "risque": "OUI",
      "question": ""
    },
    {
      "id": 89,
      "pays": "Honduras",
      "risque": "OUI",
      "question": ""
    },
    {
      "id": 90,
      "pays": "Hongrie",
      "risque": "NON",
      "question": ""
    },
    {
      "id": 91,
      "pays": "×le Christmas",
      "risque": "NON",
      "question": ""
    },
    {
      "id": 92,
      "pays": "×le de Man",
      "risque": "NON",
      "question": ""
    },
    {
      "id": 93,
      "pays": "×le Norfolk",
      "risque": "NON",
      "question": ""
    },
    {
      "id": 94,
      "pays": "×les Ca‹manes",
      "risque": "OUI",
      "question": ""
    },
    {
      "id": 95,
      "pays": "×les Cook",
      "risque": "OUI",
      "question": ""
    },
    {
      "id": 96,
      "pays": "×les d?land",
      "risque": "NON",
      "question": ""
    },
    {
      "id": 97,
      "pays": "×les des Cocos (Keeling)",
      "risque": "NON",
      "question": ""
    },
    {
      "id": 98,
      "pays": "×les Falkland (Malvinas)",
      "risque": "NON",
      "question": ""
    },
    {
      "id": 99,
      "pays": "×les F‚ro‚",
      "risque": "NON",
      "question": ""
    },
    {
      "id": 100,
      "pays": "×les Mariannes du Nord",
      "risque": "OUI",
      "question": ""
    },
    {
      "id": 101,
      "pays": "×les Marshall",
      "risque": "OUI",
      "question": ""
    },
    {
      "id": 102,
      "pays": "×les Salomon",
      "risque": "OUI",
      "question": ""
    },
    {
      "id": 103,
      "pays": "×les Svalbard-et-Jan Mayen",
      "risque": "NON",
      "question": ""
    },
    {
      "id": 104,
      "pays": "×les Turques-et-Ca‹ques",
      "risque": "OUI",
      "question": ""
    },
    {
      "id": 105,
      "pays": "×les Vierges am‚ricaines",
      "risque": "OUI",
      "question": ""
    },
    {
      "id": 106,
      "pays": "×les Vierges britanniques",
      "risque": "OUI",
      "question": ""
    },
    {
      "id": 107,
      "pays": "×les Wallis-et-Futuna",
      "risque": "OUI",
      "question": ""
    },
    {
      "id": 108,
      "pays": "Inde",
      "risque": "OUI",
      "question": ""
    },
    {
      "id": 109,
      "pays": "Indon‚sie",
      "risque": "OUI",
      "question": ""
    },
    {
      "id": 110,
      "pays": "Irak",
      "risque": "NON",
      "question": ""
    },
    {
      "id": 111,
      "pays": "Iran (R‚publique islamique d?)",
      "risque": "NON",
      "question": ""
    },
    {
      "id": 112,
      "pays": "Irlande",
      "risque": "NON",
      "question": ""
    },
    {
      "id": 113,
      "pays": "Islande",
      "risque": "NON",
      "question": ""
    },
    {
      "id": 114,
      "pays": "Isra‰l",
      "risque": "NON",
      "question": ""
    },
    {
      "id": 115,
      "pays": "Italie",
      "risque": "NON",
      "question": ""
    },
    {
      "id": 116,
      "pays": "Jama‹que",
      "risque": "OUI",
      "question": ""
    },
    {
      "id": 117,
      "pays": "Japon",
      "risque": "NON",
      "question": ""
    },
    {
      "id": 118,
      "pays": "Jersey",
      "risque": "NON",
      "question": ""
    },
    {
      "id": 119,
      "pays": "Jordanie",
      "risque": "NON",
      "question": ""
    },
    {
      "id": 120,
      "pays": "Kazakhstan",
      "risque": "NON",
      "question": ""
    },
    {
      "id": 121,
      "pays": "Kenya",
      "risque": "OUI",
      "question": ""
    },
    {
      "id": 122,
      "pays": "Kirghizistan",
      "risque": "NON",
      "question": ""
    },
    {
      "id": 123,
      "pays": "Kiribati",
      "risque": "OUI",
      "question": ""
    },
    {
      "id": 124,
      "pays": "Kowe‹t",
      "risque": "NON",
      "question": ""
    },
    {
      "id": 125,
      "pays": "Laos (R‚publique d‚mocratique populaire)",
      "risque": "OUI",
      "question": ""
    },
    {
      "id": 126,
      "pays": "Lesotho",
      "risque": "NON",
      "question": ""
    },
    {
      "id": 127,
      "pays": "Lettonie",
      "risque": "NON",
      "question": ""
    },
    {
      "id": 128,
      "pays": "Liban",
      "risque": "NON",
      "question": ""
    },
    {
      "id": 129,
      "pays": "Lib‚ria",
      "risque": "NON",
      "question": ""
    },
    {
      "id": 130,
      "pays": "Libye",
      "risque": "NON",
      "question": ""
    },
    {
      "id": 131,
      "pays": "Liechtenstein",
      "risque": "NON",
      "question": ""
    },
    {
      "id": 132,
      "pays": "Lituanie",
      "risque": "NON",
      "question": ""
    },
    {
      "id": 133,
      "pays": "Luxembourg",
      "risque": "NON",
      "question": ""
    },
    {
      "id": 134,
      "pays": "Mac‚doine",
      "risque": "NON",
      "question": ""
    },
    {
      "id": 135,
      "pays": "Madagascar",
      "risque": "OUI",
      "question": ""
    },
    {
      "id": 136,
      "pays": "Malaisie",
      "risque": "OUI",
      "question": ""
    },
    {
      "id": 137,
      "pays": "Malawi",
      "risque": "NON",
      "question": ""
    },
    {
      "id": 138,
      "pays": "Maldives",
      "risque": "OUI",
      "question": ""
    },
    {
      "id": 139,
      "pays": "Mali",
      "risque": "OUI",
      "question": ""
    },
    {
      "id": 140,
      "pays": "Malte",
      "risque": "NON",
      "question": ""
    },
    {
      "id": 141,
      "pays": "Maroc",
      "risque": "NON",
      "question": ""
    },
    {
      "id": 142,
      "pays": "Martinique",
      "risque": "OUI",
      "question": ""
    },
    {
      "id": 143,
      "pays": "Maurice",
      "risque": "OUI",
      "question": ""
    },
    {
      "id": 144,
      "pays": "Mauritanie",
      "risque": "NON",
      "question": ""
    },
    {
      "id": 145,
      "pays": "Mayotte",
      "risque": "NON",
      "question": ""
    },
    {
      "id": 146,
      "pays": "Mexique",
      "risque": "OUI",
      "question": ""
    },
    {
      "id": 147,
      "pays": "Micron‚sie (tats f‚d‚r‚s de)",
      "risque": "OUI",
      "question": ""
    },
    {
      "id": 148,
      "pays": "Moldavie",
      "risque": "NON",
      "question": ""
    },
    {
      "id": 149,
      "pays": "Monaco",
      "risque": "NON",
      "question": ""
    },
    {
      "id": 150,
      "pays": "Mongolie",
      "risque": "NON",
      "question": ""
    },
    {
      "id": 151,
      "pays": "Mont‚n‚gro",
      "risque": "NON",
      "question": ""
    },
    {
      "id": 152,
      "pays": "Montserrat",
      "risque": "OUI",
      "question": ""
    },
    {
      "id": 153,
      "pays": "Mozambique",
      "risque": "OUI",
      "question": ""
    },
    {
      "id": 154,
      "pays": "Myanmar",
      "risque": "OUI",
      "question": ""
    },
    {
      "id": 155,
      "pays": "Namibie",
      "risque": "NON",
      "question": ""
    },
    {
      "id": 156,
      "pays": "Nauru",
      "risque": "OUI",
      "question": ""
    },
    {
      "id": 157,
      "pays": "N‚pal",
      "risque": "OUI",
      "question": ""
    },
    {
      "id": 158,
      "pays": "Nicaragua",
      "risque": "OUI",
      "question": ""
    },
    {
      "id": 159,
      "pays": "Niger",
      "risque": "NON",
      "question": ""
    },
    {
      "id": 160,
      "pays": "Nig‚ria",
      "risque": "OUI",
      "question": ""
    },
    {
      "id": 161,
      "pays": "Niou‚",
      "risque": "OUI",
      "question": ""
    },
    {
      "id": 162,
      "pays": "NorvŠge",
      "risque": "NON",
      "question": ""
    },
    {
      "id": 163,
      "pays": "Nouvelle-Cal‚donie",
      "risque": "OUI",
      "question": ""
    },
    {
      "id": 164,
      "pays": "Nouvelle-Z‚lande",
      "risque": "NON",
      "question": ""
    },
    {
      "id": 165,
      "pays": "Oman",
      "risque": "NON",
      "question": ""
    },
    {
      "id": 166,
      "pays": "Ouganda",
      "risque": "NON",
      "question": ""
    },
    {
      "id": 167,
      "pays": "Ouzb‚kistan",
      "risque": "NON",
      "question": ""
    },
    {
      "id": 168,
      "pays": "Pakistan",
      "risque": "OUI",
      "question": ""
    },
    {
      "id": 169,
      "pays": "Palaos",
      "risque": "OUI",
      "question": ""
    },
    {
      "id": 170,
      "pays": "Palestine",
      "risque": "NON",
      "question": ""
    },
    {
      "id": 171,
      "pays": "Panama",
      "risque": "OUI",
      "question": ""
    },
    {
      "id": 172,
      "pays": "Papouasie-Nouvelle-Guin‚e",
      "risque": "OUI",
      "question": ""
    },
    {
      "id": 173,
      "pays": "Paraguay",
      "risque": "OUI",
      "question": ""
    },
    {
      "id": 174,
      "pays": "Pays-Bas",
      "risque": "NON",
      "question": ""
    },
    {
      "id": 175,
      "pays": "P‚rou",
      "risque": "OUI",
      "question": ""
    },
    {
      "id": 176,
      "pays": "Philippines",
      "risque": "OUI",
      "question": ""
    },
    {
      "id": 177,
      "pays": "Pitcairn",
      "risque": "NON",
      "question": ""
    },
    {
      "id": 178,
      "pays": "Pologne",
      "risque": "NON",
      "question": ""
    },
    {
      "id": 179,
      "pays": "Polyn‚sie fran‡aise",
      "risque": "OUI",
      "question": ""
    },
    {
      "id": 180,
      "pays": "Porto Rico",
      "risque": "OUI",
      "question": ""
    },
    {
      "id": 181,
      "pays": "Portugal",
      "risque": "NON",
      "question": ""
    },
    {
      "id": 182,
      "pays": "Qatar",
      "risque": "NON",
      "question": ""
    },
    {
      "id": 183,
      "pays": "R‚publique centrafricaine",
      "risque": "NON",
      "question": ""
    },
    {
      "id": 184,
      "pays": "R‚publique dominicaine",
      "risque": "OUI",
      "question": ""
    },
    {
      "id": 185,
      "pays": "R‚publique tchŠque",
      "risque": "NON",
      "question": ""
    },
    {
      "id": 186,
      "pays": "R‚union",
      "risque": "OUI",
      "question": ""
    },
    {
      "id": 187,
      "pays": "Roumanie",
      "risque": "NON",
      "question": ""
    },
    {
      "id": 188,
      "pays": "Royaume-Uni",
      "risque": "NON",
      "question": ""
    },
    {
      "id": 189,
      "pays": "Russie (F‚d‚ration de)",
      "risque": "NON",
      "question": ""
    },
    {
      "id": 190,
      "pays": "Rwanda",
      "risque": "NON",
      "question": ""
    },
    {
      "id": 191,
      "pays": "Saba",
      "risque": "OUI",
      "question": ""
    },
    {
      "id": 192,
      "pays": "Sahara occidental",
      "risque": "NON",
      "question": ""
    },
    {
      "id": 193,
      "pays": "Saint-Barth‚lemy",
      "risque": "OUI",
      "question": ""
    },
    {
      "id": 194,
      "pays": "Sainte-H‚lŠne",
      "risque": "NON",
      "question": ""
    },
    {
      "id": 195,
      "pays": "Sainte-Lucie",
      "risque": "OUI",
      "question": ""
    },
    {
      "id": 196,
      "pays": "Saint-Eustache",
      "risque": "OUI",
      "question": ""
    },
    {
      "id": 197,
      "pays": "Saint-Kitts-et-Nevis",
      "risque": "OUI",
      "question": ""
    },
    {
      "id": 198,
      "pays": "Saint-Marin",
      "risque": "NON",
      "question": ""
    },
    {
      "id": 199,
      "pays": "Saint-Martin (partie fran‡aise)",
      "risque": "OUI",
      "question": ""
    },
    {
      "id": 200,
      "pays": "Saint-Martin (partie n‚erlandaise)",
      "risque": "OUI",
      "question": ""
    },
    {
      "id": 201,
      "pays": "Saint-Pierre-et-Miquelon",
      "risque": "NON",
      "question": ""
    },
    {
      "id": 202,
      "pays": "Saint-Vincent-et-les Grenadines",
      "risque": "OUI",
      "question": ""
    },
    {
      "id": 203,
      "pays": "Samoa",
      "risque": "OUI",
      "question": ""
    },
    {
      "id": 204,
      "pays": "Samoa am‚ricaines",
      "risque": "OUI",
      "question": ""
    },
    {
      "id": 205,
      "pays": "Sao Tom‚-et-Principe",
      "risque": "NON",
      "question": ""
    },
    {
      "id": 206,
      "pays": "S‚n‚gal",
      "risque": "OUI",
      "question": ""
    },
    {
      "id": 207,
      "pays": "Serbie",
      "risque": "NON",
      "question": ""
    },
    {
      "id": 208,
      "pays": "Sercq",
      "risque": "NON",
      "question": ""
    },
    {
      "id": 209,
      "pays": "Seychelles",
      "risque": "OUI",
      "question": ""
    },
    {
      "id": 210,
      "pays": "Sierra Leone",
      "risque": "OUI",
      "question": ""
    },
    {
      "id": 211,
      "pays": "Singapour",
      "risque": "OUI",
      "question": ""
    },
    {
      "id": 212,
      "pays": "Slovaquie",
      "risque": "NON",
      "question": ""
    },
    {
      "id": 213,
      "pays": "Slov‚nie",
      "risque": "NON",
      "question": ""
    },
    {
      "id": 214,
      "pays": "Somalie",
      "risque": "OUI",
      "question": ""
    },
    {
      "id": 215,
      "pays": "Soudan",
      "risque": "OUI",
      "question": ""
    },
    {
      "id": 216,
      "pays": "Soudan du Sud",
      "risque": "NON",
      "question": ""
    },
    {
      "id": 217,
      "pays": "Sri Lanka",
      "risque": "OUI",
      "question": ""
    },
    {
      "id": 218,
      "pays": "SuŠde",
      "risque": "NON",
      "question": ""
    },
    {
      "id": 219,
      "pays": "Suisse",
      "risque": "NON",
      "question": ""
    },
    {
      "id": 220,
      "pays": "Suriname",
      "risque": "OUI",
      "question": ""
    },
    {
      "id": 221,
      "pays": "Syrie",
      "risque": "NON",
      "question": ""
    },
    {
      "id": 222,
      "pays": "Tadjikistan",
      "risque": "NON",
      "question": ""
    },
    {
      "id": 223,
      "pays": "Ta‹wan",
      "risque": "OUI",
      "question": ""
    },
    {
      "id": 224,
      "pays": "Tanzanie (R‚publique-Unie de)",
      "risque": "OUI",
      "question": ""
    },
    {
      "id": 225,
      "pays": "Tchad",
      "risque": "NON",
      "question": ""
    },
    {
      "id": 226,
      "pays": "Terres australes fran‡aises",
      "risque": "NON",
      "question": ""
    },
    {
      "id": 227,
      "pays": "Territoire britannique de l'oc‚an Indien",
      "risque": "NON",
      "question": ""
    },
    {
      "id": 228,
      "pays": "Tha‹lande",
      "risque": "OUI",
      "question": ""
    },
    {
      "id": 229,
      "pays": "Timor-Leste",
      "risque": "OUI",
      "question": ""
    },
    {
      "id": 230,
      "pays": "Togo",
      "risque": "OUI",
      "question": ""
    },
    {
      "id": 231,
      "pays": "Tok‚laou",
      "risque": "OUI",
      "question": ""
    },
    {
      "id": 232,
      "pays": "Tonga",
      "risque": "OUI",
      "question": ""
    },
    {
      "id": 233,
      "pays": "Trinit‚-et-Tobago",
      "risque": "OUI",
      "question": ""
    },
    {
      "id": 234,
      "pays": "Tunisie",
      "risque": "NON",
      "question": ""
    },
    {
      "id": 235,
      "pays": "Turkm‚nistan",
      "risque": "NON",
      "question": ""
    },
    {
      "id": 236,
      "pays": "Turquie",
      "risque": "NON",
      "question": ""
    },
    {
      "id": 237,
      "pays": "Tuvalu",
      "risque": "NON",
      "question": ""
    },
    {
      "id": 238,
      "pays": "Ukraine",
      "risque": "NON",
      "question": ""
    },
    {
      "id": 239,
      "pays": "Uruguay",
      "risque": "OUI",
      "question": ""
    },
    {
      "id": 240,
      "pays": "Vanuatu",
      "risque": "OUI",
      "question": ""
    },
    {
      "id": 241,
      "pays": "Vatican",
      "risque": "NON",
      "question": ""
    },
    {
      "id": 242,
      "pays": "Venezuela (R‚publique bolivarienne du)",
      "risque": "OUI",
      "question": ""
    },
    {
      "id": 243,
      "pays": "Viet Nam",
      "risque": "OUI",
      "question": ""
    },
    {
      "id": 244,
      "pays": "Y‚men",
      "risque": "OUI",
      "question": ""
    },
    {
      "id": 245,
      "pays": "Zambie",
      "risque": "NON",
      "question": ""
    },
    {
      "id": 246,
      "pays": "Zimbabwe",
      "risque": "NON",
      "question": ""
    }
  ]
  export default dengue;