import axios from 'axios';

import {
  ADD_SANTE,
  GET_ERRORS,
  CLEAR_ERRORS,
  GET_ONE_SANTE,
  GET_SANTE,
  SANTE_LOADING,
  DELETE_SANTE,
  UPDATE_SANTE,
} from './types';
import config from '../utils/config';

const listArray = [
  'allergie',
];

// Set loading state
export const setSantesLoading = () => ({
  type: SANTE_LOADING,
});

// Clear errors
export const clearErrors = () => ({
  type: CLEAR_ERRORS,
});

// Get Santes
export const getSante = () => (dispatch, getState) => {
  dispatch(setSantesLoading());
  axios
    .get(`${config.baseUrl}/api/sante`)
    .then((res) => {
      const payload = res.data.map((i) => {
        const result = i;
        listArray.map((j) => {
          if (i[j] && i[j].length > 0) {
            result[j] = getState()[j] && getState()[j][j] ? getState()[j][j].filter(p => i[j].map(k => k._id).includes(p._id)) : [];
          }
          return result;
        });
        return result;
      });
      dispatch({
        type: GET_SANTE,
        payload,
      });
    })
    .catch(() =>
      dispatch({
        type: GET_SANTE,
        payload: null,
      }),
    );
};

// Add Post
export const updateSante = santeData => (dispatch) => {
  dispatch(clearErrors());
  axios
    .put(`${config.baseUrl}/api/sante`, santeData)
    .then(() => dispatch(getSante()))
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response ? err.response.data : err,
      }),
    );
};

// Add Post
export const addSante = santeData => (dispatch, getState) => {
  dispatch(clearErrors());
  const user = getState().auth.user ? getState().auth.user.id : null;
  if (!user) return;
  axios
    .post(`${config.baseUrl}/api/sante/${user}`, santeData)
    .then(() => dispatch(getSante()))
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response,
      }),
    );
};

// Get Post
export const postSante = id => (dispatch) => {
  dispatch(setSantesLoading());
  axios
    .get(`${config.baseUrl}/api/sante/${id}`)
    .then(res =>
      dispatch({
        type: GET_ONE_SANTE,
        payload: res.data,
      }),
    )
    .catch(() =>
      dispatch({
        type: GET_ONE_SANTE,
        payload: null,
      }),
    );
};

// Delete Santes
export const deleteSante = id => (dispatch) => {
  axios
    .delete(`${config.baseUrl}/api/sante/${id}`)
    .then(() =>
      dispatch({
        type: DELETE_SANTE,
        payload: id,
      }),
    )
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response ? err.response.data : err,
      }),
    );
};
