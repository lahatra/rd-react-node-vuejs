const allergie = [
  'name',
  'categorie',
  'description',
];

const urgence = [
  'description',
  'pays',
  'numero',
  'service',
];

const centre = [
  'name',
  'service',
  'numero',
  'rue',
  'cp',
  'ville',
  'telephone',
  'email',
];

const voyage = [
  'user',
  'pays',
  'dateArrive',
  'dateDepart',
];

const pays = [
  'name',
  'capital',
  'indicatifPhone',
  'decalageHoraore',
  'monnaie',
  'permis',
  'maladie',
  'centre',
  'medecin',
  'eau',
  'prise',
  'urgence',
];

const sejour = [
  'description',
  'isQuestion',
];

const sante = [
  'sanguin',
  'poids',
  'user',
  'allergie',
  'problemeSantePasse',
  'problemeSanteEncours',
  'naissance',
];

const checklist = [
  'intitule',
  'ordre',
  'commentaire',
];

const secours = [
  'intitule',
  'ordre',
  'commentaire',
];

const documents = [
  'description',
  'commentaire',
];

const maladie = [
  'name',
  'sejour',
  'vaccin',
  'vaccinSugg',
  'risque',
];

const vaccin = [
  'rappel',
  'name',
];

export default {
  vaccin,
  maladie,
  documents,
  secours,
  checklist,
  urgence,
  centre,
  voyage,
  pays,
  sejour,
  sante,
  allergie,
};
