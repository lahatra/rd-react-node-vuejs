import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Header4 from '../components/header4';

import { connect } from 'react-redux';

import mapStateToProps from '../redux/mapStateToProps';
import mapDispatchToProps from '../redux/mapDispatchToProps';

const getUnique = (arr, comp) => {
  const unique = arr
    .map(e => e[comp])
    .map((e, i, final) => final.indexOf(e) === i && i)
    .filter(e => arr[e]).map(e => arr[e]);

  return unique;
};

class CarnetVaccination extends Component {
  constructor(props) {
    super(props);
    this.state = {
      vaccinAll: [],
      paysCurrent: {},
      maladieCurrent: [],
      vaccinList: {
        v1: [],
        v2: [],
      },
      date: '',
      i: 0,
    };
  }

  async componentWillMount() {
    await this.props.getVaccin();
    await this.props.getPays();
    await this.props.getMaladie();
    await this.props.getVoyage();
    await this.props.getVaccin();
    const { one_voyage, voyage } = this.props.voyage;
    const { pays } = this.props.pays;
    const { maladie } = this.props.maladie;
    const { vaccin } = this.props.vaccin;

    const resD = {
      v1: [],
      v2: [],
    };
    const maladieDetail = voyage.reduce((acc, i) => {
      const paysV = pays.filter(j => j._id === i.pays);
      if (paysV.length > 0) {
        const idsMaladie = paysV[0].maladie.map(i => i._id);
        const maladieList = maladie && idsMaladie && maladie.filter(i => idsMaladie.includes(i._id));
        console.log('maladieList', maladieList);
        maladieList && acc.push(...maladieList);
      }
      return acc;
    }, []);

    console.log('maladieDetail maladieDetail', maladieDetail);
    const vaccinDetail = maladieDetail.reduce((ac, k) => {
      if(k.vaccin && k.vaccin.length > 0) {
        const v1 = vaccin.filter(j => k.vaccin.map(h => h._id).includes(j._id));
        ac.v1.push(...v1);
      }
      if(k.vaccinSugg && k.vaccinSugg.length > 0) {
        const v2 = vaccin.filter(j => k.vaccinSugg.map(h => h._id).includes(j._id));
        ac.v2.push(...v2);
      }
      return ac;
    }, resD);
    console.log('vaccinDetail vaccinDetail', vaccinDetail);
    const vaccinList = vaccinDetail ? { v1: getUnique(vaccinDetail.v1, '_id'), v2: getUnique(vaccinDetail.v2, '_id') } : { v1: [], v2: [] };
    this.setState({ vaccinList });
  }

  returnVaccinName = (id) => {
    const { vaccin } = this.props.vaccin;
    if (!vaccin) return {};
    const res = vaccin.filter(i => i._id === id);
    console.log('res', res);
    return res.length > 0 ? res[0] : {};
  }

  renderVaccinObl = () => {
    const list = []; 
    this.state.maladieCurrent.map(i => {
      return 
    });
  }

  render() {
    if (!this.props.auth.user) return <di />;
    const { vaccin, id, name, email, avatar } = this.props.auth.user;
    const { vaccinList } = this.state;

    return (
      <div>
        <Header4 />
        <div className="carnet-vaccination">
          <div id="titre">
            <div className="element">
              <label className="title">Carnet de Vaccination </label>
            </div>
          </div>


          <div id="conteneur">

            <div className="tab blue">
              <input id="tab-one" type="radio" name="tabs1" />
              <label htmlFor="tab-one">Mes vaccins à jour </label>
              {
                vaccin && vaccin.map(i => (
                  <div className="tab-content" key={i._id}>
                    <p>{this.returnVaccinName(i._id).name} - Mars 2018</p>
                    <img className="imgSuppr" src="../img/supprimer.png" alt="supprimer" />
                    { <p>Prochain rappel : { parseInt(i.date, 10) + this.returnVaccinName(i._id).rappel} </p>}

                  </div>
                ))
              }
              {/* <div className="tab-content">

                <p>DTP (diphtérie, tétanos, poliomielyte) - Mars 2018</p>
               <img className="imgSuppr" src="../img/supprimer.png" alt="supprimer" />
                <p>Prochain rappel : 2033 </p>

              </div> */}
            </div>

          </div>

          <div id="conteneur">
            <div className="tab blue">
              <input id="tab-two" type="radio" name="tabs2" />
              <label htmlFor="tab-two">Mes vaccins obligatoires </label>
              {
                vaccinList && vaccinList.v1.map(i => {
                  return (
                    <div className="tab-content" key={i._id}>
                      <p>{i.name} - Mars 2018</p>
                      <img className="imgSuppr" src="../img/supprimer.png" alt="supprimer" />
                      <p>Prochain rappel : {i.rappel} </p>
                    </div>
                  )
                })
              }
              {/* <div className="tab-content">
                <p>DTP (diphtérie, tétanos, poliomielyte) - Mars 2018</p>
                <img className="imgSuppr" src="../img/supprimer.png" alt="supprimer" />
                <p>Prochain rappel : 2033 </p>
              </div> */}
            </div>
          </div>

          <div id="conteneur">
            <div className="tab blue">
              <input id="tab-three" type="radio" name="tabs2" />
              <label htmlFor="tab-three">Mes vaccins recommandés </label>
              <div className="tab-content">
                {
                    vaccinList && vaccinList.v2.map(i => {
                      return (
                        <div id="conteneur"  key={i._id}>
                          <div className="element">
                            <input type="checkbox" className="ios-switch" /><div><div /></div>
                          </div>
                          <div className="element">
                            <p>{i.name}</p>
                          </div>
                          <div className="element">
                            <img src="../img/supprimer.png" className="imgSuppr" alt="supprimer" />
                          </div>
                        </div>
                      )
                    })
                  }
                {/* <div id="conteneur">
                  <div className="element">
                    <input type="checkbox" className="ios-switch" /><div><div /></div>
                  </div>
                  <div className="element">
                    <p>Coqueluche</p>
                  </div>
                  <div className="element">
                    <img src="../img/supprimer.png" className="imgSuppr" alt="supprimer" />
                  </div>

                </div>
                <div id="conteneur">
                  <div className="element"><input type="checkbox" className="ios-switch" /><div><div /></div></div>
                  <div className="element"><p>ROR -----</p></div>
                  <div className="element">
                    <img src="../img/supprimer.png" className="imgSuppr" alt="supprimer" />
                  </div>
                </div> */}

                <p>Recommandé habituellement
                        mais interdit à cause de votre allergie :</p>

                <div id="conteneur">
                  <div className="element"> <img src="../img/x.png" className="imgSuppr" alt="supprimer" /></div>
                  <div className="element"><p>Fièvre jaune</p></div>
                  <div className="element"> <img src="../img/supprimer.png" className="imgSuppr" alt="supprimer" /></div>
                </div>
              </div>

            </div>
          </div>


          <label className="description">  Vaccin 1  </label>


          <div className="tab blue">
            <input id="tab-four" type="radio" name="tabs2" />
            <label htmlFor="tab-four">Vaccination obligatoire ou recommandée : quelle différence ?
            </label>
            <div className="tab-content">
              <p>Il n’y a aucune différence en termes d’efficacité ou de sécurité entre les vaccins obligatoires ou recommandés, ni en termes de gravité de la maladie.

Schématiquement pour les vaccinations du voyage, les vaccinations obligatoires sont imposées par le règlement sanitaire international afin de limiter le risque de propagation de maladies à l’échelle des populations : déclenchement d’épidémies, importation de maladies dans une zone où elle n’existait pas, etc… On peut donc me refuser l’entrée dans un territoire si je ne suis pas vacciné car je serais susceptible de représenter un risque pour le reste de la population sur place, voire à mon retour.
Les vaccinations recommandées sont surtout destinées à me protéger. Elles ont néanmoins généralement un effet bénéfique à l’échelle de la population lorsque beaucoup de personnes sont vaccinées.

Pour ma protection personnelle, il n’y a donc pas de distinctions à faire entre les vaccinations obligatoires ou recommandées.
              </p>

            </div>
          </div>

          <div className="tab blue">
            <input id="tab-six" type="radio" name="tabs2" />
            <label htmlFor="tab-six">Contre-indications aux vaccins </label>
            <div className="tab-content">
              <p>Elles sont relativement rares, à l’exception des limites d’âge.
Elles sont prises en comptes dans « Mon état de santé ». Cela me permet d’avoir une idée des vaccins possibles et contre-indiqués. Néanmoins, il existe toujours des exceptions, les contre-indications devront donc être vérifiées par un professionnel de santé.
Pour tous les vaccins : un vaccin donné est définitivement contre-indiqué en prévention en cas d’allergie (hypersensibilité) connue à ce vaccin (que ce soit à la substance active, à l'un des excipients, ou aux résidus de production du vaccin) et temporairement contre-indiqué en cas d’infection aiguë modérée fébrile ou d’infection aigüe sévère (avec ou sans fièvre) jusqu’à guérison. Ces contre-indications ne sont donc pas mentionnées dans « Mon état de santé ».
La présence d’une infection mineure n’est pas une contre-indication à la vaccination.

              </p>
            </div>
          </div>
        </div>
        <div className="group">
          <div className="footer">

            <Link to="/vaccin">
              <img src="../img/vaccin.png" className="image_footer" alt="vaccin" />
            </Link>
            <Link to="/medicament">
              <img src="../img/medicament.png" className="image_footer" alt="medicament" />
            </Link>
            <Link to="/trousse">
              <img src="../img/trousse.png" className="image_footer" alt="trousse" />
            </Link>
            <Link to="/medecin">
              <img src="../img/medecin.png" className="image_footer" alt="medecin" />
            </Link>
            <Link to="/hand">
              <img src="../img/hand.png" className="image_footer" alt="hand" />
            </Link>
          </div>
        </div>
      </div>


    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(CarnetVaccination);
