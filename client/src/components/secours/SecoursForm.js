import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { addSecours, updateSecours } from '../../actions/SecoursActions';
import attribut from '../../attributs';

import TextFieldGroup from '../common/TextFieldGroup';
import TextAreaFieldGroup from '../common/TextareaFieldGroup';

class SecoursForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      text: this.props.data.name || '',
      errors: {},
      user: '',
      intitule: this.props.data.intitule || '',
      ordre: this.props.data.ordre || 0,
      commentaire: this.props.data.commentaire || '',
    };

    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  componentWillReceiveProps(newProps) {
    if (newProps.errors) {
      this.setState({ errors: newProps.errors });
    }
  }

  onSubmit(e) {
    e.preventDefault();

    const { user } = this.props.auth;

    const newSecours = {
      intitule: this.state.intitule,
      ordre: this.state.ordre,
      // categorie: this.state.categorie,
      commentaire: this.state.commentaire,
    };

    if(this.props.data._id) {
      newSecours.id = this.props.data._id
      this.props.updateSecours(newSecours);
    } else {
      this.props.addSecours(newSecours);
    }
    this.props.onClose();
  }

  onChange(e) {
    this.setState({ [e.target.name]: e.target.value });
  }

  render() {
    const { errors } = this.state;

    return (
      <div className="Secours-form mb-3">
        <div className="card card-info">
          <div className="card-header bg-info text-white">Say Something...</div>
          <div className="card-body">
            <form onSubmit={this.onSubmit}>
              {
                attribut.secours.map(key => {
                  if (key === 'user') {
                    return <div key={key} />;
                  }
                  if (key === 'ordre') {
                    return <div className="form-group" key={key}>
                      <TextFieldGroup
                        key={key}
                        type="number"
                        placeholder={`Creaction ${key}`}
                        name={key}
                        value={this.state[key]}
                        onChange={this.onChange}
                        error={errors[key]}
                      />
                    </div>
                  }
                  return <div className="form-group" key={key}>
                  <TextAreaFieldGroup
                    placeholder={`Creaction ${key}`}
                    name={key}
                    value={this.state[key]}
                    onChange={this.onChange}
                    error={errors[key]}
                  />
                </div>
                })
              }
              <button type="submit" className="btn btn-dark">
                Submit
              </button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

SecoursForm.propTypes = {
  updateSecours: PropTypes.func.isRequired,
  addSecours: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired,
  errors: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  auth: state.auth,
  errors: state.errors
});

export default connect(mapStateToProps, { addSecours, updateSecours })(SecoursForm);
